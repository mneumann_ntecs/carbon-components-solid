# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!-- ## Unreleased -->

## [0.0.10](https://gitlab.com/mneumann_ntecs/carbon-components-solid/-/tree/v0.0.10) - 2021-02-08

**Features**

- `Button` now respects `style` prop and applies it to the root element.

- Updated to `solid-js` version 0.24.0.

## [0.0.9](https://gitlab.com/mneumann_ntecs/carbon-components-solid/-/tree/v0.0.9) - 2021-01-01

**Features**

- Added `CustomLinkContext`. This can be used to set a custom component to be
  used instead of the `<a>` tag for all `UnstyledLink`'s (this includes the
  `Link` component, which is based on the `UnstyledLink`). This comes in handy
  when using a router library. For example when using `@rturnq/solid-router`:

  ```jsx
  import { Link as RouterLink } from '@rturnq/solid-router';
  import { Link, CustomLinkContext } from 'carbon-components-solid';

  const App = () => (
    <CustomLinkContext.Provider value={{component: RouterLink}}>
      <Link href="/">...</Link>
    </CustomLinkContext.Provider>
  );
  ```

- Added `UnstyledLink` component.

- The following components respect a `class` prop: `ButtonSet`,
  `ButtonSkeleton`, `BreadcrumbItem`, `BreadcrumbSkeleton`, `Checkbox`,
  `CheckboxSkeleton`, `ClickableTile`, `ContentSwitcher.Switch`,
  `DataTable.Toolbar`, `Form`, `FormItem`, `FormLabel`, `InlineCheckbox`,
  `LoadingSpinner`, `Modal`, `Slider`, `Table`, `TableContainer`,
  `TextInputSkeleton`, `Tile`, `UIShell.GlobalHeader.HeaderNav`,
  `UIShell.HeaderGlobalAction`, `UIShell.SideNav`,
  `UIShell.SideNav.HamburgerMenu`, `UIShell.SideNavIcon`,
  `UIShell.SideNavItem`.

- The `class` property of `GlobalHeader.HeaderNavItem` and
  `UIShell.SideNavLink` is respected and applied to the inner anchor element.

## [0.0.8](https://gitlab.com/mneumann_ntecs/carbon-components-solid/-/tree/v0.0.8) - 2020-12-30

**Features**

- Added `Breadcrumb`, `BreadcrumbItem` and `BreadcrumbSkeleton` components.

- Added `tabindex` prop to `TextInput` and `PasswordInput`.

**Breaking Changes**

- Moved icons out into `carbon-icons-solid` npm package.

**Bug Fixes**

- Fixed dereferencing an `undefined` in the `Slider` component
  (`evt.touches[0].clientX`) when the `TouchList` is empty.

## [0.0.7](https://gitlab.com/mneumann_ntecs/carbon-components-solid/-/tree/v0.0.7) - 2020-12-26

**Features**

- Added `ContentSwitcher` and `Switch` components.

- Added `Grid`, `Row` and `Column` components.

- Added `Slider` component.

**Breaking Changes**

- `TextInput`: Renamed `model` prop to `value`. There is no good reason why you
  would want to use a `TextInput` without a backing signal.

## [0.0.6](https://gitlab.com/mneumann_ntecs/carbon-components-solid/-/tree/v0.0.6) - 2020-12-19

**Features**

- Added `hideCloseButton` boolean prop for `Modal`.

- InlineLoading respects `style` prop.

- Added `Link` component.

- Added `SideNavIcon`, `SideNavItem` and `SideNavLinkText` components. Used
  internally by `SideNav`. `SideNavLink` now respects `large` prop.

- `Content` respects custom CSS `class`.

- Added `SideNav`, `SideNavItems` and `SideNavLink` components.

- `Header` now respects the `persistentHamburgerMenu` prop.

- Added `Icon` and `IconSkeleton` components.

- Added `HeaderNav` and `HeaderNavItem` components.

- Added `ClickableTile` component. Has no `clicked` property as svelte does. The
  reason: There is no `bx--tile--is-clicked` CSS class. Add `onClicked`
  callback instead.

**Breaking Changes**

- `HamburgerMenu` and `GlobalHeader/Header` now take `aria-label` props instead of `ariaLabel`.

**Bug Fixes**

- Fix `SideNav`. The overlay now works properly.

- Fixed an issue with Storybook. `createEffect()` did not work in Storybook as
  the components were not running in a `createRoot()`.

## [0.0.5](https://gitlab.com/mneumann_ntecs/carbon-components-solid/-/tree/v0.0.5) - 2020-12-17

**Features**

- Implement ToastNotification and InlineNotification.

- Add `class` prop to `Button`.

- Implement `Tile` component.

- Implement `Loading` and `InlineLoading` components.

- Add `invalid` and `invalidText` props to `Checkbox`. IMHO, checkbox can have
  invalid state, especially when `indeterminate` is used. Furthermore it's
  common practice to use checkboxes for "Agree Terms of Service", which must be
  checked. It's then better to highlight the checkbox instead of letting the
  user find where he has to click.

- Include the CSS in the package.

- Add `fluid` prop to `TextInput`. This can be used to override the value
  supplied by the fluid context.

**Breaking Changes**

- `CarbonIconComponent` no longer takes prop `className`. Use `class` instead.

- `TextInput` takes now a `model` prop instead of a `value` prop. If no `model`
  is passed, but a `value` prop, this is used as the initial value. Input type
  defaults to `"text"` now.

- `TextInput` no longer takes `restProps`. If there is need to pass through any
  props to the `div` element, it'd be better to add an explicit `divProps`
  props.

## [0.0.4](https://gitlab.com/mneumann_ntecs/carbon-components-solid/-/tree/v0.0.4) - 2020-12-16

**Fixes**

- `TextInput` and `PasswordInput`: Apply `restProps` on `input` element in order to support `onBlur` event.

