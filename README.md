# Carbon Components for Solid (WIP)

This is a port of [Carbon Components Svelte][carbon-components-svelte] to
[Solid][solid]. Included are all 5800+ Carbon icons as Solid components.

Credit goes to the developers of the projects this was ported from.

[Live Storybook][storybook]

## Run storybook

```
npm run storybook
```

## External Dependencies

The `carbon-components-solid` package depends on two external packages:

* `solid-js`
* `carbon-icons-solid`

## Status

Ported components:

* _All carbon icons_
* [Breadcrumb](https://mneumann_ntecs.gitlab.io/carbon-components-solid/storybook/?path=/story/breadcrumb--default)
* Button
* ButtonSet
* ButtonSkeleton
* InlineCheckbox
* Checkbox
* CheckboxSkeleton
* Form
* FormGroup
* FormItem
* FormLabel
* Modal
* PasswordInput
* TextInput
* TextInputSkeleton
* UIShell/Content
* UIShell/Header
* UIShell/HeaderUtilities
* UIShell/HeaderGlobalAction
* UIShell/SideBar
* Table
* DataTable
* Icon
* InlineLoading
* Link
* Loading
* InlineNotification
* ToastNotification
* Tile
* ClickableTile
* [Slider](https://mneumann_ntecs.gitlab.io/carbon-components-solid/storybook/?path=/story/slider--default)
* [Grid](https://mneumann_ntecs.gitlab.io/carbon-components-solid/storybook/?path=/story/grid--default)
* [ContentSwitcher](https://mneumann_ntecs.gitlab.io/carbon-components-solid/storybook/?path=/story/contentswitcher--default)

Custom components (not directly found in carbon-components-svelte):

* Theme

[carbon-components-svelte]: https://github.com/IBM/carbon-components-svelte
[solid]: https://github.com/ryansolid/solid
[storybook]: https://mneumann_ntecs.gitlab.io/carbon-components-solid/storybook/
