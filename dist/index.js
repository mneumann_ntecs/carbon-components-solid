import { template, createComponent, Dynamic, mergeProps as mergeProps$1, spread, effect, setAttribute, memo, insert, addEventListener, style, delegateEvents, classList } from 'solid-js/web';
import { splitProps, mergeProps, createContext, useContext, createMemo, createSignal, createEffect, onMount, onCleanup, Show, createState, For, Switch as Switch$1, Match } from 'solid-js';

var idCounter = 0;
function uniqueId(prefix = "ccs-") {
  var id = ++idCounter;
  return `${prefix}${id}`;
}
function clamp(val, min, max) {
  return Math.max(min, Math.min(val, max));
}
function extractProps(allProps, ownPropsDefaults) {
  const ownKeys = Object.keys(Object.getOwnPropertyDescriptors(ownPropsDefaults));
  const [ownProps, restProps] = splitProps(allProps, ownKeys);
  const props = mergeProps(ownPropsDefaults, ownProps);
  return [props, restProps];
}
function cx(...classNames) {
  return classNames.filter(Boolean).join(" ");
}

const CustomLinkContext = createContext({
  component: null
});

const _tmpl$ = template(`<a></a>`, 2);
const UnstyledLink = props => {
  const ctx = useContext(CustomLinkContext);
  const rel = createMemo(() => props.target === "_blank" ? "noopener noreferrer" : undefined);
  return ctx && ctx.component ? createComponent(Dynamic, mergeProps$1({
    get component() {
      return ctx.component;
    },

    get rel() {
      return rel();
    }

  }, props, {})) : (() => {
    const _el$ = _tmpl$.cloneNode(true);

    spread(_el$, props, false, false);

    effect(() => setAttribute(_el$, "rel", rel()));

    return _el$;
  })();
};

const _tmpl$$1 = template(`<div></div>`, 2);
const ButtonSkeleton = props => () => {
  const [, restProps] = splitProps(props, ["size", "href", "class"]);

  const classes = () => cx("bx--skeleton", "bx--btn", props.size === "field" && "bx--btn--field", props.size === "small" && "bx--btn--sm", props.class);

  return props.href === undefined ? (() => {
    const _el$ = _tmpl$$1.cloneNode(true);

    spread(_el$, restProps, false, false);

    effect(() => _el$.className = classes());

    return _el$;
  })() : createComponent(UnstyledLink, mergeProps$1({
    role: "button",

    get href() {
      return props.href;
    },

    get ["class"]() {
      return classes();
    },

    children: ""
  }, restProps, {}));
};

const tooltipPositionClass = tooltipPosition => tooltipPosition && `bx--tooltip--${tooltipPosition}`;

const tooltipAlignmentClass = tooltipAlignment => tooltipAlignment && `bx--tooltip--align-${tooltipAlignment}`;

const _tmpl$$2 = template(`<span class="bx--assistive-text"></span>`, 2),
      _tmpl$2 = template(`<button></button>`, 2);
const Button = props => {
  const body = () => [memo((() => {
    const _c$ = memo(() => !!props.hasIconOnly, true);

    return () => _c$() && (() => {
      const _el$ = _tmpl$$2.cloneNode(true);

      insert(_el$, () => props.iconDescription);

      return _el$;
    })();
  })()), memo(() => props.children), memo((() => {
    const _c$2 = memo(() => !!props.icon, true);

    return () => _c$2() && createComponent(Dynamic, {
      get component() {
        return props.icon;
      },

      "aria-hidden": "true",
      "class": "bx--btn__icon",

      get ["aria-label"]() {
        return props.iconDescription;
      }

    });
  })())];

  if (props.skeleton) {
    return createComponent(ButtonSkeleton, mergeProps$1({
      get href() {
        return props.href;
      },

      get size() {
        return props.size;
      },

      get onClick() {
        return props.onClick;
      },

      get style() {
        return props.hasIconOnly ? `width: 3rem; ${props.style}` : props.style;
      },

      get ["class"]() {
        return props.class;
      }

    }, props.restProps, {}));
  }

  if (props.href && !props.disabled) {
    return createComponent(UnstyledLink, {
      role: "button",

      get href() {
        return props.href;
      },

      get tabindex() {
        return props.tabindex || "0";
      },

      get ["class"]() {
        return buttonClass(props);
      },

      get style() {
        return props.style;
      },

      get children() {
        return body();
      }

    });
  }

  return (() => {
    const _el$2 = _tmpl$2.cloneNode(true);

    addEventListener(_el$2, "click", props.onClick, true);

    spread(_el$2, () => props.restProps, false, true);

    insert(_el$2, body);

    effect(_p$ => {
      const _v$ = props.type || "button",
            _v$2 = props.tabindex || "0",
            _v$3 = props.disabled,
            _v$4 = buttonClass(props),
            _v$5 = props.style;

      _v$ !== _p$._v$ && setAttribute(_el$2, "type", _p$._v$ = _v$);
      _v$2 !== _p$._v$2 && setAttribute(_el$2, "tabindex", _p$._v$2 = _v$2);
      _v$3 !== _p$._v$3 && (_el$2.disabled = _p$._v$3 = _v$3);
      _v$4 !== _p$._v$4 && (_el$2.className = _p$._v$4 = _v$4);
      _p$._v$5 = style(_el$2, _v$5, _p$._v$5);
      return _p$;
    }, {
      _v$: undefined,
      _v$2: undefined,
      _v$3: undefined,
      _v$4: undefined,
      _v$5: undefined
    });

    return _el$2;
  })();
};

const buttonKindClass = kind => `bx--btn--${kind}`;

const buttonClass = props => cx(props.class, "bx--btn", props.size === "field" && "bx--btn--field", props.size === "small" && "bx--btn--sm", buttonKindClass(props.kind || "primary"), props.disabled && "bx--btn--disabled", props.hasIconOnly && "bx--btn--icon-only bx--tooltip__trigger bx--tooltip--a11y", tooltipPositionClass(props.hasIconOnly ? props.tooltipPosition : undefined), tooltipAlignmentClass(props.hasIconOnly ? props.tooltipAlignment : undefined));

delegateEvents(["click"]);

const _tmpl$$3 = template(`<div></div>`, 2);
const ButtonSet = props => {
  const [local, restProps] = splitProps(props, ["stacked", "class"]);
  return (() => {
    const _el$ = _tmpl$$3.cloneNode(true);

    spread(_el$, restProps, false, false);

    effect(() => _el$.className = cx("bx--btn-set", local.stacked && "bx--btn-set--stacked", local.class));

    return _el$;
  })();
};

function preventDefault(origHandler, ev) {
  ev.preventDefault();
  callEventHandler(origHandler, ev);
}
function callEventHandler(origHandler, ev) {
  if (typeof origHandler === "function") {
    origHandler(ev);
  } else {
    const [handler, value] = origHandler;

    if (handler) {
      handler(value, ev);
    }
  }
}

const _tmpl$$4 = template(`<form></form>`, 2);
const Form = props => {
  const [local, restProps] = splitProps(props, ["onSubmit", "class"]);
  return (() => {
    const _el$ = _tmpl$$4.cloneNode(true);

    _el$.addEventListener("submit", e => preventDefault(local.onSubmit, e));

    spread(_el$, restProps, false, false);

    effect(() => _el$.className = cx("bx--form", local.class));

    return _el$;
  })();
};

const _tmpl$$5 = template(`<fieldset><legend class="bx--label"></legend></fieldset>`, 4),
      _tmpl$2$1 = template(`<div class="bx--form-requirement"></div>`, 2);
const FormGroup = allProps => {
  const [props, restProps] = splitProps(allProps, ["invalid", "children", "message", "legendText", "messageText", "class"]);
  return (() => {
    const _el$ = _tmpl$$5.cloneNode(true),
          _el$2 = _el$.firstChild;

    spread(_el$, restProps, false, true);

    insert(_el$2, () => props.legendText);

    insert(_el$, () => props.children, null);

    insert(_el$, (() => {
      const _c$ = memo(() => !!props.message, true);

      return () => _c$() && (() => {
        const _el$3 = _tmpl$2$1.cloneNode(true);

        insert(_el$3, () => props.messageText);

        return _el$3;
      })();
    })(), null);

    effect(_p$ => {
      const _v$ = props.invalid || undefined,
            _v$2 = cx("bx--fieldset", props.class);

      _v$ !== _p$._v$ && setAttribute(_el$, "data-invalid", _p$._v$ = _v$);
      _v$2 !== _p$._v$2 && (_el$.className = _p$._v$2 = _v$2);
      return _p$;
    }, {
      _v$: undefined,
      _v$2: undefined
    });

    return _el$;
  })();
};

const _tmpl$$6 = template(`<div></div>`, 2);
const FormItem = props => {
  const [local, restProps] = splitProps(props, ["class"]);
  return (() => {
    const _el$ = _tmpl$$6.cloneNode(true);

    spread(_el$, restProps, false, false);

    effect(() => _el$.className = cx("bx--form-item", local.class));

    return _el$;
  })();
};

const _tmpl$$7 = template(`<label></label>`, 2);
const FormLabel = props => {
  const [local, restProps] = splitProps(props, ["id", "class"]);
  return (() => {
    const _el$ = _tmpl$$7.cloneNode(true);

    spread(_el$, restProps, false, false);

    effect(_p$ => {
      const _v$ = cx("bx--label", local.class),
            _v$2 = local.id || uniqueId();

      _v$ !== _p$._v$ && (_el$.className = _p$._v$ = _v$);
      _v$2 !== _p$._v$2 && setAttribute(_el$, "for", _p$._v$2 = _v$2);
      return _p$;
    }, {
      _v$: undefined,
      _v$2: undefined
    });

    return _el$;
  })();
};

const _tmpl$$8 = template(`<main></main>`, 2);
const Content = props => {
  props = mergeProps({
    id: "main-content"
  }, props);
  const [local, restProps] = splitProps(props, ["class"]);
  return (() => {
    const _el$ = _tmpl$$8.cloneNode(true);

    spread(_el$, restProps, false, false);

    effect(() => _el$.className = `bx--content ${local.class || ""}`);

    return _el$;
  })();
};

const _tmpl$$9 = template(
    `<svg data-carbon-icon="Close20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M24 9.4L22.6 8 16 14.6 9.4 8 8 9.4 14.6 16 8 22.6 9.4 24 16 17.4 22.6 24 24 22.6 17.4 16 24 9.4z"></path></svg>`,
    4
  ),
  _tmpl$2$2 = template(`<title></title>`, 2);

const Close20 = (props) =>
  (() => {
    const _el$ = _tmpl$$9.cloneNode(true);
    _el$.firstChild;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$3 = _tmpl$2$2.cloneNode(true);

              insert(_el$3, () => props.title);

              return _el$3;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "20",
          _v$3 = props.height || "20",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

const _tmpl$$a = template(
    `<svg data-carbon-icon="Menu20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" preserveAspectRatio="xMidYMid meet"><path d="M2 14.8H18V16H2zM2 11.2H18V12.399999999999999H2zM2 7.6H18V8.799999999999999H2zM2 4H18V5.2H2z"></path></svg>`,
    4
  ),
  _tmpl$2$3 = template(`<title></title>`, 2);

const Menu20 = (props) =>
  (() => {
    const _el$ = _tmpl$$a.cloneNode(true);
    _el$.firstChild;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$3 = _tmpl$2$3.cloneNode(true);

              insert(_el$3, () => props.title);

              return _el$3;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "20",
          _v$3 = props.height || "20",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

const _tmpl$$b = template(`<button type="button" title="Open menu"></button>`, 2);
const HamburgerMenu = props => {
  const [local, restProps] = splitProps(props, ["openSignal", "class"]);
  let [isOpen, setOpen] = local.openSignal;
  return (() => {
    const _el$ = _tmpl$$b.cloneNode(true);

    _el$.$$click = () => setOpen(!isOpen());

    spread(_el$, restProps, false, true);

    insert(_el$, (() => {
      const _c$ = memo(() => !!isOpen(), true);

      return () => _c$() ? createComponent(Close20, {
        title: "Close"
      }) : createComponent(Menu20, {
        title: "Open Menu"
      });
    })());

    effect(() => _el$.className = cx("bx--header__action", "bx--header__menu-trigger", "bx--header__menu-toggle", local.class));

    return _el$;
  })();
};

delegateEvents(["click"]);

const [shouldRenderHamburgerMenu, setShouldRenderHamburgerMenu] = createSignal(true);

const _tmpl$$c = template(`<header role="banner" class="bx--header"></header>`, 2),
      _tmpl$2$4 = template(`<span class="bx--header__name--prefix">&nbsp;</span>`, 2);
const Header = allProps => {
  const props = mergeProps({
    expandedByDefault: true,
    persistentHamburgerMenu: false,
    "aria-label": undefined
  }, allProps);
  const [winWidth, setWinWidth] = createSignal(window.innerWidth);
  const [, setSideNavOpen] = props.sideNavOpenSignal;
  createEffect(() => {
    setSideNavOpen(props.expandedByDefault && winWidth() >= 1056 && !props.persistentHamburgerMenu);
  });

  const onResize = () => setWinWidth(window.innerWidth);

  onMount(() => window.addEventListener("resize", onResize));
  onCleanup(() => window.removeEventListener("resize", onResize));
  return (() => {
    const _el$ = _tmpl$$c.cloneNode(true);

    insert(_el$, () => props.skipToContent, null);

    insert(_el$, (() => {
      const _c$ = memo(() => shouldRenderHamburgerMenu(), true);

      return createComponent(Show, {
        get when() {
          return _c$() && winWidth() < 1056 || props.persistentHamburgerMenu;
        },

        get children() {
          return createComponent(HamburgerMenu, {
            get openSignal() {
              return props.sideNavOpenSignal;
            }

          });
        }

      });
    })(), null);

    insert(_el$, createComponent(UnstyledLink, {
      get href() {
        return props.href;
      },

      "class": "bx--header__name",

      get children() {
        return [memo((() => {
          const _c$2 = memo(() => !!props.company, true);

          return () => _c$2() && (() => {
            const _el$2 = _tmpl$2$4.cloneNode(true),
                  _el$3 = _el$2.firstChild;

            insert(_el$2, () => props.company, _el$3);

            return _el$2;
          })();
        })()), memo(() => props.platform)];
      }

    }), null);

    insert(_el$, () => props.children, null);

    effect(() => setAttribute(_el$, "aria-label", props["aria-label"]));

    return _el$;
  })();
};

const _tmpl$$d = template(`<nav><ul role="menubar" class="bx--header__menu-bar"></ul></nav>`, 4);
const HeaderNav = props => {
  const [local, restProps] = splitProps(props, ["children", "aria-label", "class"]);
  return (() => {
    const _el$ = _tmpl$$d.cloneNode(true),
          _el$2 = _el$.firstChild;

    spread(_el$, restProps, false, true);

    insert(_el$2, () => local.children);

    effect(_p$ => {
      const _v$ = local["aria-label"],
            _v$2 = cx("bx--header__nav", local.class),
            _v$3 = local["aria-label"];

      _v$ !== _p$._v$ && setAttribute(_el$, "aria-label", _p$._v$ = _v$);
      _v$2 !== _p$._v$2 && (_el$.className = _p$._v$2 = _v$2);
      _v$3 !== _p$._v$3 && setAttribute(_el$2, "aria-label", _p$._v$3 = _v$3);
      return _p$;
    }, {
      _v$: undefined,
      _v$2: undefined,
      _v$3: undefined
    });

    return _el$;
  })();
};

const _tmpl$$e = template(`<span class="bx--text-truncate--end"></span>`, 2),
      _tmpl$2$5 = template(`<li></li>`, 2);
const HeaderNavItem = props => {
  const [, restProps] = splitProps(props, ["href", "text", "children", "class"]);
  return (() => {
    const _el$ = _tmpl$2$5.cloneNode(true);

    insert(_el$, createComponent(UnstyledLink, mergeProps$1({
      role: "menuitem",
      tabindex: "0",

      get href() {
        return props.href;
      },

      get ["class"]() {
        return cx("bx--header__menu-item", props.class);
      }

    }, restProps, {
      get children() {
        const _el$2 = _tmpl$$e.cloneNode(true);

        insert(_el$2, () => props.text);

        return _el$2;
      }

    })));

    return _el$;
  })();
};

const _tmpl$$f = template(`<button type="button"></button>`, 2);
const HeaderGlobalAction = props => {
  return (() => {
    const _el$ = _tmpl$$f.cloneNode(true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(_el$, () => props.icon, null);

    insert(_el$, () => props.children, null);

    effect(_p$ => {
      const _v$ = cx("bx--header__action", props.isActive && "bx--header__action--active", props.class),
            _v$2 = props["aria-label"],
            _v$3 = props["aria-labelledby"];

      _v$ !== _p$._v$ && (_el$.className = _p$._v$ = _v$);
      _v$2 !== _p$._v$2 && setAttribute(_el$, "aria-label", _p$._v$2 = _v$2);
      _v$3 !== _p$._v$3 && setAttribute(_el$, "aria-labelledby", _p$._v$3 = _v$3);
      return _p$;
    }, {
      _v$: undefined,
      _v$2: undefined,
      _v$3: undefined
    });

    return _el$;
  })();
};

delegateEvents(["click"]);

const _tmpl$$g = template(`<div class="bx--header__global"></div>`, 2);

const HeaderUtilities = props => (() => {
  const _el$ = _tmpl$$g.cloneNode(true);

  insert(_el$, () => props.children);

  return _el$;
})();

const _tmpl$$h = template(`<nav></nav>`, 2),
      _tmpl$2$6 = template(`<div></div>`, 2);
const SideNav = props => {
  const [local, restProps] = splitProps(props, ["children", "fixed", "sideNavOpenSignal", "class"]);
  const [isOpen, setOpen] = local.sideNavOpenSignal;

  const close = () => {
    setOpen(false);
  };

  onMount(() => {
    setShouldRenderHamburgerMenu(true);
  });
  onCleanup(() => {
    setShouldRenderHamburgerMenu(false);
  });
  return () => [memo((() => {
    const _c$ = memo(() => !!local.fixed, true);

    return () => _c$() ? null : (() => {
      const _el$2 = _tmpl$2$6.cloneNode(true);

      addEventListener(_el$2, "click", close, true);

      effect(() => _el$2.className = cx("bx--side-nav__overlay", isOpen() && "bx--side-nav__overlay-active", local.class));

      return _el$2;
    })();
  })()), (() => {
    const _el$ = _tmpl$$h.cloneNode(true);

    spread(_el$, restProps, false, true);

    insert(_el$, () => local.children);

    effect(() => _el$.className = cx("bx--side-nav", isOpen() && "bx--side-nav--expanded", !isOpen() && local.fixed && "bx--side-nav--collapsed", "bx--side-nav__navigation", "bx--side-nav--ux", local.class));

    return _el$;
  })()];
};

delegateEvents(["click"]);

const _tmpl$$i = template(`<div></div>`, 2);
const SideNavIcon = props => (() => {
  const _el$ = _tmpl$$i.cloneNode(true);

  insert(_el$, () => props.children);

  effect(() => _el$.className = cx("bx--side-nav__icon", props.small && "bx--side-nav__item--small", props.class));

  return _el$;
})();

const _tmpl$$j = template(`<li></li>`, 2);
const SideNavItem = props => (() => {
  const _el$ = _tmpl$$j.cloneNode(true);

  insert(_el$, () => props.children);

  effect(() => _el$.className = cx("bx--side-nav__item", props.large && "bx--side-nav__item--large", props.class));

  return _el$;
})();

const _tmpl$$k = template(`<ul class="bx--side-nav__items"></ul>`, 2);

const SideNavItems = props => (() => {
  const _el$ = _tmpl$$k.cloneNode(true);

  insert(_el$, () => props.children);

  return _el$;
})();

const _tmpl$$l = template(`<span class="bx--side-nav__link-text"></span>`, 2);

const SideNavLinkText = props => (() => {
  const _el$ = _tmpl$$l.cloneNode(true);

  insert(_el$, () => props.children);

  return _el$;
})();

const SideNavLink = props => {
  const [, restProps] = splitProps(props, ["isSelected", "href", "text", "icon", "large", "class"]);
  return createComponent(SideNavItem, {
    get large() {
      return props.large;
    },

    get children() {
      return createComponent(UnstyledLink, mergeProps$1({
        get ["aria-current"]() {
          return props.isSelected ? "page" : undefined;
        },

        get href() {
          return props.href;
        },

        get ["class"]() {
          return cx("bx--side-nav__link", props.isSelected && "bx--side-nav__link--current", props.class);
        }

      }, restProps, {
        get children() {
          return [memo((() => {
            const _c$ = memo(() => !!props.icon, true);

            return () => _c$() && createComponent(SideNavIcon, {
              small: true,

              get children() {
                return props.icon();
              }

            });
          })()), createComponent(SideNavLinkText, {
            get children() {
              return props.text;
            }

          })];
        }

      }));
    }

  });
};

const _tmpl$$m = template(
    `<svg data-carbon-icon="View16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" preserveAspectRatio="xMidYMid meet"><path d="M15.5,7.8C14.3,4.7,11.3,2.6,8,2.5C4.7,2.6,1.7,4.7,0.5,7.8c0,0.1,0,0.2,0,0.3c1.2,3.1,4.1,5.2,7.5,5.3	c3.3-0.1,6.3-2.2,7.5-5.3C15.5,8.1,15.5,7.9,15.5,7.8z M8,12.5c-2.7,0-5.4-2-6.5-4.5c1-2.5,3.8-4.5,6.5-4.5s5.4,2,6.5,4.5	C13.4,10.5,10.6,12.5,8,12.5z"></path><path d="M8,5C6.3,5,5,6.3,5,8s1.3,3,3,3s3-1.3,3-3S9.7,5,8,5z M8,10c-1.1,0-2-0.9-2-2s0.9-2,2-2s2,0.9,2,2S9.1,10,8,10z"></path></svg>`,
    6
  ),
  _tmpl$2$7 = template(`<title></title>`, 2);

const View16 = (props) =>
  (() => {
    const _el$ = _tmpl$$m.cloneNode(true),
      _el$2 = _el$.firstChild;
    _el$2.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$4 = _tmpl$2$7.cloneNode(true);

              insert(_el$4, () => props.title);

              return _el$4;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "16",
          _v$3 = props.height || "16",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

const _tmpl$$n = template(
    `<svg data-carbon-icon="ViewOff16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" preserveAspectRatio="xMidYMid meet"><path d="M2.6,11.3l0.7-0.7C2.6,9.8,1.9,9,1.5,8c1-2.5,3.8-4.5,6.5-4.5c0.7,0,1.4,0.1,2,0.4l0.8-0.8C9.9,2.7,9,2.5,8,2.5	C4.7,2.6,1.7,4.7,0.5,7.8c0,0.1,0,0.2,0,0.3C1,9.3,1.7,10.4,2.6,11.3z"></path><path d="M6 7.9c.1-1 .9-1.8 1.8-1.8l.9-.9C7.2 4.7 5.5 5.6 5.1 7.2 5 7.7 5 8.3 5.1 8.8L6 7.9zM15.5 7.8c-.6-1.5-1.6-2.8-2.9-3.7L15 1.7 14.3 1 1 14.3 1.7 15l2.6-2.6c1.1.7 2.4 1 3.7 1.1 3.3-.1 6.3-2.2 7.5-5.3C15.5 8.1 15.5 7.9 15.5 7.8zM10 8c0 1.1-.9 2-2 2-.3 0-.7-.1-1-.3L9.7 7C9.9 7.3 10 7.6 10 8zM8 12.5c-1 0-2.1-.3-3-.8l1.3-1.3c1.4.9 3.2.6 4.2-.8.7-1 .7-2.4 0-3.4l1.4-1.4c1.1.8 2 1.9 2.6 3.2C13.4 10.5 10.6 12.5 8 12.5z"></path></svg>`,
    6
  ),
  _tmpl$2$8 = template(`<title></title>`, 2);

const ViewOff16 = (props) =>
  (() => {
    const _el$ = _tmpl$$n.cloneNode(true),
      _el$2 = _el$.firstChild;
    _el$2.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$4 = _tmpl$2$8.cloneNode(true);

              insert(_el$4, () => props.title);

              return _el$4;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "16",
          _v$3 = props.height || "16",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

const _tmpl$$o = template(
    `<svg data-carbon-icon="WarningFilled16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" preserveAspectRatio="xMidYMid meet"><path d="M8,1C4.2,1,1,4.2,1,8s3.2,7,7,7s7-3.1,7-7S11.9,1,8,1z M7.5,4h1v5h-1C7.5,9,7.5,4,7.5,4z M8,12.2	c-0.4,0-0.8-0.4-0.8-0.8s0.3-0.8,0.8-0.8c0.4,0,0.8,0.4,0.8,0.8S8.4,12.2,8,12.2z"></path><path d="M7.5,4h1v5h-1C7.5,9,7.5,4,7.5,4z M8,12.2c-0.4,0-0.8-0.4-0.8-0.8s0.3-0.8,0.8-0.8	c0.4,0,0.8,0.4,0.8,0.8S8.4,12.2,8,12.2z" data-icon-path="inner-path" opacity="0"></path></svg>`,
    6
  ),
  _tmpl$2$9 = template(`<title></title>`, 2);

const WarningFilled16 = (props) =>
  (() => {
    const _el$ = _tmpl$$o.cloneNode(true),
      _el$2 = _el$.firstChild;
    _el$2.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$4 = _tmpl$2$9.cloneNode(true);

              insert(_el$4, () => props.title);

              return _el$4;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "16",
          _v$3 = props.height || "16",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

const _tmpl$$p = template(
    `<svg data-carbon-icon="WarningAltFilled16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path fill="none" d="M14.875,11h2.25V21h-2.25ZM16,27a1.5,1.5,0,1,1,1.5-1.5A1.5,1.5,0,0,1,16,27Z"></path><path d="M29.8872,28.5386l-13-25a1,1,0,0,0-1.7744,0l-13,25A1,1,0,0,0,3,30H29a1,1,0,0,0,.8872-1.4614ZM14.875,11h2.25V21h-2.25ZM16,27a1.5,1.5,0,1,1,1.5-1.5A1.5,1.5,0,0,1,16,27Z"></path></svg>`,
    6
  ),
  _tmpl$2$a = template(`<title></title>`, 2);

const WarningAltFilled16 = (props) =>
  (() => {
    const _el$ = _tmpl$$p.cloneNode(true),
      _el$2 = _el$.firstChild;
    _el$2.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$4 = _tmpl$2$a.cloneNode(true);

              insert(_el$4, () => props.title);

              return _el$4;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "16",
          _v$3 = props.height || "16",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

const _tmpl$$q = template(`<div></div>`, 2),
      _tmpl$2$b = template(`<hr class="bx--text-input__divider">`, 1),
      _tmpl$3 = template(`<div class="bx--form-requirement"></div>`, 2),
      _tmpl$4 = template(`<input>`, 1),
      _tmpl$5 = template(`<div class="bx--text-input__label-helper-wrapper"></div>`, 2),
      _tmpl$6 = template(`<label></label>`, 2),
      _tmpl$7 = template(`<div><div></div></div>`, 4);

const inputClasses = (props, controlType) => cx("bx--text-input", controlType === "PasswordInput" && "bx--password-input", props.light && "bx--text-input--light", props.invalid && "bx--text-input--invalid", props.warn && "bx--text-input--warn", props.size === "sm" && "bx--text-input--sm", props.size === "xl" && "bx--text-input--xl");

const inputWrapperClasses = (props, controlType) => cx("bx--form-item", "bx--text-input-wrapper", props.light && "bx--text-input-wrapper--light", props.inline && "bx--text-input-wrapper--inline", controlType === "PasswordInput" && "bx--password-input-wrapper");

const fieldWrapperClasses = props => cx("bx--text-input__field-wrapper", !props.invalid && props.warn && "bx--text-input__field-wrapper--warning");

const labelClasses = props => cx("bx--label", props.hideLabel && "bx--visually-hidden", props.disabled && "bx--label--disabled", props.inline && "bx--label--inline", props.inline && props.size === "sm" && "bx--label--inline--sm", props.inline && props.size === "xl" && "bx--label--inline--xl");

const renderFieldWrapper = (props, input) => {
  return (() => {
    const _el$ = _tmpl$$q.cloneNode(true);

    insert(_el$, (() => {
      const _c$ = memo(() => !!props.invalid, true);

      return () => _c$() && createComponent(WarningFilled16, {
        "class": "bx--text-input__invalid-icon"
      });
    })(), null);

    insert(_el$, (() => {
      const _c$2 = memo(() => !!(!props.invalid && props.warn), true);

      return () => _c$2() && createComponent(WarningAltFilled16, {
        "class": "bx--text-input__invalid-icon bx--text-input__invalid-icon--warning"
      });
    })(), null);

    insert(_el$, input, null);

    insert(_el$, (() => {
      const _c$3 = memo(() => !!props.fluid, true);

      return () => _c$3() && _tmpl$2$b.cloneNode(true);
    })(), null);

    insert(_el$, (() => {
      const _c$4 = memo(() => !!(props.fluid && !props.inline), true);

      return () => _c$4() && renderFormRequirement(props);
    })(), null);

    effect(_p$ => {
      const _v$ = props.invalid || undefined,
            _v$2 = fieldWrapperClasses(props);

      _v$ !== _p$._v$ && setAttribute(_el$, "data-invalid", _p$._v$ = _v$);
      _v$2 !== _p$._v$2 && (_el$.className = _p$._v$2 = _v$2);
      return _p$;
    }, {
      _v$: undefined,
      _v$2: undefined
    });

    return _el$;
  })();
};

const renderFormRequirement = props => props.invalid || props.warn ? (() => {
  const _el$3 = _tmpl$3.cloneNode(true);

  insert(_el$3, () => props.invalid ? props.invalidText : props.warn ? props.warnText : undefined);

  effect(() => setAttribute(_el$3, "id", props.invalid ? props.errorId : props.warn ? props.warnId : undefined));

  return _el$3;
})() : null;

const renderHelperText = props => props.helperText ? (() => {
  const _el$4 = _tmpl$$q.cloneNode(true);

  insert(_el$4, () => props.helperText);

  effect(() => _el$4.className = helperTextClasses(props));

  return _el$4;
})() : null;

const helperTextClasses = props => cx("bx--form__helper-text", props.disabled && "bx--form__helper-text--disabled", props.inline && "bx--form__helper-text--inline");

const fieldOuterWrapperClasses = props => cx("bx--text-input__field-outer-wrapper", props.inline && "bx--text-input__field-outer-wrapper--inline");

const renderInput = (props, typeFn, controlType) => {
  const [value, setValue] = props.value();
  return (() => {
    const _el$5 = _tmpl$4.cloneNode(true);

    addEventListener(_el$5, "blur", props.onBlur);

    _el$5.$$input = ({
      target
    }) => {
      setValue(target.value);
    };

    setAttribute(_el$5, "data-toggle-password-visibility", controlType === "PasswordInput" ? "true" : undefined);

    effect(_p$ => {
      const _v$3 = props.invalid || undefined,
            _v$4 = props.invalid || undefined,
            _v$5 = props.invalid ? props.errorId : props.warn ? props.warnId : undefined,
            _v$6 = props.placeholder,
            _v$7 = props.disabled,
            _v$8 = props.id,
            _v$9 = props.name,
            _v$10 = props.placeholder,
            _v$11 = typeFn(),
            _v$12 = value(),
            _v$13 = props.required,
            _v$14 = inputClasses(props, controlType),
            _v$15 = props.tabindex;

      _v$3 !== _p$._v$3 && setAttribute(_el$5, "data-invalid", _p$._v$3 = _v$3);
      _v$4 !== _p$._v$4 && setAttribute(_el$5, "aria-invalid", _p$._v$4 = _v$4);
      _v$5 !== _p$._v$5 && setAttribute(_el$5, "aria-describedby", _p$._v$5 = _v$5);
      _v$6 !== _p$._v$6 && setAttribute(_el$5, "title", _p$._v$6 = _v$6);
      _v$7 !== _p$._v$7 && (_el$5.disabled = _p$._v$7 = _v$7);
      _v$8 !== _p$._v$8 && setAttribute(_el$5, "id", _p$._v$8 = _v$8);
      _v$9 !== _p$._v$9 && setAttribute(_el$5, "name", _p$._v$9 = _v$9);
      _v$10 !== _p$._v$10 && setAttribute(_el$5, "placeholder", _p$._v$10 = _v$10);
      _v$11 !== _p$._v$11 && setAttribute(_el$5, "type", _p$._v$11 = _v$11);
      _v$12 !== _p$._v$12 && (_el$5.value = _p$._v$12 = _v$12);
      _v$13 !== _p$._v$13 && (_el$5.required = _p$._v$13 = _v$13);
      _v$14 !== _p$._v$14 && (_el$5.className = _p$._v$14 = _v$14);
      _v$15 !== _p$._v$15 && setAttribute(_el$5, "tabindex", _p$._v$15 = _v$15);
      return _p$;
    }, {
      _v$3: undefined,
      _v$4: undefined,
      _v$5: undefined,
      _v$6: undefined,
      _v$7: undefined,
      _v$8: undefined,
      _v$9: undefined,
      _v$10: undefined,
      _v$11: undefined,
      _v$12: undefined,
      _v$13: undefined,
      _v$14: undefined,
      _v$15: undefined
    });

    return _el$5;
  })();
};

const renderLabel = props => props.inline ? renderInlineLabel(props) : renderNormalLabel(props);

const renderInlineLabel = props => (() => {
  const _el$6 = _tmpl$5.cloneNode(true);

  insert(_el$6, () => renderNormalLabel(props), null);

  insert(_el$6, (() => {
    const _c$5 = memo(() => !!!props.fluid, true);

    return () => _c$5() && renderHelperText(props);
  })(), null);

  return _el$6;
})();

const renderNormalLabel = props => props.labelText ? (() => {
  const _el$7 = _tmpl$6.cloneNode(true);

  insert(_el$7, () => props.labelText);

  effect(_p$ => {
    const _v$16 = props.id,
          _v$17 = labelClasses(props);

    _v$16 !== _p$._v$16 && setAttribute(_el$7, "for", _p$._v$16 = _v$16);
    _v$17 !== _p$._v$17 && (_el$7.className = _p$._v$17 = _v$17);
    return _p$;
  }, {
    _v$16: undefined,
    _v$17: undefined
  });

  return _el$7;
})() : null;

const renderControl = (props, controlType, typeFn, afterInput) => (() => {
  const _el$8 = _tmpl$7.cloneNode(true),
        _el$9 = _el$8.firstChild;

  insert(_el$8, () => renderLabel(props), _el$9);

  insert(_el$9, () => renderFieldWrapper(props, [memo(() => renderInput(props, typeFn, controlType)), afterInput]), null);

  insert(_el$9, (() => {
    const _c$6 = memo(() => !!!props.fluid, true);

    return () => _c$6() && renderFormRequirement(props);
  })(), null);

  insert(_el$9, (() => {
    const _c$7 = memo(() => !!(!props.fluid && !props.invalid && !props.warn && !props.inline), true);

    return () => _c$7() && renderHelperText(props);
  })(), null);

  effect(_p$ => {
    const _v$18 = props.style,
          _v$19 = inputWrapperClasses(props, controlType),
          _v$20 = fieldOuterWrapperClasses(props);

    _p$._v$18 = style(_el$8, _v$18, _p$._v$18);
    _v$19 !== _p$._v$19 && (_el$8.className = _p$._v$19 = _v$19);
    _v$20 !== _p$._v$20 && (_el$9.className = _p$._v$20 = _v$20);
    return _p$;
  }, {
    _v$18: undefined,
    _v$19: undefined,
    _v$20: undefined
  });

  return _el$8;
})();

delegateEvents(["input"]);

const _tmpl$$r = template(`<button type="button"><span class="bx--assistive-text"></span></button>`, 4);
const PasswordInput = allProps => {
  const defaultId = createMemo(() => uniqueId());
  const props = mergeProps({
    type: "password",
    hidePasswordLabel: "Hide password",
    showPasswordLabel: "Show password",

    get id() {
      return defaultId();
    },

    get warnId() {
      return `warn-${props.id}`;
    },

    get errorId() {
      return `error-${props.id}`;
    },

    value: () => createSignal("")
  }, allProps, // XXX: PasswordInput cannot be fluid (yet). Don't put it inside a <FluidForm>
  {
    fluid: false
  });
  const [type, setType] = createSignal(props.type);

  const togglePasswordVisibility = () => {
    setType(type() === "password" ? "text" : "password");
  };

  const passwordIsVisible = createMemo(() => type() === "text");

  const renderButton = () => (() => {
    const _el$ = _tmpl$$r.cloneNode(true),
          _el$2 = _el$.firstChild;

    addEventListener(_el$, "click", togglePasswordVisibility, true);

    insert(_el$2, () => passwordIsVisible() ? props.hidePasswordLabel : props.showPasswordLabel);

    insert(_el$, (() => {
      const _c$ = memo(() => !!passwordIsVisible(), true);

      return () => _c$() ? createComponent(ViewOff16, {
        "class": "bx--icon-visibility-off"
      }) : createComponent(View16, {
        "class": "bx--icon-visibility-on"
      });
    })(), null);

    effect(() => _el$.className = passwordVisibilityToggleClasses(props));

    return _el$;
  })();

  return renderControl(props, "PasswordInput", type, renderButton());
};

const passwordVisibilityToggleClasses = props => cx("bx--text-input--password__visibility__toggle", "bx--btn--icon-only", "bx--tooltip__trigger", "bx--tooltip--a11y", tooltipPositionClass(props.tooltipPosition), tooltipAlignmentClass(props.tooltipAlignment));

delegateEvents(["click"]);

const TextInput = allProps => {
  const defaultId = createMemo(() => uniqueId());
  const fluidContext = {
    fluid: false
  }; // XXX

  const props = mergeProps({
    type: "text",

    get id() {
      return defaultId();
    },

    get warnId() {
      return `warn-${props.id}`;
    },

    get errorId() {
      return `error-${props.id}`;
    },

    fluid: fluidContext.fluid,
    value: () => createSignal("")
  }, allProps);
  return renderControl(props, "TextInput", () => props.type, null);
};

const _tmpl$$s = template(`<div><div class="bx--skeleton bx--text-area"></div></div>`, 4),
      _tmpl$2$c = template(`<span class="bx--label bx--skeleton"></span>`, 2);
const TextInputSkeleton = allProps => {
  const [props, restProps] = splitProps(allProps, ["hideLabel", "class"]);
  return (() => {
    const _el$ = _tmpl$$s.cloneNode(true),
          _el$2 = _el$.firstChild;

    spread(_el$, restProps, false, true);

    insert(_el$, (() => {
      const _c$ = memo(() => !!!props.hideLabel, true);

      return () => _c$() && _tmpl$2$c.cloneNode(true);
    })(), _el$2);

    effect(() => _el$.className = cx("bx--form-item", props.class));

    return _el$;
  })();
};

const _tmpl$$t = template(`<div><span class="bx--checkbox-label-text bx--skeleton"></span></div>`, 4);
const CheckboxSkeleton = props => {
  const [local, restProps] = splitProps(props, ["class"]);
  return (() => {
    const _el$ = _tmpl$$t.cloneNode(true);

    spread(_el$, restProps, false, true);

    effect(() => _el$.className = cx("bx--form-item", "bx--checkbox-wrapper", "bx--checkbox-label", local.class));

    return _el$;
  })();
};

const _tmpl$$u = template(`<div class="bx--form-requirement"></div>`, 2),
      _tmpl$2$d = template(`<div><input type="checkbox" class="bx--checkbox"><label class="bx--checkbox-label"><span></span></label></div>`, 7);
const Checkbox = allProps => {
  const [ownProps, restProps] = splitProps(allProps, ["checked", "indeterminate", "skeleton", "readonly", "disabled", "labelText", "hideLabel", "name", "title", "id", "setChecked", "invalid", "invalidText", "onBlur", "class"]);
  const props = mergeProps({
    checked: false,
    indeterminate: false,
    skeleton: false,
    readonly: false,
    disabled: false,
    labelText: "",
    hideLabel: false,
    title: undefined,
    name: "",
    invalid: false,
    invalidText: undefined
  }, ownProps);
  const id = createMemo(() => props.id || uniqueId());
  const errorId = createMemo(() => `error-${id()}`);

  const onChange = () => {
    const setter = props.setChecked;
    if (setter) setter(!props.checked);
  };

  return (() => {
    const _c$ = memo(() => !!props.skeleton, true);

    return () => _c$() ? createComponent(CheckboxSkeleton, mergeProps$1({
      get ["class"]() {
        return props.class;
      }

    }, restProps, {})) : (() => {
      const _el$ = _tmpl$2$d.cloneNode(true),
            _el$2 = _el$.firstChild,
            _el$3 = _el$2.nextSibling,
            _el$4 = _el$3.firstChild;

      spread(_el$, restProps, false, true);

      addEventListener(_el$2, "blur", props.onBlur);

      addEventListener(_el$2, "change", onChange);

      insert(_el$4, () => props.labelText);

      insert(_el$, createComponent(Show, {
        get when() {
          return props.invalid;
        },

        get children() {
          const _el$5 = _tmpl$$u.cloneNode(true);

          insert(_el$5, () => props.invalidText);

          effect(() => setAttribute(_el$5, "id", errorId()));

          return _el$5;
        }

      }), null);

      effect(_p$ => {
        const _v$ = cx("bx--form-item", "bx--checkbox-wrapper", props.class),
              _v$2 = props.invalid || undefined,
              _v$3 = props.invalid ? errorId() : undefined,
              _v$4 = props.checked,
              _v$5 = props.disabled,
              _v$6 = id(),
              _v$7 = props.indeterminate,
              _v$8 = props.name,
              _v$9 = props.readonly,
              _v$10 = id(),
              _v$11 = props.title,
              _v$12 = cx("bx--checkbox-label-text", props.hideLabel && "bx--visually-hidden");

        _v$ !== _p$._v$ && (_el$.className = _p$._v$ = _v$);
        _v$2 !== _p$._v$2 && setAttribute(_el$2, "data-invalid", _p$._v$2 = _v$2);
        _v$3 !== _p$._v$3 && setAttribute(_el$2, "aria-describedby", _p$._v$3 = _v$3);
        _v$4 !== _p$._v$4 && (_el$2.checked = _p$._v$4 = _v$4);
        _v$5 !== _p$._v$5 && (_el$2.disabled = _p$._v$5 = _v$5);
        _v$6 !== _p$._v$6 && setAttribute(_el$2, "id", _p$._v$6 = _v$6);
        _v$7 !== _p$._v$7 && (_el$2.indeterminate = _p$._v$7 = _v$7);
        _v$8 !== _p$._v$8 && setAttribute(_el$2, "name", _p$._v$8 = _v$8);
        _v$9 !== _p$._v$9 && (_el$2.readonly = _p$._v$9 = _v$9);
        _v$10 !== _p$._v$10 && setAttribute(_el$3, "for", _p$._v$10 = _v$10);
        _v$11 !== _p$._v$11 && setAttribute(_el$3, "title", _p$._v$11 = _v$11);
        _v$12 !== _p$._v$12 && (_el$4.className = _p$._v$12 = _v$12);
        return _p$;
      }, {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined
      });

      return _el$;
    })();
  })();
};

const _tmpl$$v = template(`<input type="checkbox">`, 1),
      _tmpl$2$e = template(`<label class="bx--checkbox-label"></label>`, 2);
const InlineCheckbox = allProps => {
  const [ownProps, restProps] = splitProps(allProps, ["checked", "indeterminate", "title", "id", "aria-label", "class"]);
  const props = mergeProps({
    checked: false,
    indeterminate: false,
    title: undefined,
    id: undefined,
    "aria-label": undefined
  }, ownProps);
  const id = createMemo(() => props.id || uniqueId());
  return [(() => {
    const _el$ = _tmpl$$v.cloneNode(true);

    spread(_el$, restProps, false, false);

    effect(_p$ => {
      const _v$ = cx("bx--checkbox", props.class),
            _v$2 = props.indeterminate ? false : props.checked,
            _v$3 = props.indeterminate,
            _v$4 = id(),
            _v$5 = props.indeterminate ? "mixed" : props.checked;

      _v$ !== _p$._v$ && (_el$.className = _p$._v$ = _v$);
      _v$2 !== _p$._v$2 && (_el$.checked = _p$._v$2 = _v$2);
      _v$3 !== _p$._v$3 && (_el$.indeterminate = _p$._v$3 = _v$3);
      _v$4 !== _p$._v$4 && setAttribute(_el$, "id", _p$._v$4 = _v$4);
      _v$5 !== _p$._v$5 && setAttribute(_el$, "aria-checked", _p$._v$5 = _v$5);
      return _p$;
    }, {
      _v$: undefined,
      _v$2: undefined,
      _v$3: undefined,
      _v$4: undefined,
      _v$5: undefined
    });

    return _el$;
  })(), (() => {
    const _el$2 = _tmpl$2$e.cloneNode(true);

    effect(_p$ => {
      const _v$6 = id(),
            _v$7 = props.title,
            _v$8 = props["aria-label"];

      _v$6 !== _p$._v$6 && setAttribute(_el$2, "for", _p$._v$6 = _v$6);
      _v$7 !== _p$._v$7 && setAttribute(_el$2, "title", _p$._v$7 = _v$7);
      _v$8 !== _p$._v$8 && setAttribute(_el$2, "aria-label", _p$._v$8 = _v$8);
      return _p$;
    }, {
      _v$6: undefined,
      _v$7: undefined,
      _v$8: undefined
    });

    return _el$2;
  })()];
};

const _tmpl$$w = template(`<button type="button" class="bx--modal-close"></button>`, 2),
      _tmpl$2$f = template(`<div class="bx--modal-header"><h3 class="bx--modal-header__heading"></h3></div>`, 4),
      _tmpl$3$1 = template(`<h2 class="bx--modal-header__label"></h2>`, 2),
      _tmpl$4$1 = template(`<div></div>`, 2),
      _tmpl$5$1 = template(`<div class="bx--modal-footer"></div>`, 2),
      _tmpl$6$1 = template(`<div role="presentation" tabindex="-1"><div aria-modal="true"></div></div>`, 4),
      _tmpl$7$1 = template(`<div class="bx--modal-content--overflow-indicator"></div>`, 2);
const Modal = allProps => {
  const props = mergeProps({
    size: undefined,
    open: false,
    danger: false,
    alert: false,
    passiveModal: false,
    modalHeading: undefined,
    modalLabel: undefined,
    modalAriaLabel: undefined,
    iconDescription: "Close the modal",
    hasForm: false,
    hasScrollingContent: false,
    primaryButtonText: "",
    primaryButtonDisabled: false,
    shouldSubmitOnEnter: true,
    secondaryButtonText: "",
    selectorPrimaryFocus: "[data-modal-primary-focus]",
    preventCloseOnClickOutside: false,
    id: undefined,
    "aria-label": undefined,
    children: undefined,
    onOpen: undefined,
    onClose: undefined,
    onSubmit: undefined,
    onClickSecondaryButton: undefined,
    hideCloseButton: false,
    class: undefined
  }, allProps);
  const id = createMemo(() => props.id || uniqueId());
  const modalLabelId = createMemo(() => `bx--modal-header__label--modal-${id()}`);
  const modalHeadingId = createMemo(() => `bx--modal-header__heading--modal-${id()}`);
  const modalBodyId = createMemo(() => `bx--modal-body--${id()}`);
  const ariaLabel = createMemo(() => props.modalLabel || props["aria-label"] || props.modalAriaLabel || props.modalHeading);
  const alertDialogProps = {
    role: createMemo(() => props.alert ? props.passiveModal ? "alert" : "alertdialog" : undefined),
    ariaDescribedBy: createMemo(() => props.alert && !props.passiveModal ? modalBodyId() : undefined)
  };
  onMount(() => {
    document.body.classList.remove("bx--body--with-modal-open");
  });
  const [opened, setOpened] = createSignal(false);
  const [didClickInnerModal, setDidClickInnerModal] = createSignal(false); // Custom event dispatchers

  const dispatchOpen = () => props.onOpen && props.onOpen();

  const dispatchClose = () => props.onClose && props.onClose();

  const dispatchClickSecondaryButton = () => props.onClickSecondaryButton && props.onClickSecondaryButton();

  const dispatchSubmit = () => props.onSubmit && props.onSubmit();

  const closeDialog = () => {
    setOpened(false);
    dispatchClose();
    document.body.classList.remove("bx--body--with-modal-open");
  };

  const onKeyDown = ({
    key
  }) => {
    if (props.open) {
      if (key === "Escape") {
        closeDialog();
      } else if (props.shouldSubmitOnEnter && key === "Enter") {
        dispatchSubmit();
      }
    }
  };

  const onClick = () => {
    if (!didClickInnerModal() && !props.preventCloseOnClickOutside) {
      closeDialog();
    }

    setDidClickInnerModal(false);
  };

  var innerModal = null;
  var buttonRef = null;

  const focus = element => {
    const node = element.querySelector(props.selectorPrimaryFocus) || buttonRef;
    if (node) node.focus();
  };

  createEffect(() => {
    if (opened()) {
      if (!props.open) {
        closeDialog();
      }
    } else if (props.open) {
      setOpened(true);
      focus(innerModal);
      dispatchOpen();
      document.body.classList.add("bx--body--with-modal-open");
    }
  });

  const closeButton = () => props.hideCloseButton ? undefined : (() => {
    const _el$ = _tmpl$$w.cloneNode(true);

    addEventListener(_el$, "click", closeDialog, true);

    const _ref$ = buttonRef;
    typeof _ref$ === "function" ? _ref$(_el$) : buttonRef = _el$;

    insert(_el$, createComponent(Close20, {
      get ["aria-label"]() {
        return props.iconDescription;
      },

      "class": "bx--modal-close__icon"
    }));

    effect(_p$ => {
      const _v$ = props.iconDescription,
            _v$2 = props.iconDescription;
      _v$ !== _p$._v$ && setAttribute(_el$, "aria-label", _p$._v$ = _v$);
      _v$2 !== _p$._v$2 && setAttribute(_el$, "title", _p$._v$2 = _v$2);
      return _p$;
    }, {
      _v$: undefined,
      _v$2: undefined
    });

    return _el$;
  })();

  const modalHeaderDiv = () => (() => {
    const _el$2 = _tmpl$2$f.cloneNode(true),
          _el$3 = _el$2.firstChild;

    insert(_el$2, (() => {
      const _c$ = memo(() => !!props.passiveModal, true);

      return () => _c$() && closeButton();
    })(), _el$3);

    insert(_el$2, (() => {
      const _c$2 = memo(() => !!props.modalLabel, true);

      return () => _c$2() && (() => {
        const _el$4 = _tmpl$3$1.cloneNode(true);

        insert(_el$4, () => props.modalLabel);

        effect(() => setAttribute(_el$4, "id", modalLabelId()));

        return _el$4;
      })();
    })(), _el$3);

    insert(_el$3, () => props.modalHeading);

    insert(_el$2, (() => {
      const _c$3 = memo(() => !!!props.passiveModal, true);

      return () => _c$3() && closeButton();
    })(), null);

    effect(() => setAttribute(_el$3, "id", modalHeadingId()));

    return _el$2;
  })();

  const modalBody = () => (() => {
    const _el$5 = _tmpl$4$1.cloneNode(true);

    insert(_el$5, () => props.children);

    effect(_p$ => {
      const _v$3 = modalBodyId(),
            _v$4 = cx("bx--modal-content", props.hasForm && "bx--modal-content--with-form", props.hasScrollingContent && "bx--modal-scroll-content"),
            _v$5 = props.hasScrollingContent ? "0" : undefined,
            _v$6 = props.hasScrollingContent ? "region" : undefined,
            _v$7 = props.hasScrollingContent ? ariaLabel() : undefined,
            _v$8 = props.modalLabel ? modalLabelId() : modalHeadingId();

      _v$3 !== _p$._v$3 && setAttribute(_el$5, "id", _p$._v$3 = _v$3);
      _v$4 !== _p$._v$4 && (_el$5.className = _p$._v$4 = _v$4);
      _v$5 !== _p$._v$5 && setAttribute(_el$5, "tabindex", _p$._v$5 = _v$5);
      _v$6 !== _p$._v$6 && setAttribute(_el$5, "role", _p$._v$6 = _v$6);
      _v$7 !== _p$._v$7 && setAttribute(_el$5, "aria-label", _p$._v$7 = _v$7);
      _v$8 !== _p$._v$8 && setAttribute(_el$5, "aria-labelledby", _p$._v$8 = _v$8);
      return _p$;
    }, {
      _v$3: undefined,
      _v$4: undefined,
      _v$5: undefined,
      _v$6: undefined,
      _v$7: undefined,
      _v$8: undefined
    });

    return _el$5;
  })();

  const modalFooter = () => (() => {
    const _el$6 = _tmpl$5$1.cloneNode(true);

    insert(_el$6, createComponent(Button, {
      kind: "secondary",
      onClick: () => dispatchClickSecondaryButton(),

      get children() {
        return props.secondaryButtonText;
      }

    }), null);

    insert(_el$6, createComponent(Button, {
      get kind() {
        return props.danger ? "danger" : "primary";
      },

      get disabled() {
        return props.primaryButtonDisabled;
      },

      onClick: () => dispatchSubmit(),

      get children() {
        return props.primaryButtonText;
      }

    }), null);

    return _el$6;
  })();

  return (() => {
    const _el$7 = _tmpl$6$1.cloneNode(true),
          _el$8 = _el$7.firstChild;

    addEventListener(_el$7, "click", onClick, true);

    addEventListener(_el$7, "keydown", onKeyDown, true);

    _el$8.$$click = () => setDidClickInnerModal(true);

    const _ref$2 = innerModal;
    typeof _ref$2 === "function" ? _ref$2(_el$8) : innerModal = _el$8;

    insert(_el$8, modalHeaderDiv, null);

    insert(_el$8, modalBody, null);

    insert(_el$8, (() => {
      const _c$4 = memo(() => !!props.hasScrollingContent, true);

      return () => _c$4() && _tmpl$7$1.cloneNode(true);
    })(), null);

    insert(_el$8, (() => {
      const _c$5 = memo(() => !!!props.passiveModal, true);

      return () => _c$5() && modalFooter();
    })(), null);

    effect(_p$ => {
      const _v$9 = id(),
            _v$10 = cx("bx--modal", !props.passiveModal && "bx--modal-tall", props.open && "is-visible", props.danger && "bx--modal--danger", props.class),
            _v$11 = alertDialogProps.role() || "dialog",
            _v$12 = alertDialogProps.ariaDescribedBy(),
            _v$13 = ariaLabel(),
            _v$14 = cx("bx--modal-container", props.size === "xs" && "bx--modal-container--xs", props.size === "sm" && "bx--modal-container--sm", props.size === "lg" && "bx--modal-container--lg");

      _v$9 !== _p$._v$9 && setAttribute(_el$7, "id", _p$._v$9 = _v$9);
      _v$10 !== _p$._v$10 && (_el$7.className = _p$._v$10 = _v$10);
      _v$11 !== _p$._v$11 && setAttribute(_el$8, "role", _p$._v$11 = _v$11);
      _v$12 !== _p$._v$12 && setAttribute(_el$8, "aria-describedby", _p$._v$12 = _v$12);
      _v$13 !== _p$._v$13 && setAttribute(_el$8, "aria-label", _p$._v$13 = _v$13);
      _v$14 !== _p$._v$14 && (_el$8.className = _p$._v$14 = _v$14);
      return _p$;
    }, {
      _v$9: undefined,
      _v$10: undefined,
      _v$11: undefined,
      _v$12: undefined,
      _v$13: undefined,
      _v$14: undefined
    });

    return _el$7;
  })();
};

delegateEvents(["click", "keydown"]);

const AllThemes = ["white", "g10", "g90", "g100"];
const isValidTheme = theme => AllThemes.includes(theme);
const Theme = props => {
  createEffect(() => {
    const theme = props.theme;

    if (isValidTheme(theme)) {
      document.documentElement.setAttribute("theme", theme);
    }
  });
  return props.children;
};

const _tmpl$$x = template(`<div></div>`, 2),
      _tmpl$2$g = template(`<div class="bx--data-table-header"><h4 class="bx--data-table-header__title"></h4><p class="bx--data-table-header__description"></p></div>`, 6);
const tableContainerDefaultProps = {
  title: "",
  description: "",
  stickyHeader: false,
  children: undefined,
  class: undefined
};
const TableContainer = allProps => {
  const [props, restProps] = extractProps(allProps, tableContainerDefaultProps);
  return (() => {
    const _el$ = _tmpl$$x.cloneNode(true);

    spread(_el$, restProps, false, true);

    insert(_el$, (() => {
      const _c$ = memo(() => !!props.title, true);

      return () => _c$() ? (() => {
        const _el$2 = _tmpl$2$g.cloneNode(true),
              _el$3 = _el$2.firstChild,
              _el$4 = _el$3.nextSibling;

        insert(_el$3, () => props.title);

        insert(_el$4, () => props.description);

        return _el$2;
      })() : undefined;
    })(), null);

    insert(_el$, () => props.children, null);

    effect(() => _el$.className = cx("bx--data-table-container", props.stickyHeader && "bx--data-table--max-width", props.class));

    return _el$;
  })();
};

const _tmpl$$y = template(`<section class="bx--data-table_inner-container"><table></table></section>`, 4),
      _tmpl$2$h = template(`<table></table>`, 2);

const tableClasses = props => cx("bx--data-table", props.size === "compact" && "bx--data-table--compact", props.size === "short" && "bx--data-table--short", props.size === "tall" && "bx--data-table--tall", props.sortable && "bx--data-table--sort", props.zebra && "bx--data-table--zebra", props.useStaticWidth && "bx--data-table--static", !props.shouldShowBorder && "bx--data-table--no-border", props.stickyHeader && "bx--data-table--sticky-header", props.class);

const tableDefaultProps = {
  size: undefined,
  zebra: false,
  useStaticWidth: false,
  shouldShowBorder: false,
  sortable: false,
  stickyHeader: false,
  children: undefined,
  class: undefined
};
const Table = allProps => {
  const [props, restProps] = extractProps(allProps, tableDefaultProps);
  return () => props.stickyHeader ? (() => {
    const _el$ = _tmpl$$y.cloneNode(true),
          _el$2 = _el$.firstChild;

    spread(_el$, restProps, false, true);

    insert(_el$2, () => props.children);

    effect(() => _el$2.className = tableClasses(props));

    return _el$;
  })() : (() => {
    const _el$3 = _tmpl$2$h.cloneNode(true);

    spread(_el$3, restProps, false, true);

    insert(_el$3, () => props.children);

    effect(() => _el$3.className = tableClasses(props));

    return _el$3;
  })();
};

const _tmpl$$z = template(
    `<svg data-carbon-icon="ArrowUp20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M16 4L6 14 7.41 15.41 15 7.83 15 28 17 28 17 7.83 24.59 15.41 26 14 16 4z"></path></svg>`,
    4
  ),
  _tmpl$2$i = template(`<title></title>`, 2);

const ArrowUp20 = (props) =>
  (() => {
    const _el$ = _tmpl$$z.cloneNode(true);
    _el$.firstChild;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$3 = _tmpl$2$i.cloneNode(true);

              insert(_el$3, () => props.title);

              return _el$3;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "20",
          _v$3 = props.height || "20",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

const _tmpl$$A = template(
    `<svg data-carbon-icon="ArrowsVertical20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path d="M27.6 20.6L24 24.2 24 4 22 4 22 24.2 18.4 20.6 17 22 23 28 29 22zM9 4L3 10 4.4 11.4 8 7.8 8 28 10 28 10 7.8 13.6 11.4 15 10z"></path></svg>`,
    4
  ),
  _tmpl$2$j = template(`<title></title>`, 2);

const ArrowsVertical20 = (props) =>
  (() => {
    const _el$ = _tmpl$$A.cloneNode(true);
    _el$.firstChild;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$3 = _tmpl$2$j.cloneNode(true);

              insert(_el$3, () => props.title);

              return _el$3;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "20",
          _v$3 = props.height || "20",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

const _tmpl$$B = template(`<th><button><div class="bx--table-header-label"></div></button></th>`, 6),
      _tmpl$2$k = template(`<th><div class="bx--table-header-label"></div></th>`, 4);
const tableHeaderDefaultProps = {
  scope: "col",
  translateWithId: () => "",
  onClick: undefined,
  id: undefined,
  key: undefined,
  children: undefined,
  tableSortable: undefined,
  // XXX: required
  sortHeader: {} // XXX: required

};
const TableHeader = allProps => {
  const [props, restProps] = extractProps(allProps, tableHeaderDefaultProps);
  const id = createMemo(() => props.id || uniqueId());
  const active = createMemo(() => props.sortHeader.key === props.key && props.sortHeader.sortDirection !== "none");
  const ariaLabel = createMemo(() => props.translateWithId()); // TODO

  const renderTableSortable = () => (() => {
    const _el$ = _tmpl$$B.cloneNode(true),
          _el$2 = _el$.firstChild,
          _el$3 = _el$2.firstChild;

    spread(_el$, restProps, false, true);

    addEventListener(_el$2, "click", props.onClick, true);

    insert(_el$3, () => props.children);

    insert(_el$2, createComponent(ArrowUp20, {
      get ["aria-label"]() {
        return ariaLabel();
      },

      "class": "bx--table-sort__icon"
    }), null);

    insert(_el$2, createComponent(ArrowsVertical20, {
      get ["aria-label"]() {
        return ariaLabel();
      },

      "class": "bx--table-sort__icon-unsorted"
    }), null);

    effect(_p$ => {
      const _v$ = active() ? props.sortHeader.sortDirection : "none",
            _v$2 = props.scope,
            _v$3 = id(),
            _v$4 = {
        "bx--table-sort": true,
        "bx--table-sort--active": active(),
        "bx--table-sort--ascending": active() && props.sortHeader.sortDirection === "ascending"
      };

      _v$ !== _p$._v$ && setAttribute(_el$, "aria-sort", _p$._v$ = _v$);
      _v$2 !== _p$._v$2 && setAttribute(_el$, "scope", _p$._v$2 = _v$2);
      _v$3 !== _p$._v$3 && setAttribute(_el$, "id", _p$._v$3 = _v$3);
      _p$._v$4 = classList(_el$2, _v$4, _p$._v$4);
      return _p$;
    }, {
      _v$: undefined,
      _v$2: undefined,
      _v$3: undefined,
      _v$4: undefined
    });

    return _el$;
  })();

  const renderTableNotSortable = () => (() => {
    const _el$4 = _tmpl$2$k.cloneNode(true),
          _el$5 = _el$4.firstChild;

    spread(_el$4, restProps, false, true);

    insert(_el$5, () => props.children);

    effect(_p$ => {
      const _v$5 = props.scope,
            _v$6 = id();

      _v$5 !== _p$._v$5 && setAttribute(_el$4, "scope", _p$._v$5 = _v$5);
      _v$6 !== _p$._v$6 && setAttribute(_el$4, "id", _p$._v$6 = _v$6);
      return _p$;
    }, {
      _v$5: undefined,
      _v$6: undefined
    });

    return _el$4;
  })();

  return () => props.tableSortable ? renderTableSortable() : renderTableNotSortable();
};

delegateEvents(["click"]);

const _tmpl$$C = template(`<thead></thead>`, 2);

const TableHead = props => (() => {
  const _el$ = _tmpl$$C.cloneNode(true);

  spread(_el$, props, false, false);

  return _el$;
})();

const _tmpl$$D = template(`<tbody aria-live="polite"></tbody>`, 2);

const TableBody = props => (() => {
  const _el$ = _tmpl$$D.cloneNode(true);

  spread(_el$, props, false, false);

  return _el$;
})();

const _tmpl$$E = template(`<tr></tr>`, 2);

const TableRow = props => (() => {
  const _el$ = _tmpl$$E.cloneNode(true);

  spread(_el$, props, false, false);

  return _el$;
})();

const _tmpl$$F = template(`<td></td>`, 2);

const TableCell = props => (() => {
  const _el$ = _tmpl$$F.cloneNode(true);

  spread(_el$, props, false, false);

  return _el$;
})();

const _tmpl$$G = template(
    `<svg data-carbon-icon="ChevronRight16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" preserveAspectRatio="xMidYMid meet"><path d="M11 8L6 13 5.3 12.3 9.6 8 5.3 3.7 6 3z"></path></svg>`,
    4
  ),
  _tmpl$2$l = template(`<title></title>`, 2);

const ChevronRight16 = (props) =>
  (() => {
    const _el$ = _tmpl$$G.cloneNode(true);
    _el$.firstChild;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$3 = _tmpl$2$l.cloneNode(true);

              insert(_el$3, () => props.title);

              return _el$3;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "16",
          _v$3 = props.height || "16",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

const _tmpl$$H = template(`<button type="button" class="bx--table-expand__button"></button>`, 2),
      _tmpl$2$m = template(`<th scope="col" class="bx--table-expand"></th>`, 2),
      _tmpl$3$2 = template(`<th scope="col" class="bx--table-column-checkbox"></th>`, 2),
      _tmpl$4$2 = template(`<th scope="col"></th>`, 2),
      _tmpl$5$2 = template(`<td></td>`, 2),
      _tmpl$6$2 = template(`<div class="bx--child-row-inner-container"></div>`, 2),
      _tmpl$7$2 = template(`<tr data-child-row="" class="bx--expandable-row"></tr>`, 2),
      _tmpl$8 = template(`<div>No data</div>`, 2);
const dataTableDefaultProps = {
  headers: [],
  rows: [],
  size: undefined,
  title: "",
  description: "",
  zebra: false,
  sortable: false,
  expandable: false,
  batchExpansion: false,
  expandedRowIds: [],
  radio: false,
  selectable: false,
  batchSelection: false,
  selectedRowIds: [],
  setSelectedRowIds: undefined,
  setExpandedRowIds: undefined,
  stickyHeader: false,
  children: undefined,
  renderCellHeader: header => header.value,
  renderCell: undefined,
  // Fn(row, cell) -> Element
  renderExpandedRow: undefined // Fn (row)

};
const DataTable = allProps => {
  const [props, restProps] = extractProps(allProps, dataTableDefaultProps); // XXX: Propagate up

  const dispatch = data => {
    console.log(data);
  };

  const headerKeys = createMemo(() => props.headers.map(({
    key
  }) => key));
  const [sortHeader, setSortHeader] = createState({
    key: null,
    sort: undefined,
    sortDirection: "none"
  });
  const rows = createMemo(() => props.rows.map(row => ({ ...row,
    cells: headerKeys().map(key => ({
      key,
      value: row[key]
    }))
  })));
  const sortedRows = createMemo(() => {
    if (props.sortable && sortHeader.key !== null && sortHeader.sortDirection !== "none") {
      const ascending = sortHeader.sortDirection === "ascending";
      const sortKey = sortHeader.key;
      return rows().slice(0).sort((a, b) => {
        const itemA = ascending ? a[sortKey] : b[sortKey];
        const itemB = ascending ? b[sortKey] : a[sortKey];
        if (sortHeader.sort) return sortHeader.sort(itemA, itemB);
        if (typeof itemA === "number" && typeof itemB === "number") return itemA - itemB;
        return itemA.toString().localeCompare(itemB.toString(), "en", {
          numeric: true
        });
      });
    } else {
      return rows();
    }
  });
  const allRowIds = createMemo(() => rows().map(row => row.id));
  const [expanded, setExpanded] = createSignal(false);
  const expandable = createMemo(() => props.expandable || props.batchExpansion);
  const [parentRowId, setParentRowId] = createSignal(null);

  const setExpandedRowIds = v => {
    const setter = props.setExpandedRowIds;

    if (setter) {
      setter(v);
    }
  };

  const expandedRowIds = createMemo(() => props.expandedRowIds);
  const expandedRows = createMemo(() => new Set(expandedRowIds()));
  const selectable = createMemo(() => props.selectable || props.radio || props.batchSelection);

  const setSelectedRowIds = newSelectedRowIds => {
    const setter = props.setSelectedRowIds;

    if (setter) {
      setter(newSelectedRowIds);
    }
  };

  const toggleAllExpansions = e => {
    setExpanded(!expanded());
    setExpandedRowIds(expanded() ? allRowIds() : []);
    dispatch({
      type: "click:header--expand",
      expanded: expanded()
    });
  };

  const toggleRowExpansion = row => {
    const rowExpanded = expandedRows().has(row.id);
    setExpandedRowIds(rowExpanded ? expandedRowIds().filter(id => id !== row.id) : expandedRowIds().concat([row.id]));
    dispatch({
      type: "click:row--expand",
      row,
      expanded: !rowExpanded
    });
  };

  const renderBatchExpansionButton = () => (() => {
    const _el$ = _tmpl$$H.cloneNode(true);

    _el$.$$click = toggleAllExpansions;
    _el$.$$clickData = null;

    insert(_el$, createComponent(ChevronRight16, {
      "class": "bx--table-expand__svg"
    }));

    return _el$;
  })();

  const renderExpandableTableHead = () => (() => {
    const _el$2 = _tmpl$2$m.cloneNode(true);

    insert(_el$2, (() => {
      const _c$ = memo(() => !!props.batchExpansion, true);

      return () => _c$() && renderBatchExpansionButton();
    })());

    effect(() => setAttribute(_el$2, "data-previous-value", expanded() ? "collapsed" : undefined));

    return _el$2;
  })();

  const selectedRowIds = createMemo(() => new Set(props.selectedRowIds));
  const allSelected = createMemo(() => selectedRowIds().size == props.rows.length);
  const noneSelected = createMemo(() => selectedRowIds().size == 0);
  const indeterminate = createMemo(() => !allSelected() && !noneSelected());

  const onChangeSelectAllRows = () => {
    if (indeterminate() || allSelected()) {
      setSelectedRowIds([]);
    } else {
      setSelectedRowIds(allRowIds());
    }
  }; // XXX


  var refSelectAll = undefined;

  const renderBatchSelectionHeaderCheckbox = () => (() => {
    const _el$3 = _tmpl$3$2.cloneNode(true);

    insert(_el$3, createComponent(InlineCheckbox, {
      ref(r$) {
        const _ref$ = refSelectAll;
        typeof _ref$ === "function" ? _ref$(r$) : refSelectAll = r$;
      },

      "aria-label": "Select all rows",

      get checked() {
        return allSelected() ? true : undefined;
      },

      get indeterminate() {
        return indeterminate() ? true : undefined;
      },

      onInput: onChangeSelectAllRows
    }));

    return _el$3;
  })();

  const nextSortDirection = {
    none: "ascending",
    ascending: "descending",
    descending: "none"
  };

  const onClickHeader = header => {
    const active = header.key === sortHeader.key;
    const currentSortDirection = active ? sortHeader.sortDirection : "none";
    const sortDirection = nextSortDirection[currentSortDirection];
    setSortHeader({
      //id: sortDirection === 'none' ? null : props.headers.find((h) => h.key === header.key).id, //thKeys()[header.key],
      key: header.key,
      sort: header.sort,
      sortDirection
    });
    dispatch({
      type: "click:header",
      header,
      sortDirection
    });
  };

  const renderHeader = header => {
    if (header.empty) {
      return _tmpl$4$2.cloneNode(true);
    } else {
      return createComponent(TableHeader, {
        onClick: () => onClickHeader(header),

        get tableSortable() {
          return props.sortable;
        },

        sortHeader: sortHeader,

        get key() {
          return header.key;
        },

        get children() {
          return props.renderCellHeader(header);
        }

      });
    }
  }; // XXX


  const tableHead = createComponent(TableHead, {
    get children() {
      return createComponent(TableRow, {
        get children() {
          return [createComponent(Show, {
            get when() {
              return expandable();
            },

            get children() {
              return renderExpandableTableHead();
            }

          }), createComponent(Show, {
            get when() {
              return selectable() && !props.batchSelection;
            },

            get children() {
              return _tmpl$4$2.cloneNode(true);
            }

          }), createComponent(Show, {
            get when() {
              return props.batchSelection && !props.radio;
            },

            get children() {
              return renderBatchSelectionHeaderCheckbox();
            }

          }), createComponent(For, {
            get each() {
              return props.headers;
            },

            children: renderHeader
          })];
        }

      });
    }

  });

  const tableRowClasses = row => cx(selectedRowIds().has(row.id) && "bx--data-table--selected", expandedRows().has(row.id) && "bx--expandable-row", expandable() && "bx--parent-row", expandable() && parentRowId() === row.id && "bx--expandable-row--hover");

  const renderExpandable = row => createComponent(TableCell, {
    "class": "bx--table-expand",
    headers: "expand",

    get ["data-previous-value"]() {
      return expandedRows().has(row.id) ? "collapsed" : undefined;
    },

    get children() {
      const _el$6 = _tmpl$$H.cloneNode(true);

      _el$6.$$click = toggleRowExpansion;
      _el$6.$$clickData = row;

      insert(_el$6, createComponent(ChevronRight16, {
        "class": "bx--table-expand__svg"
      }));

      effect(() => setAttribute(_el$6, "aria-label", expandedRows().has(row.id) ? "Collapse current row" : "Expand current row"));

      return _el$6;
    }

  });

  const onToggleSelect = rowId => {
    setSelectedRowIds(selectedRowIds().has(rowId) ? props.selectedRowIds.filter(id => id !== rowId) : props.selectedRowIds.concat([rowId]));
  };

  const renderSelectable = row => (() => {
    const _el$7 = _tmpl$5$2.cloneNode(true);

    insert(_el$7, createComponent(InlineCheckbox, {
      get name() {
        return `select-row-${row.id}`;
      },

      get checked() {
        return selectedRowIds().has(row.id);
      },

      get onInput() {
        return [onToggleSelect, row.id];
      }

    }));

    effect(_$p => classList(_el$7, {
      "bx--table-column-checkbox": true,
      "bx--table-column-radio": props.radio
    }, _$p));

    return _el$7;
  })();

  const renderCell = (row, cell, columnIndex) => {
    const header = props.headers[columnIndex()];
    const cellBody = props.renderCell ? props.renderCell(row, cell) : header.display ? header.display(cell.value) : cell.value;

    if (header.empty) {
      return (() => {
        const _el$8 = _tmpl$5$2.cloneNode(true);

        insert(_el$8, cellBody);

        effect(_$p => classList(_el$8, {
          "bx--table-column-menu": header.columnMenu
        }, _$p));

        return _el$8;
      })();
    } else {
      return createComponent(TableCell, {
        onClick: [dispatch, {
          type: "click:cell",
          row,
          cell
        }],
        children: cellBody
      });
    }
  };

  const renderRow = row => createComponent(TableRow, {
    get id() {
      return `row-${row.id}`;
    },

    get ["class"]() {
      return tableRowClasses(row);
    },

    onClick: [dispatch, {
      type: "click:row",
      row
    }],
    onMouseEnter: [dispatch, {
      type: "mouseenter:row",
      row
    }],
    onMouseLeave: [dispatch, {
      type: "mouseleave:row",
      row
    }],

    get children() {
      return [createComponent(Show, {
        get when() {
          return expandable();
        },

        get children() {
          return renderExpandable(row);
        }

      }), createComponent(Show, {
        get when() {
          return selectable();
        },

        get children() {
          return renderSelectable(row);
        }

      }), createComponent(For, {
        get each() {
          return row.cells;
        },

        children: (cell, columnIndex) => renderCell(row, cell, columnIndex)
      })];
    }

  });

  const renderExpandableRow = row => (() => {
    const _el$9 = _tmpl$7$2.cloneNode(true);

    _el$9.addEventListener("mouseleave", () => setParentRowId(null));

    _el$9.addEventListener("mouseenter", () => setParentRowId(row.id));

    insert(_el$9, createComponent(TableCell, {
      get colspan() {
        return props.headers.length + +(expandable() ? 1 : 0) + (selectable() ? 1 : 0);
      },

      get children() {
        const _el$10 = _tmpl$6$2.cloneNode(true);

        insert(_el$10, (() => {
          const _c$2 = memo(() => !!props.renderExpandedRow, true);

          return () => _c$2() && props.renderExpandedRow(row);
        })());

        return _el$10;
      }

    }));

    return _el$9;
  })();

  const tableBody = createComponent(TableBody, {
    get children() {
      return createComponent(For, {
        get each() {
          return sortedRows();
        },

        get fallback() {
          return _tmpl$8.cloneNode(true);
        },

        children: row => [memo(() => renderRow(row)), (() => {
          const _c$3 = memo(() => !!expandable(), true);

          return createComponent(Show, {
            get when() {
              return _c$3() && expandedRows().has(row.id);
            },

            get children() {
              return renderExpandableRow(row);
            }

          });
        })()]
      });
    }

  });

  const table = () => createComponent(Table, {
    get zebra() {
      return props.zebra;
    },

    get size() {
      return props.size;
    },

    get stickyHeader() {
      return props.stickyHeader;
    },

    get sortable() {
      return props.sortable;
    },

    get children() {
      return [tableHead, tableBody];
    }

  });

  const tableContainer = createComponent(TableContainer, mergeProps$1({
    get title() {
      return props.title;
    },

    get description() {
      return props.description;
    }

  }, restProps, {
    get children() {
      return [memo(() => props.children), memo(() => table())];
    }

  }));

  return tableContainer;
};

delegateEvents(["click"]);

const _tmpl$$I = template(`<button type="button" class="bx--table-expand__button"></button>`, 2),
      _tmpl$2$n = template(`<th scope="col" class="bx--table-expand"></th>`, 2),
      _tmpl$3$3 = template(`<th scope="col" class="bx--table-column-checkbox"></th>`, 2),
      _tmpl$4$3 = template(`<th scope="col"></th>`, 2),
      _tmpl$5$3 = template(`<td></td>`, 2),
      _tmpl$6$3 = template(`<div class="bx--child-row-inner-container"></div>`, 2),
      _tmpl$7$3 = template(`<tr data-child-row="" class="bx--expandable-row"></tr>`, 2),
      _tmpl$8$1 = template(`<div>No data</div>`, 2);
function OptDataTableModel({
  headers,
  rows,
  initiallyExpanded,
  initiallySelected
}) {
  return createState({
    headers,
    rows: rows.map((data, idx) => ({
      data,
      selected: initiallySelected && initiallySelected.includes(idx) ? 1 : 0,
      expanded: initiallyExpanded && initiallyExpanded.includes(idx),
      expandedHover: false
    })),
    ui: {
      expanded: false
    },
    sort: {
      key: null,
      sortFn: undefined,
      sortDirection: "none"
    }
  });
}
const dataTableDefaultProps$1 = {
  model: undefined,
  size: undefined,
  title: "",
  description: "",
  zebra: false,
  sortable: false,
  expandable: false,
  batchExpansion: false,
  radio: false,
  selectable: false,
  batchSelection: false,
  stickyHeader: false,
  children: undefined,
  renderCellHeader: header => header.value,
  renderCell: undefined,
  // Fn(row, cell) -> Element
  renderExpandedRow: undefined // Fn (row)

};
const nextSortDirection = {
  none: "ascending",
  ascending: "descending",
  descending: "none"
};
const OptDataTable = allProps => {
  const [props, restProps] = extractProps(allProps, dataTableDefaultProps$1);
  const [state, setState] = props.model; // XXX: Propagate up

  const dispatch = data => {
    console.log(data);
  };

  var selectCounter = 1;
  const sortedRowIndices = createMemo(() => {
    const sort = state.sort;
    const indices = state.rows.map((_, i) => i);

    if (props.sortable && sort.key !== null && sort.sortDirection !== "none") {
      const ascending = sort.sortDirection === "ascending";
      const sortKey = sort.key;
      const rows = state.rows;
      indices.sort((j, k) => {
        const itemA = rows[ascending ? j : k].data[sortKey];
        const itemB = rows[ascending ? k : j].data[sortKey];
        if (sort.sortFn) return sort.sortFn(itemA, itemB);
        if (typeof itemA === "number" && typeof itemB === "number") return itemA - itemB;
        return itemA.toString().localeCompare(itemB.toString(), "en", {
          numeric: true
        });
      });
    }

    return indices;
  });
  const expandable = createMemo(() => props.expandable || props.batchExpansion);
  const selectable = createMemo(() => props.selectable || props.radio || props.batchSelection); //
  // Actions
  //

  const toggleAllExpansions = e => {
    setState("ui", "expanded", old => !old);
    setState("rows", {}, "expanded", state.ui.expanded);
    dispatch({
      type: "click:header--expand",
      expanded: state.ui.expanded
    });
  };

  const toggleRowExpansion = rowIndex => {
    setState("rows", rowIndex, "expanded", old => !old);
    const row = state.rows[rowIndex];
    dispatch({
      type: "click:row--expand",
      row,
      expanded: row.expanded
    });
  };

  const onClickHeader = header => {
    const active = header.key === state.sort.key;
    const currentSortDirection = active ? state.sort.sortDirection : "none";
    const sortDirection = nextSortDirection[currentSortDirection];
    setState("sort", {
      key: header.key,
      sortFn: header.sort,
      sortDirection
    });
    dispatch({
      type: "click:header",
      header,
      sortDirection
    });
  };

  const countSelected = createMemo(() => state.rows.reduce((acc, row) => row.selected ? acc + 1 : acc, 0));
  const allSelected = createMemo(() => countSelected() == state.rows.length);
  const noneSelected = createMemo(() => countSelected() == 0);
  const indeterminate = createMemo(() => !allSelected() && !noneSelected());

  const toggleBatchSelection = () => {
    const newSelected = indeterminate() || allSelected() ? 0 : ++selectCounter;
    setState("rows", {}, "selected", newSelected);
  };

  const toggleSelection = rowIndex => {
    setState("rows", rowIndex, "selected", old => old ? 0 : ++selectCounter);
  };

  const updateExpandHoverEnter = rowIndex => setState("rows", rowIndex, "expandedHover", true);

  const updateExpandHoverLeave = rowIndex => setState("rows", rowIndex, "expandedHover", false); //
  // Render functions
  //


  const renderExpandableTableHead = () => (() => {
    const _el$ = _tmpl$2$n.cloneNode(true);

    insert(_el$, createComponent(Show, {
      get when() {
        return props.batchExpansion;
      },

      get children() {
        const _el$2 = _tmpl$$I.cloneNode(true);

        _el$2.$$click = toggleAllExpansions;
        _el$2.$$clickData = null;

        insert(_el$2, createComponent(ChevronRight16, {
          "class": "bx--table-expand__svg"
        }));

        return _el$2;
      }

    }));

    effect(() => setAttribute(_el$, "data-previous-value", state.ui.expanded ? "collapsed" : undefined));

    return _el$;
  })();

  const renderBatchSelectionHeaderCheckbox = () => (() => {
    const _el$3 = _tmpl$3$3.cloneNode(true);

    insert(_el$3, createComponent(InlineCheckbox, {
      "aria-label": "Select all rows",

      get checked() {
        return allSelected() ? true : undefined;
      },

      get indeterminate() {
        return indeterminate() ? true : undefined;
      },

      onInput: toggleBatchSelection
    }));

    return _el$3;
  })();

  const renderHeader = header => {
    if (header.empty) {
      return _tmpl$4$3.cloneNode(true);
    } else {
      return createComponent(TableHeader, {
        onClick: () => onClickHeader(header),

        get tableSortable() {
          return props.sortable;
        },

        get sortHeader() {
          return state.sort;
        },

        get key() {
          return header.key;
        },

        get children() {
          return props.renderCellHeader(header);
        }

      });
    }
  }; // XXX


  const tableHead = createComponent(TableHead, {
    get children() {
      return createComponent(TableRow, {
        get children() {
          return [createComponent(Show, {
            get when() {
              return expandable();
            },

            get children() {
              return renderExpandableTableHead();
            }

          }), createComponent(Show, {
            get when() {
              return selectable() && !props.batchSelection;
            },

            get children() {
              return _tmpl$4$3.cloneNode(true);
            }

          }), createComponent(Show, {
            get when() {
              return props.batchSelection && !props.radio;
            },

            get children() {
              return renderBatchSelectionHeaderCheckbox();
            }

          }), createComponent(For, {
            get each() {
              return state.headers;
            },

            children: renderHeader
          })];
        }

      });
    }

  });

  const tableRowClasses = row => cx(!!row.selected && "bx--data-table--selected", row.expanded === true && "bx--expandable-row", expandable() && "bx--parent-row", expandable() && row.expandedHover === true && "bx--expandable-row--hover");

  const renderExpandable = (row, rowIndex) => createComponent(TableCell, {
    "class": "bx--table-expand",
    headers: "expand",

    get ["data-previous-value"]() {
      return row.expanded ? "collapsed" : undefined;
    },

    get children() {
      const _el$6 = _tmpl$$I.cloneNode(true);

      _el$6.$$click = toggleRowExpansion;
      _el$6.$$clickData = rowIndex;

      insert(_el$6, createComponent(ChevronRight16, {
        "class": "bx--table-expand__svg"
      }));

      effect(() => setAttribute(_el$6, "aria-label", row.expanded ? "Collapse current row" : "Expand current row"));

      return _el$6;
    }

  });

  const renderSelectable = (row, rowIndex) => (() => {
    const _el$7 = _tmpl$5$3.cloneNode(true);

    insert(_el$7, createComponent(InlineCheckbox, {
      get name() {
        return `select-row-${row.data.id}`;
      },

      get checked() {
        return !!row.selected;
      },

      onInput: [toggleSelection, rowIndex]
    }));

    effect(_$p => classList(_el$7, {
      "bx--table-column-checkbox": true,
      "bx--table-column-radio": props.radio
    }, _$p));

    return _el$7;
  })(); // XXX


  const renderCell = (row, header) => {
    const cell = {
      key: header.key,
      value: row.data[header.key]
    };
    const cellBody = props.renderCell ? props.renderCell(row.data, cell) : header.display ? header.display(cell.value) : cell.value;

    if (header.empty) {
      return (() => {
        const _el$8 = _tmpl$5$3.cloneNode(true);

        insert(_el$8, cellBody);

        effect(_$p => classList(_el$8, {
          "bx--table-column-menu": header.columnMenu
        }, _$p));

        return _el$8;
      })();
    } else {
      return createComponent(TableCell, {
        onClick: [dispatch, {
          type: "click:cell",
          row,
          cell
        }],
        children: cellBody
      });
    }
  };

  const renderExpandableRow = (row, rowIndex) => (() => {
    const _el$9 = _tmpl$7$3.cloneNode(true);

    _el$9.addEventListener("mouseleave", e => updateExpandHoverLeave(rowIndex));

    _el$9.addEventListener("mouseenter", e => updateExpandHoverEnter(rowIndex));

    insert(_el$9, createComponent(TableCell, {
      get colspan() {
        return state.headers.length + (expandable() ? 1 : 0) + (selectable() ? 1 : 0);
      },

      get children() {
        const _el$10 = _tmpl$6$3.cloneNode(true);

        insert(_el$10, createComponent(Show, {
          get when() {
            return props.renderExpandedRow;
          },

          children: render => render(row.data)
        }));

        return _el$10;
      }

    }));

    return _el$9;
  })();

  const renderRow = (row, rowIndex) => [createComponent(TableRow, {
    get id() {
      return `row-${row.data.id}`;
    },

    get ["class"]() {
      return tableRowClasses(row);
    },

    onClick: [dispatch, {
      type: "click:row",
      row
    }],
    onMouseEnter: [dispatch, {
      type: "mouseenter:row",
      row
    }],
    onMouseLeave: [dispatch, {
      type: "mouseleave:row",
      row
    }],

    get children() {
      return [createComponent(Show, {
        get when() {
          return expandable();
        },

        get children() {
          return renderExpandable(row, rowIndex);
        }

      }), createComponent(Show, {
        get when() {
          return selectable();
        },

        get children() {
          return renderSelectable(row, rowIndex);
        }

      }), createComponent(For, {
        get each() {
          return state.headers;
        },

        children: header => renderCell(row, header)
      })];
    }

  }), createComponent(Show, {
    get when() {
      return expandable() && row.expanded;
    },

    get children() {
      return renderExpandableRow(row, rowIndex);
    }

  })];

  const tableBody = createComponent(TableBody, {
    get children() {
      return createComponent(For, {
        get each() {
          return sortedRowIndices();
        },

        get fallback() {
          return _tmpl$8$1.cloneNode(true);
        },

        children: rowIndex => renderRow(state.rows[rowIndex], rowIndex)
      });
    }

  });

  const table = createComponent(Table, {
    get zebra() {
      return props.zebra;
    },

    get size() {
      return props.size;
    },

    get stickyHeader() {
      return props.stickyHeader;
    },

    get sortable() {
      return props.sortable;
    },

    get children() {
      return [tableHead, tableBody];
    }

  });

  const tableContainer = createComponent(TableContainer, mergeProps$1({
    get title() {
      return props.title;
    },

    get description() {
      return props.description;
    }

  }, restProps, {
    get children() {
      return [memo(() => props.children), table];
    }

  }));

  return tableContainer;
};

delegateEvents(["click"]);

const _tmpl$$J = template(`<section aria-label="data table toolbar"></section>`, 2);
const toolbarDefaultProps = {
  size: "default",
  class: undefined
};
const Toolbar = allProps => {
  const [props, restProps] = extractProps(allProps, toolbarDefaultProps); // XXX: Setup context

  return () => (() => {
    const _el$ = _tmpl$$J.cloneNode(true);

    spread(_el$, restProps, false, false);

    effect(() => _el$.className = cx("bx--table-toolbar", props.size === "sm" && "bx--table-toolbar--small", props.size === "default" && "bx--table-toolbar--normal", props.class));

    return _el$;
  })();
};

const _tmpl$$K = template(`<div class="bx--toolbar-content"></div>`, 2);

const ToolbarContent = props => (() => {
  const _el$ = _tmpl$$K.cloneNode(true);

  insert(_el$, () => props.children);

  return _el$;
})();

const _tmpl$$L = template(`<div aria-atomic="true"><label class="bx--visually-hidden"></label><svg class="bx--loading__svg" viewBox="0 0 150 150"><title></title><circle class="bx--loading__stroke" cx="50%" cy="50%"></circle></svg></div>`, 10),
      _tmpl$2$o = template(`<svg><circle class="bx--loading__background" cx="50%" cy="50%"></circle></svg>`, 4, true);
const SpinnerRadius = {
  small: "26.8125",
  large: "37.5"
};
const LoadingSpinner = props => {
  const id = createMemo(() => props.id || uniqueId());
  const spinnerRadius = createMemo(() => props.small ? SpinnerRadius.small : SpinnerRadius.large);
  return (() => {
    const _el$ = _tmpl$$L.cloneNode(true),
          _el$2 = _el$.firstChild,
          _el$3 = _el$2.nextSibling,
          _el$4 = _el$3.firstChild,
          _el$5 = _el$4.nextSibling;

    insert(_el$2, () => props.description);

    insert(_el$4, () => props.description);

    insert(_el$3, (() => {
      const _c$ = memo(() => !!props.small, true);

      return () => _c$() ? (() => {
        const _el$6 = _tmpl$2$o.cloneNode(true);

        effect(() => setAttribute(_el$6, "r", spinnerRadius()));

        return _el$6;
      })() : null;
    })(), _el$5);

    effect(_p$ => {
      const _v$ = id(),
            _v$2 = props.active ? "assertive" : "off",
            _v$3 = cx("bx--loading", props.small && "bx--loading--small", !props.active && "bx--loading--stop", props.class),
            _v$4 = id(),
            _v$5 = spinnerRadius();

      _v$ !== _p$._v$ && setAttribute(_el$, "aria-labelledby", _p$._v$ = _v$);
      _v$2 !== _p$._v$2 && setAttribute(_el$, "aria-live", _p$._v$2 = _v$2);
      _v$3 !== _p$._v$3 && (_el$.className = _p$._v$3 = _v$3);
      _v$4 !== _p$._v$4 && setAttribute(_el$2, "id", _p$._v$4 = _v$4);
      _v$5 !== _p$._v$5 && setAttribute(_el$5, "r", _p$._v$5 = _v$5);
      return _p$;
    }, {
      _v$: undefined,
      _v$2: undefined,
      _v$3: undefined,
      _v$4: undefined,
      _v$5: undefined
    });

    return _el$;
  })();
};

const _tmpl$$M = template(`<div></div>`, 2);
const LoadingPropsDefaults = {
  small: false,
  active: true,
  withOverlay: true,
  description: "Active loading indicator",
  id: undefined
};
const Loading = allProps => {
  const props = mergeProps(LoadingPropsDefaults, allProps);

  const loading = () => createComponent(LoadingSpinner, mergeProps$1(props, {}));

  return props.withOverlay ? (() => {
    const _el$ = _tmpl$$M.cloneNode(true);

    insert(_el$, loading);

    effect(_$p => classList(_el$, {
      "bx--loading-overlay": true,
      "bx--loading-overlay--stop": !props.active
    }, _$p));

    return _el$;
  })() : loading();
};

const _tmpl$$N = template(
    `<svg data-carbon-icon="ErrorFilled16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" preserveAspectRatio="xMidYMid meet"><path d="M8,1C4.1,1,1,4.1,1,8s3.1,7,7,7s7-3.1,7-7S11.9,1,8,1z M10.7,11.5L4.5,5.3l0.8-0.8l6.2,6.2L10.7,11.5z"></path><path fill="none" d="M10.7,11.5L4.5,5.3l0.8-0.8l6.2,6.2L10.7,11.5z" data-icon-path="inner-path" opacity="0"></path></svg>`,
    6
  ),
  _tmpl$2$p = template(`<title></title>`, 2);

const ErrorFilled16 = (props) =>
  (() => {
    const _el$ = _tmpl$$N.cloneNode(true),
      _el$2 = _el$.firstChild;
    _el$2.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$4 = _tmpl$2$p.cloneNode(true);

              insert(_el$4, () => props.title);

              return _el$4;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "16",
          _v$3 = props.height || "16",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

const _tmpl$$O = template(
    `<svg data-carbon-icon="CheckmarkFilled16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" preserveAspectRatio="xMidYMid meet"><path d="M8,1C4.1,1,1,4.1,1,8c0,3.9,3.1,7,7,7s7-3.1,7-7C15,4.1,11.9,1,8,1z M7,11L4.3,8.3l0.9-0.8L7,9.3l4-3.9l0.9,0.8L7,11z"></path><path d="M7,11L4.3,8.3l0.9-0.8L7,9.3l4-3.9l0.9,0.8L7,11z" data-icon-path="inner-path" opacity="0"></path></svg>`,
    6
  ),
  _tmpl$2$q = template(`<title></title>`, 2);

const CheckmarkFilled16 = (props) =>
  (() => {
    const _el$ = _tmpl$$O.cloneNode(true),
      _el$2 = _el$.firstChild;
    _el$2.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$4 = _tmpl$2$q.cloneNode(true);

              insert(_el$4, () => props.title);

              return _el$4;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "16",
          _v$3 = props.height || "16",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

const _tmpl$$P = template(`<div class="bx--inline-loading" aria-live="assertive"></div>`, 2),
      _tmpl$2$r = template(`<div class="bx--inline-loading__text"></div>`, 2),
      _tmpl$3$4 = template(`<div class="bx--inline-loading__animation"></div>`, 2);
const DefaultSuccessDelay = 1500;
const InlineLoading = allProps => {
  const props = mergeProps({
    status: "active",
    successDelay: DefaultSuccessDelay,
    onSuccess: () => {}
  }, allProps);
  let timerId = null;

  const clearTimer = () => {
    if (timerId) {
      clearTimeout(timerId);
    }

    timerId = null;
  };

  const setTimer = () => {
    timerId = setTimeout(props.onSuccess, props.successDelay);
  };

  const reloadTimer = () => {
    clearTimer();
    setTimer();
  };

  createEffect(() => {
    if (props.status === "finished") {
      reloadTimer();
    }
  });
  onCleanup(clearTimer);
  return (() => {
    const _el$ = _tmpl$$P.cloneNode(true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(_el$, () => loadingAnimation(props), null);

    insert(_el$, (() => {
      const _c$ = memo(() => !!props.description, true);

      return () => _c$() && (() => {
        const _el$2 = _tmpl$2$r.cloneNode(true);

        insert(_el$2, () => props.description);

        return _el$2;
      })();
    })(), null);

    effect(_$p => style(_el$, props.style, _$p));

    return _el$;
  })();
};

const loadingAnimation = props => (() => {
  const _el$3 = _tmpl$3$4.cloneNode(true);

  insert(_el$3, createComponent(Switch$1, {
    get children() {
      return [createComponent(Match, {
        get when() {
          return props.status === "error";
        },

        get children() {
          return createComponent(ErrorFilled16, {
            "class": "bx--inline-loading--error"
          });
        }

      }), createComponent(Match, {
        get when() {
          return props.status === "finished";
        },

        get children() {
          return createComponent(CheckmarkFilled16, {
            "class": "bx--inline-loading__checkmark-container"
          });
        }

      }), createComponent(Match, {
        get when() {
          return props.status === "inactive" || props.status === "active";
        },

        get children() {
          return createComponent(Loading, {
            small: true,

            get description() {
              return props.iconDescription;
            },

            withOverlay: false,

            get active() {
              return props.status === "active";
            }

          });
        }

      })];
    }

  }));

  return _el$3;
})();

delegateEvents(["click", "mouseover"]);

const _tmpl$$Q = template(`<div></div>`, 2);
const Tile = props => (() => {
  const _el$ = _tmpl$$Q.cloneNode(true);

  addEventListener(_el$, "mouseleave", props.onMouseLeave);

  addEventListener(_el$, "mouseenter", props.onMouseEnter);

  addEventListener(_el$, "mouseover", props.onMouseOver, true);

  addEventListener(_el$, "click", props.onClick, true);

  insert(_el$, () => props.children);

  effect(() => _el$.className = cx("bx--tile", props.light && "bx--tile--light", props.class));

  return _el$;
})();

delegateEvents(["click", "mouseover"]);

const ClickableTile = props => createComponent(UnstyledLink, {
  get href() {
    return props.href;
  },

  get target() {
    return props.target;
  },

  get ["class"]() {
    return cx("bx--tile", "bx--tile--clickable", props.light && "bx--tile--light");
  },

  onClick: ev => {
    props.onClick && props.onClick(ev);
    props.onClicked && props.onClicked();
  },
  onKeyDown: ev => {
    props.onKeyDown && props.onKeyDown(ev);

    if (ev.key === " " || ev.key === "Enter") {
      props.onClicked && props.onClicked();
    }
  },

  get onMouseOver() {
    return props.onMouseOver;
  },

  get onMouseEnter() {
    return props.onMouseEnter;
  },

  get onMouseLeave() {
    return props.onMouseLeave;
  },

  get children() {
    return props.children;
  }

});

const NotificationActionButton = props => createComponent(Button, mergeProps$1({
  kind: "ghost",
  size: "small",
  "class": "bx--inline-notification__action-button"
}, props, {}));

const _tmpl$$R = template(`<button type="button"></button>`, 2);
const NotificationButton = props => {
  const notificationType = props.notificationType || "toast";
  const renderIcon = props.renderIcon || Close20;
  const iconDescription = props.iconDescription || "Close icon";
  return (() => {
    const _el$ = _tmpl$$R.cloneNode(true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    setAttribute(_el$, "aria-label", iconDescription);

    setAttribute(_el$, "title", iconDescription);

    _el$.className = `bx--${notificationType}-notification__close-button`;

    insert(_el$, createComponent(Dynamic, {
      component: renderIcon,

      get title() {
        return props.title;
      },

      "class": `bx--${notificationType}-notification__close-icon`
    }));

    return _el$;
  })();
};

delegateEvents(["click", "mouseover"]);

const _tmpl$$S = template(
    `<svg data-carbon-icon="CheckmarkFilled20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" preserveAspectRatio="xMidYMid meet"><path d="M10,1c-4.9,0-9,4.1-9,9s4.1,9,9,9s9-4,9-9S15,1,10,1z M8.7,13.5l-3.2-3.2l1-1l2.2,2.2l4.8-4.8l1,1L8.7,13.5z"></path><path fill="none" d="M8.7,13.5l-3.2-3.2l1-1l2.2,2.2l4.8-4.8l1,1L8.7,13.5z" data-icon-path="inner-path" opacity="0"></path></svg>`,
    6
  ),
  _tmpl$2$s = template(`<title></title>`, 2);

const CheckmarkFilled20 = (props) =>
  (() => {
    const _el$ = _tmpl$$S.cloneNode(true),
      _el$2 = _el$.firstChild;
    _el$2.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$4 = _tmpl$2$s.cloneNode(true);

              insert(_el$4, () => props.title);

              return _el$4;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "20",
          _v$3 = props.height || "20",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

const _tmpl$$T = template(
    `<svg data-carbon-icon="ErrorFilled20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" preserveAspectRatio="xMidYMid meet"><path d="M10,1c-5,0-9,4-9,9s4,9,9,9s9-4,9-9S15,1,10,1z M13.5,14.5l-8-8l1-1l8,8L13.5,14.5z"></path><path d="M13.5,14.5l-8-8l1-1l8,8L13.5,14.5z" data-icon-path="inner-path" opacity="0"></path></svg>`,
    6
  ),
  _tmpl$2$t = template(`<title></title>`, 2);

const ErrorFilled20 = (props) =>
  (() => {
    const _el$ = _tmpl$$T.cloneNode(true),
      _el$2 = _el$.firstChild;
    _el$2.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$4 = _tmpl$2$t.cloneNode(true);

              insert(_el$4, () => props.title);

              return _el$4;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "20",
          _v$3 = props.height || "20",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

const _tmpl$$U = template(
    `<svg data-carbon-icon="InformationFilled20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path fill="none" d="M16,8a1.5,1.5,0,1,1-1.5,1.5A1.5,1.5,0,0,1,16,8Zm4,13.875H17.125v-8H13v2.25h1.875v5.75H12v2.25h8Z"></path><path d="M16,2A14,14,0,1,0,30,16,14,14,0,0,0,16,2Zm0,6a1.5,1.5,0,1,1-1.5,1.5A1.5,1.5,0,0,1,16,8Zm4,16.125H12v-2.25h2.875v-5.75H13v-2.25h4.125v8H20Z"></path></svg>`,
    6
  ),
  _tmpl$2$u = template(`<title></title>`, 2);

const InformationFilled20 = (props) =>
  (() => {
    const _el$ = _tmpl$$U.cloneNode(true),
      _el$2 = _el$.firstChild;
    _el$2.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$4 = _tmpl$2$u.cloneNode(true);

              insert(_el$4, () => props.title);

              return _el$4;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "20",
          _v$3 = props.height || "20",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

const _tmpl$$V = template(
    `<svg data-carbon-icon="InformationSquareFilled20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path fill="none" d="M16,8a1.5,1.5,0,1,1-1.5,1.5A1.5,1.5,0,0,1,16,8Zm4,13.875H17.125v-8H13v2.25h1.875v5.75H12v2.25h8Z"></path><path d="M26,4H6A2,2,0,0,0,4,6V26a2,2,0,0,0,2,2H26a2,2,0,0,0,2-2V6A2,2,0,0,0,26,4ZM16,8a1.5,1.5,0,1,1-1.5,1.5A1.5,1.5,0,0,1,16,8Zm4,16.125H12v-2.25h2.875v-5.75H13v-2.25h4.125v8H20Z"></path></svg>`,
    6
  ),
  _tmpl$2$v = template(`<title></title>`, 2);

const InformationSquareFilled20 = (props) =>
  (() => {
    const _el$ = _tmpl$$V.cloneNode(true),
      _el$2 = _el$.firstChild;
    _el$2.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$4 = _tmpl$2$v.cloneNode(true);

              insert(_el$4, () => props.title);

              return _el$4;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "20",
          _v$3 = props.height || "20",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

const _tmpl$$W = template(
    `<svg data-carbon-icon="WarningFilled20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" preserveAspectRatio="xMidYMid meet"><path d="M10,1c-5,0-9,4-9,9s4,9,9,9s9-4,9-9S15,1,10,1z M9.2,5h1.5v7H9.2V5z M10,16c-0.6,0-1-0.4-1-1s0.4-1,1-1	s1,0.4,1,1S10.6,16,10,16z"></path><path d="M9.2,5h1.5v7H9.2V5z M10,16c-0.6,0-1-0.4-1-1s0.4-1,1-1s1,0.4,1,1S10.6,16,10,16z" data-icon-path="inner-path" opacity="0"></path></svg>`,
    6
  ),
  _tmpl$2$w = template(`<title></title>`, 2);

const WarningFilled20 = (props) =>
  (() => {
    const _el$ = _tmpl$$W.cloneNode(true),
      _el$2 = _el$.firstChild;
    _el$2.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$4 = _tmpl$2$w.cloneNode(true);

              insert(_el$4, () => props.title);

              return _el$4;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "20",
          _v$3 = props.height || "20",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

const _tmpl$$X = template(
    `<svg data-carbon-icon="WarningAltFilled20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" preserveAspectRatio="xMidYMid meet"><path fill="none" d="M14.875,11h2.25V21h-2.25ZM16,27a1.5,1.5,0,1,1,1.5-1.5A1.5,1.5,0,0,1,16,27Z"></path><path d="M29.8872,28.5386l-13-25a1,1,0,0,0-1.7744,0l-13,25A1,1,0,0,0,3,30H29a1,1,0,0,0,.8872-1.4614ZM14.875,11h2.25V21h-2.25ZM16,27a1.5,1.5,0,1,1,1.5-1.5A1.5,1.5,0,0,1,16,27Z"></path></svg>`,
    6
  ),
  _tmpl$2$x = template(`<title></title>`, 2);

const WarningAltFilled20 = (props) =>
  (() => {
    const _el$ = _tmpl$$X.cloneNode(true),
      _el$2 = _el$.firstChild;
    _el$2.nextSibling;

    addEventListener(_el$, "keydown", props.onKeyDown, true);

    addEventListener(_el$, "keyup", props.onKeyUp, true);

    addEventListener(_el$, "mouseleave", props.onMouseLeave);

    addEventListener(_el$, "mouseenter", props.onMouseEnter);

    addEventListener(_el$, "mouseover", props.onMouseOver, true);

    addEventListener(_el$, "click", props.onClick, true);

    insert(
      _el$,
      (() => {
        const _c$ = memo(() => props.children, true);

        return () =>
          _c$() ||
          (props.title &&
            (() => {
              const _el$4 = _tmpl$2$x.cloneNode(true);

              insert(_el$4, () => props.title);

              return _el$4;
            })());
      })(),
      null
    );

    effect(
      (_p$) => {
        const _v$ = props.fill || "currentColor",
          _v$2 = props.width || "20",
          _v$3 = props.height || "20",
          _v$4 = props["aria-label"],
          _v$5 = props["aria-labelledby"],
          _v$6 = props["aria-label"] || props["aria-labelledby"] || props.title,
          _v$7 =
            props["aria-label"] || props["aria-labelledby"] || props.title
              ? "img"
              : undefined,
          _v$8 = props.tabindex === "0" ? true : props.focusable,
          _v$9 = props.tabindex,
          _v$10 = props.id,
          _v$11 = props.class,
          _v$12 = props.title,
          _v$13 = props.style,
          _v$14 = props.stroke;

        _v$ !== _p$._v$ && setAttribute(_el$, "fill", (_p$._v$ = _v$));
        _v$2 !== _p$._v$2 && setAttribute(_el$, "width", (_p$._v$2 = _v$2));
        _v$3 !== _p$._v$3 && setAttribute(_el$, "height", (_p$._v$3 = _v$3));
        _v$4 !== _p$._v$4 &&
          setAttribute(_el$, "aria-label", (_p$._v$4 = _v$4));
        _v$5 !== _p$._v$5 &&
          setAttribute(_el$, "aria-labelledby", (_p$._v$5 = _v$5));
        _v$6 !== _p$._v$6 &&
          setAttribute(_el$, "aria-hidden", (_p$._v$6 = _v$6));
        _v$7 !== _p$._v$7 && setAttribute(_el$, "role", (_p$._v$7 = _v$7));
        _v$8 !== _p$._v$8 && setAttribute(_el$, "focusable", (_p$._v$8 = _v$8));
        _v$9 !== _p$._v$9 && setAttribute(_el$, "tabindex", (_p$._v$9 = _v$9));
        _v$10 !== _p$._v$10 && setAttribute(_el$, "id", (_p$._v$10 = _v$10));
        _v$11 !== _p$._v$11 && setAttribute(_el$, "class", (_p$._v$11 = _v$11));
        _v$12 !== _p$._v$12 && setAttribute(_el$, "title", (_p$._v$12 = _v$12));
        _p$._v$13 = style(_el$, _v$13, _p$._v$13);
        _v$14 !== _p$._v$14 &&
          setAttribute(_el$, "stroke", (_p$._v$14 = _v$14));
        return _p$;
      },
      {
        _v$: undefined,
        _v$2: undefined,
        _v$3: undefined,
        _v$4: undefined,
        _v$5: undefined,
        _v$6: undefined,
        _v$7: undefined,
        _v$8: undefined,
        _v$9: undefined,
        _v$10: undefined,
        _v$11: undefined,
        _v$12: undefined,
        _v$13: undefined,
        _v$14: undefined,
      }
    );

    return _el$;
  })();

delegateEvents(["click", "mouseover", "keyup", "keydown"]);

const icons = {
  error: ErrorFilled20,
  info: InformationFilled20,
  "info-square": InformationSquareFilled20,
  success: CheckmarkFilled20,
  warning: WarningFilled20,
  "warning-alt": WarningAltFilled20
};
const NotificationIcon = props => {
  const kind = props.kind || "error";
  const notificationType = props.notificationType || "toast";
  const iconDescription = props.iconDescription || "Closes notification";
  return createComponent(Dynamic, {
    get component() {
      return icons[kind];
    },

    title: iconDescription,
    "class": `bx--${notificationType}-notification__icon`
  });
};

const _tmpl$$Y = template(`<div class="bx--toast-notification__details"><h3 class="bx--toast-notification__title"></h3><div class="bx--toast-notification__subtitle"></div><div class="bx--toast-notification__caption"></div></div>`, 8),
      _tmpl$2$y = template(`<div class="bx--inline-notification__text-wrapper"><p class="bx--inline-notification__title"></p><div class="bx--inline-notification__subtitle"></div></div>`, 6);

const NotificationTextDetails = props => {
  const notificationType = props.notificationType || "toast";
  const title = props.title || "Title";
  const subtitle = props.subtitle || "";
  const caption = props.caption || "Caption";

  if (notificationType === "toast") {
    return (() => {
      const _el$ = _tmpl$$Y.cloneNode(true),
            _el$2 = _el$.firstChild,
            _el$3 = _el$2.nextSibling,
            _el$4 = _el$3.nextSibling;

      insert(_el$2, title);

      insert(_el$3, subtitle);

      insert(_el$4, caption);

      insert(_el$, () => props.children, null);

      return _el$;
    })();
  }

  if (notificationType === "inline") {
    return (() => {
      const _el$5 = _tmpl$2$y.cloneNode(true),
            _el$6 = _el$5.firstChild,
            _el$7 = _el$6.nextSibling;

      insert(_el$6, title);

      insert(_el$7, subtitle);

      insert(_el$5, () => props.children, null);

      return _el$5;
    })();
  }
};

const _tmpl$$Z = template(`<div></div>`, 2),
      _tmpl$2$z = template(`<div class="bx--inline-notification__details"></div>`, 2);
const Notification = props => {
  const kind = props.kind || "error";
  const notificationType = props.notificationType || "toast";
  const timeout = props.timeout;
  const role = props.role || "alert";
  const title = props.title || "Title";
  const subtitle = props.subtitle || "";
  const caption = props.caption || "Caption";
  const iconDescription = props.iconDescription || "Closes notification";
  const [open, setOpen] = createSignal(true);
  let timeoutId = undefined;

  const close = () => {
    setOpen(false);
    props.onClose && props.onClose();
  };

  onMount(() => {
    if (timeout) {
      timeoutId = setTimeout(close, timeout);
    }
  });
  onCleanup(() => {
    if (timeoutId) {
      clearTimeout(timeoutId);
      timeoutId = undefined;
    }
  });

  const renderInner = () => [createComponent(NotificationIcon, {
    notificationType: notificationType,
    kind: kind,
    iconDescription: iconDescription
  }), createComponent(NotificationTextDetails, {
    title: title,
    subtitle: subtitle,
    caption: caption,
    notificationType: notificationType,

    get children() {
      return props.children;
    }

  })];

  const className = cx(`bx--${notificationType}-notification`, props.lowContrast && `bx--${notificationType}-notification--low-contrast`, props.hideCloseButton && `bx--${notificationType}-notification--hide-close-button`, `bx--${notificationType}-notification--${kind}`);
  return createComponent(Show, {
    get when() {
      return open();
    },

    get children() {
      const _el$ = _tmpl$$Z.cloneNode(true);

      addEventListener(_el$, "mouseleave", props.onMouseLeave);

      addEventListener(_el$, "mouseenter", props.onMouseEnter);

      addEventListener(_el$, "mouseover", props.onMouseOver, true);

      addEventListener(_el$, "click", props.onClick, true);

      setAttribute(_el$, "role", role);

      setAttribute(_el$, "kind", kind);

      _el$.className = className;

      insert(_el$, notificationType === "inline" && (() => {
        const _el$2 = _tmpl$2$z.cloneNode(true);

        insert(_el$2, renderInner);

        return _el$2;
      })(), null);

      insert(_el$, () => notificationType === "toast" && renderInner(), null);

      insert(_el$, () => props.renderActions, null);

      insert(_el$, createComponent(Show, {
        get when() {
          return !props.hideCloseButton;
        },

        get children() {
          return createComponent(NotificationButton, {
            iconDescription: iconDescription,
            notificationType: notificationType,
            onClick: close
          });
        }

      }), null);

      effect(_$p => style(_el$, props.style, _$p));

      return _el$;
    }

  });
};
const InlineNotification = props => createComponent(Notification, mergeProps$1({
  notificationType: "inline"
}, props, {}));
const ToastNotification = props => createComponent(Notification, mergeProps$1({
  notificationType: "toast"
}, props, {}));

delegateEvents(["click", "mouseover"]);

const _tmpl$$_ = template(`<div></div>`, 2);
const IconSkeleton = props => {
  const [local, restProps] = splitProps(props, ["size", "style", "class"]);
  return (() => {
    const _el$ = _tmpl$$_.cloneNode(true);

    spread(_el$, restProps, false, false);

    effect(_p$ => {
      const _v$ = cx("bx--icon--skeleton", local.class),
            _v$2 = `${local.style}; width: ${local.size || 16}px; height: ${local.size || 16}px;`;

      _v$ !== _p$._v$ && (_el$.className = _p$._v$ = _v$);
      _p$._v$2 = style(_el$, _v$2, _p$._v$2);
      return _p$;
    }, {
      _v$: undefined,
      _v$2: undefined
    });

    return _el$;
  })();
};

const Icon = props => {
  return () => {
    if (props.skeleton === true) {
      const [local, restProps] = splitProps(props, ["size", "skeleton"]);
      return createComponent(IconSkeleton, mergeProps$1({
        get size() {
          return local.size;
        }

      }, restProps, {}));
    } else {
      const [local, restProps] = splitProps(props, ["skeleton", "render"]);
      return createComponent(Dynamic, mergeProps$1({
        get component() {
          return local.render;
        }

      }, restProps, {}));
    }
  };
};

const _tmpl$$$ = template(`<p></p>`, 2);
const Link = props => {
  const [, restProps] = splitProps(props, ["disabled", "href", "inline", "size", "visited", "children", "class"]);
  const classes = createMemo(() => cx("bx--link", props.disabled && "bx--link--disabled", props.inline && "bx--link--inline", props.visited && "bx--link--visited", props.size === "sm" && "bx--link--sm", props.size === "lg" && "bx--link--lg", props.class));
  return () => props.disabled ? (() => {
    const _el$ = _tmpl$$$.cloneNode(true);

    spread(_el$, restProps, false, true);

    insert(_el$, () => props.children);

    effect(() => _el$.className = classes());

    return _el$;
  })() : createComponent(UnstyledLink, mergeProps$1({
    get href() {
      return props.href;
    },

    get ["class"]() {
      return classes();
    }

  }, restProps, {
    get children() {
      return props.children;
    }

  }));
};

const _tmpl$$10 = template(`<div><label></label><div class="bx--slider-container"><span class="bx--slider__range-label"></span><div role="presentation" tabindex="-1"><div role="slider" tabindex="0" class="bx--slider__thumb"></div><div class="bx--slider__track"></div><div class="bx--slider__filled-track"></div></div><span class="bx--slider__range-label"></span><input></div></div>`, 19);
const KEYS = {
  ArrowDown: -1,
  ArrowLeft: -1,
  ArrowRight: 1,
  ArrowUp: 1
};
const Slider = allProps => {
  const defaultId = createMemo(() => uniqueId());
  const props = mergeProps({
    value: () => createSignal(0),
    min: 0,
    max: 100,
    //minLabel: "",
    //maxLabel: "",
    step: 1,
    stepMultiplier: 4,
    required: false,
    inputType: "number",
    disabled: false,
    light: false,
    hideTextInput: false,

    get id() {
      return defaultId();
    },

    get labelId() {
      return `label-${props.id}`;
    },

    invalid: false,
    labelText: "",
    name: "",
    "aria-label": "Slider number input",
    class: undefined
  }, allProps);

  const onDrag = evt => {
    if (props.disabled) return;

    if (evt instanceof MouseEvent) {
      setValue(calcValue(evt.clientX));
    } else if (evt instanceof TouchEvent && evt.touches.length > 0) {
      setValue(calcValue(evt.touches[0].clientX));
    }
  };

  const onDragStop = evt => {
    if (props.disabled) return;
    const doc = trackRef.ownerDocument;
    removeDragStopHandlers(doc);
    removeDragHandlers(doc);
    onDrag(evt);
  };

  const onDragStart = evt => {
    if (props.disabled) return;
    const doc = trackRef.ownerDocument;
    registerDragStopHandlers(doc);
    registerDragHandlers(doc);
    onDrag(evt);
  };

  const registerDragHandlers = doc => {
    doc.addEventListener("mousemove", onDrag);
    doc.addEventListener("touchmove", onDrag);
  };

  const removeDragHandlers = doc => {
    doc.removeEventListener("mousemove", onDrag);
    doc.removeEventListener("touchmove", onDrag);
  };

  const registerDragStopHandlers = doc => {
    doc.addEventListener("mouseup", onDragStop);
    doc.addEventListener("touchend", onDragStop);
    doc.addEventListener("touchcancel", onDragStop);
  };

  const removeDragStopHandlers = doc => {
    doc.removeEventListener("mouseup", onDragStop);
    doc.removeEventListener("touchend", onDragStop);
    doc.removeEventListener("touchcancel", onDragStop);
  };

  onCleanup(() => {
    const doc = trackRef.ownerDocument;
    removeDragStopHandlers(doc);
    removeDragHandlers(doc);
  });
  let trackRef = undefined;
  const [_value, _setValue] = props.value();
  const clampedValue = createMemo(() => clamp(_value(), props.min, props.max));

  const setValue = nextValue => _setValue(clamp(nextValue, props.min, props.max));

  const range = createMemo(() => props.max - props.min);
  const left = createMemo(() => (clampedValue() - props.min) / range() * 100); // clamp it initially

  setValue(_value());

  const formatLabel = (value, valueLabel) => valueLabel !== undefined ? valueLabel : value;

  const calcValue = offsetX => {
    const {
      left,
      width
    } = trackRef.getBoundingClientRect();
    return props.min + Math.round(range() * ((offsetX - left) / width) / props.step) * props.step;
  };

  const onKeyDown = ({
    shiftKey,
    key
  }) => {
    if (props.disabled) return;
    const direction = KEYS[key];

    if (direction) {
      const delta = props.step * (shiftKey ? range() / props.step / props.stepMultiplier : 1) * direction;

      if (delta) {
        setValue(clampedValue() + delta);
      }
    }
  };

  const onChange = evt => {
    if (props.disabled) return;
    const target = evt.target;
    const targetValue = Number.parseFloat(target.value);

    if (isNaN(targetValue)) {
      // XXX
      setValue(props.min);
    } else {
      setValue(targetValue);
    } // Update the <input> value, as clampedValue is a memo.


    target.value = clampedValue().toString();
  };

  return (() => {
    const _el$ = _tmpl$$10.cloneNode(true),
          _el$2 = _el$.firstChild,
          _el$3 = _el$2.nextSibling,
          _el$4 = _el$3.firstChild,
          _el$5 = _el$4.nextSibling,
          _el$6 = _el$5.firstChild,
          _el$7 = _el$6.nextSibling,
          _el$8 = _el$7.nextSibling,
          _el$9 = _el$5.nextSibling,
          _el$10 = _el$9.nextSibling;

    insert(_el$2, () => props.labelText);

    insert(_el$4, () => formatLabel(props.min, props.minLabel));

    addEventListener(_el$5, "keydown", onKeyDown, true);

    addEventListener(_el$5, "touchstart", onDragStart, true);

    addEventListener(_el$5, "mousedown", onDragStart, true);

    const _ref$ = trackRef;
    typeof _ref$ === "function" ? _ref$(_el$7) : trackRef = _el$7;

    insert(_el$9, () => formatLabel(props.max, props.maxLabel));

    addEventListener(_el$10, "change", onChange);

    effect(_p$ => {
      const _v$ = cx("bx--form-item", props.class),
            _v$2 = props.id,
            _v$3 = cx("bx--label", props.disabled && "bx--label--disabled"),
            _v$4 = props.labelId,
            _v$5 = cx("bx--slider", props.disabled && "bx--slider--disabled"),
            _v$6 = props.invalid || undefined,
            _v$7 = `left: ${left()}%`,
            _v$8 = props.labelId,
            _v$9 = props.max,
            _v$10 = props.min,
            _v$11 = clampedValue(),
            _v$12 = props.id,
            _v$13 = `transform: translate(0, -50%) scaleX(${left() / 100})`,
            _v$14 = props.hideTextInput ? "hidden" : props.inputType,
            _v$15 = props.hideTextInput ? "display: none" : undefined,
            _v$16 = `${props.id}-input-for-slider`,
            _v$17 = props.name,
            _v$18 = cx("bx--text-input", "bx--slider-text-input", props.light && "bx--text-input--light", props.invalid && "bx--text-input--invalid"),
            _v$19 = clampedValue(),
            _v$20 = props["aria-label"],
            _v$21 = props.disabled,
            _v$22 = props.required,
            _v$23 = props.min,
            _v$24 = props.max,
            _v$25 = props.step,
            _v$26 = props.invalid || undefined,
            _v$27 = props.invalid || undefined;

      _v$ !== _p$._v$ && (_el$.className = _p$._v$ = _v$);
      _v$2 !== _p$._v$2 && setAttribute(_el$2, "for", _p$._v$2 = _v$2);
      _v$3 !== _p$._v$3 && (_el$2.className = _p$._v$3 = _v$3);
      _v$4 !== _p$._v$4 && setAttribute(_el$2, "id", _p$._v$4 = _v$4);
      _v$5 !== _p$._v$5 && (_el$5.className = _p$._v$5 = _v$5);
      _v$6 !== _p$._v$6 && setAttribute(_el$5, "data-invalid", _p$._v$6 = _v$6);
      _p$._v$7 = style(_el$6, _v$7, _p$._v$7);
      _v$8 !== _p$._v$8 && setAttribute(_el$6, "aria-labelledby", _p$._v$8 = _v$8);
      _v$9 !== _p$._v$9 && setAttribute(_el$6, "aria-valuemax", _p$._v$9 = _v$9);
      _v$10 !== _p$._v$10 && setAttribute(_el$6, "aria-valuemin", _p$._v$10 = _v$10);
      _v$11 !== _p$._v$11 && setAttribute(_el$6, "aria-valuenow", _p$._v$11 = _v$11);
      _v$12 !== _p$._v$12 && setAttribute(_el$6, "id", _p$._v$12 = _v$12);
      _p$._v$13 = style(_el$8, _v$13, _p$._v$13);
      _v$14 !== _p$._v$14 && setAttribute(_el$10, "type", _p$._v$14 = _v$14);
      _p$._v$15 = style(_el$10, _v$15, _p$._v$15);
      _v$16 !== _p$._v$16 && setAttribute(_el$10, "id", _p$._v$16 = _v$16);
      _v$17 !== _p$._v$17 && setAttribute(_el$10, "name", _p$._v$17 = _v$17);
      _v$18 !== _p$._v$18 && (_el$10.className = _p$._v$18 = _v$18);
      _v$19 !== _p$._v$19 && (_el$10.value = _p$._v$19 = _v$19);
      _v$20 !== _p$._v$20 && setAttribute(_el$10, "aria-label", _p$._v$20 = _v$20);
      _v$21 !== _p$._v$21 && (_el$10.disabled = _p$._v$21 = _v$21);
      _v$22 !== _p$._v$22 && (_el$10.required = _p$._v$22 = _v$22);
      _v$23 !== _p$._v$23 && setAttribute(_el$10, "min", _p$._v$23 = _v$23);
      _v$24 !== _p$._v$24 && setAttribute(_el$10, "max", _p$._v$24 = _v$24);
      _v$25 !== _p$._v$25 && setAttribute(_el$10, "step", _p$._v$25 = _v$25);
      _v$26 !== _p$._v$26 && setAttribute(_el$10, "data-invalid", _p$._v$26 = _v$26);
      _v$27 !== _p$._v$27 && setAttribute(_el$10, "aria-invalid", _p$._v$27 = _v$27);
      return _p$;
    }, {
      _v$: undefined,
      _v$2: undefined,
      _v$3: undefined,
      _v$4: undefined,
      _v$5: undefined,
      _v$6: undefined,
      _v$7: undefined,
      _v$8: undefined,
      _v$9: undefined,
      _v$10: undefined,
      _v$11: undefined,
      _v$12: undefined,
      _v$13: undefined,
      _v$14: undefined,
      _v$15: undefined,
      _v$16: undefined,
      _v$17: undefined,
      _v$18: undefined,
      _v$19: undefined,
      _v$20: undefined,
      _v$21: undefined,
      _v$22: undefined,
      _v$23: undefined,
      _v$24: undefined,
      _v$25: undefined,
      _v$26: undefined,
      _v$27: undefined
    });

    return _el$;
  })();
};

delegateEvents(["mousedown", "touchstart", "keydown"]);

const Grid = props => {
  const [local, restProps] = splitProps(props, ["as", "class", "condensed", "fullWidth", "narrow", "noGutter", "noGutterLeft", "noGutterRight", "padding", "children"]);
  const cssClasses = createMemo(() => cx(local.class, "bx--grid", local.condensed && "bx--grid--condensed", local.narrow && "bx--grid--narrow", local.fullWidth && "bx--grid--full-width", local.noGutter && "bx--no-gutter", local.noGutterLeft && "bx--no-gutter--left", local.noGutterRight && "bx--no-gutter--right", local.padding && "bx--row-padding"));
  return createComponent(Dynamic, mergeProps$1({
    get component() {
      return local.as || "div";
    },

    get ["class"]() {
      return cssClasses();
    }

  }, restProps, {
    get children() {
      return local.children;
    }

  }));
};

const Row = props => {
  const [local, restProps] = splitProps(props, ["as", "class", "condensed", "narrow", "noGutter", "noGutterLeft", "noGutterRight", "padding", "children"]);
  const cssClasses = createMemo(() => cx(local.class, "bx--row", local.condensed && "bx--row--condensed", local.narrow && "bx--row--narrow", local.noGutter && "bx--no-gutter", local.noGutterLeft && "bx--no-gutter--left", local.noGutterRight && "bx--no-gutter--right", local.padding && "bx--row-padding"));
  return createComponent(Dynamic, mergeProps$1({
    get component() {
      return local.as || "div";
    },

    get ["class"]() {
      return cssClasses();
    }

  }, restProps, {
    get children() {
      return local.children;
    }

  }));
};

const Column = props => {
  const [local, restProps] = splitProps(props, ["as", "class", "sm", "md", "lg", "xlg", "max", "aspectRatio", "noGutter", "noGutterLeft", "noGutterRight", "padding", "children"]);
  const cssClasses = createMemo(() => {
    const columnClass = cx(columnSpanToClass(local.sm, "sm"), columnSpanToClass(local.md, "md"), columnSpanToClass(local.lg, "lg"), columnSpanToClass(local.xlg, "xlg"), columnSpanToClass(props.max, "max"));
    return cx(local.class, columnClass, columnClass === "" ? "bx--col" : null, local.noGutter && "bx--no-gutter", local.noGutterLeft && "bx--no-gutter--left", local.noGutterRight && "bx--no-gutter--right", local.aspectRatio && `bx--aspect-ratio bx--aspect-ratio--${local.aspectRatio}`, local.padding && "bx--col-padding");
  });
  return createComponent(Dynamic, mergeProps$1({
    get component() {
      return local.as || "div";
    },

    get ["class"]() {
      return cssClasses();
    }

  }, restProps, {
    get children() {
      return local.children;
    }

  }));
};

function columnSpanToClass(breakpoint, name) {
  if (breakpoint === true) {
    // If our breakpoint is a boolean, the user has specified that the column
    // should be "auto" at this size
    return `bx--col-${name}`;
  } else if (typeof breakpoint === "number") {
    // If our breakpoint is a number, the user has specified the number of
    // columns they'd like this column to span
    return `bx--col-${name}-${breakpoint}`;
  } else if (breakpoint !== null && typeof breakpoint === "object") {
    const {
      span,
      offset
    } = breakpoint;
    return cx(typeof span === "number" ? `bx--col-${name}-${span}` : span === true ? `bx--col-${name}` : null, typeof offset === "number" ? `bx--offset-${name}-${offset}` : null);
  } else {
    return null;
  }
}

const _tmpl$$11 = template(`<div role="tablist"></div>`, 2);
// This is just a dummy context in case the <Switch> is used outside of
// a <ContentSwitcher>.
const ContentSwitcherContext = createContext({
  createId: () => "",
  selectedId: () => null,
  register: _id => undefined,
  selectId: _id => undefined,
  selectNext: () => undefined,
  selectPrev: () => undefined
});

function signalFromSelectedId(selectedId) {
  if (typeof selectedId === "string") {
    return createSignal(selectedId);
  }

  if (typeof selectedId === "function") {
    return selectedId();
  }

  if (selectedId === null) {
    return createSignal(null);
  }

  return createSignal("0");
}

const ContentSwitcher = props => {
  const [selectedId, selectId] = signalFromSelectedId(props.selectedId);
  const ids = [];

  const createId = () => {
    return `${ids.length}`;
  };

  const register = id => {
    ids.push(id);
  };

  const selectByIndex = idx => {
    selectId(ids[idx < 0 ? ids.length - 1 : idx >= ids.length ? 0 : idx]);
  };

  const selectOffset = offset => {
    const currentId = selectedId();
    selectByIndex(ids.findIndex(id => id === currentId) + offset);
  };

  const selectNext = () => selectOffset(1);

  const selectPrev = () => selectOffset(-1);

  const store = {
    createId,
    register,
    selectedId,
    selectId,
    selectNext,
    selectPrev
  };
  return (() => {
    const _el$ = _tmpl$$11.cloneNode(true);

    insert(_el$, createComponent(ContentSwitcherContext.Provider, {
      value: store,

      get children() {
        return props.children;
      }

    }));

    effect(() => _el$.className = cx("bx--content-switcher", props.light && "bx--content-switcher--light", props.size === "sm" && "bx--content-switcher--sm", props.size === "xl" && "bx--content-switcher--xl"));

    return _el$;
  })();
};

const _tmpl$$12 = template(`<button role="tab"><span class="bx--content-switcher__label"></span></button>`, 4);
const Switch = props => {
  const {
    createId,
    selectedId,
    register,
    selectId,
    selectNext,
    selectPrev
  } = useContext(ContentSwitcherContext);
  const id = props.id || createId();
  const isSelected = createMemo(() => props.selected || selectedId() === id);
  register(id);

  const onClick = evt => {
    props.onClick && props.onClick(evt);
    evt.preventDefault();
    selectId(id);
  };

  const onKeyDown = ({
    key
  }) => {
    if (key === "ArrowRight") {
      selectNext();
    } else if (key === "ArrowLeft") {
      selectPrev();
    }
  };

  return (() => {
    const _el$ = _tmpl$$12.cloneNode(true),
          _el$2 = _el$.firstChild;

    addEventListener(_el$, "keydown", onKeyDown, true);

    addEventListener(_el$, "click", onClick, true);

    insert(_el$2, () => props.text, null);

    insert(_el$2, () => props.children, null);

    effect(_p$ => {
      const _v$ = isSelected() ? "0" : "-1",
            _v$2 = isSelected() || undefined,
            _v$3 = props.disabled || undefined,
            _v$4 = cx("bx--content-switcher-btn", isSelected() && "bx--content-switcher--selected", props.class);

      _v$ !== _p$._v$ && setAttribute(_el$, "tabindex", _p$._v$ = _v$);
      _v$2 !== _p$._v$2 && setAttribute(_el$, "aria-selected", _p$._v$2 = _v$2);
      _v$3 !== _p$._v$3 && (_el$.disabled = _p$._v$3 = _v$3);
      _v$4 !== _p$._v$4 && (_el$.className = _p$._v$4 = _v$4);
      return _p$;
    }, {
      _v$: undefined,
      _v$2: undefined,
      _v$3: undefined,
      _v$4: undefined
    });

    return _el$;
  })();
};

delegateEvents(["click", "keydown"]);

const _tmpl$$13 = template(`<nav><ol></ol></nav>`, 4);
const Breadcrumb = props => {
  const [local, restProps] = splitProps(props, ["aria-label", "noTrailingSlash", "children"]);
  return (() => {
    const _el$ = _tmpl$$13.cloneNode(true),
          _el$2 = _el$.firstChild;

    spread(_el$, restProps, false, true);

    insert(_el$2, () => local.children);

    effect(_p$ => {
      const _v$ = local["aria-label"] ? local["aria-label"] : "Breadcrumb",
            _v$2 = {
        "bx--breadcrumb": true,
        "bx--breadcrumb--no-trailing-slash": local.noTrailingSlash
      };

      _v$ !== _p$._v$ && setAttribute(_el$, "aria-label", _p$._v$ = _v$);
      _p$._v$2 = classList(_el$2, _v$2, _p$._v$2);
      return _p$;
    }, {
      _v$: undefined,
      _v$2: undefined
    });

    return _el$;
  })();
};

const _tmpl$$14 = template(`<li></li>`, 2);
const BreadcrumbItem = props => {
  const [local, restProps] = splitProps(props, ["aria-current", "href", "isCurrentPage", "children", "class"]);

  const liClasses = () => cx("bx--breadcrumb-item", local.isCurrentPage && local["aria-current"] !== "page" && "bx--breadcrumb-item--current", local.class);

  return (() => {
    const _el$ = _tmpl$$14.cloneNode(true);

    spread(_el$, restProps, false, true);

    insert(_el$, createComponent(Link, {
      get href() {
        return local.href;
      },

      get ["aria-current"]() {
        return local["aria-current"];
      },

      get children() {
        return local.children;
      }

    }));

    effect(() => _el$.className = liClasses());

    return _el$;
  })();
};

const _tmpl$$15 = template(`<div></div>`, 2),
      _tmpl$2$A = template(`<div class="bx--breadcrumb-item"><span class="bx--link">&nbsp;</span></div>`, 4);
const BreadcrumbSkeleton = props => {
  const [local, restProps] = splitProps(props, ["noTrailingSlash", "count", "children", "class"]);

  const classes = () => cx("bx--skeleton", "bx--breadcrumb", local.noTrailingSlash && "bx--breadcrumb--no-trailing-slash", local.class);

  return (() => {
    const _el$ = _tmpl$$15.cloneNode(true);

    spread(_el$, restProps, false, true);

    insert(_el$, () => Array.from({
      length: props.count || 3
    }, () => _tmpl$2$A.cloneNode(true)));

    effect(() => _el$.className = classes());

    return _el$;
  })();
};

export { AllThemes, Breadcrumb, BreadcrumbItem, BreadcrumbSkeleton, Button, ButtonSet, ButtonSkeleton, Checkbox, CheckboxSkeleton, ClickableTile, Column, Content, ContentSwitcher, CustomLinkContext, DataTable, Form, FormGroup, FormItem, FormLabel, Grid, HamburgerMenu, Header, HeaderGlobalAction, HeaderNav, HeaderNavItem, HeaderUtilities, Icon, IconSkeleton, InlineCheckbox, InlineLoading, InlineNotification, Link, Loading, Modal, Notification, NotificationActionButton, NotificationButton, NotificationIcon, NotificationTextDetails, OptDataTable, OptDataTableModel, PasswordInput, Row, SideNav, SideNavIcon, SideNavItem, SideNavItems, SideNavLink, SideNavLinkText, Slider, Switch, Table, TableBody, TableCell, TableContainer, TableHead, TableHeader, TableRow, TextInput, TextInputSkeleton, Theme, Tile, ToastNotification, Toolbar, ToolbarContent, UnstyledLink };
