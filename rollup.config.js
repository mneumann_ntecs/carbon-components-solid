import nodeResolve from "@rollup/plugin-node-resolve";
import babel from "@rollup/plugin-babel";

const extensions = [".js", ".tsx", ".ts"];

const plugins = [
  nodeResolve({
    extensions,
  }),
  babel({
    extensions,
    babelrc: false,
    exclude: "node_modules/**",
    babelHelpers: "bundled",
    presets: ["solid", "@babel/preset-typescript"],
  }),
];

export default [
  {
    input: "src/index.ts",
    output: [
      {
        file: "dist/index.js",
        format: "es",
      },
    ],
    external: ["solid-js", "solid-js/web"],
    plugins,
  },
];
