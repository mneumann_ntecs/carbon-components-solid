export default {
  title: "Breadcrumb",
};

import { Breadcrumb } from "./Breadcrumb";
import { BreadcrumbItem } from "./BreadcrumbItem";
import { BreadcrumbSkeleton } from "./BreadcrumbSkeleton";

export const Default = () => (
  <Breadcrumb>
    <BreadcrumbItem href="/">Dashboard</BreadcrumbItem>
    <BreadcrumbItem href="/reports">Annual reports</BreadcrumbItem>
    <BreadcrumbItem href="/reports/2019" isCurrentPage>
      2019
    </BreadcrumbItem>
  </Breadcrumb>
);

export const NoTrailingSlash = () => (
  <Breadcrumb noTrailingSlash>
    <BreadcrumbItem href="/">Home</BreadcrumbItem>
    <BreadcrumbItem href="/profile" isCurrentPage>
      Profile
    </BreadcrumbItem>
  </Breadcrumb>
);

export const Skeleton = () => <BreadcrumbSkeleton noTrailingSlash count={3} />;
