import { Component, splitProps } from "solid-js";

type BreadcrumbProps = {
  "aria-label"?: string;
  noTrailingSlash?: boolean;
};

export const Breadcrumb: Component<BreadcrumbProps> = (props) => {
  const [local, restProps] = splitProps(props, [
    "aria-label",
    "noTrailingSlash",
    "children",
  ]);

  return (
    <nav
      aria-label={local["aria-label"] ? local["aria-label"] : "Breadcrumb"}
      {...restProps}
    >
      <ol
        classList={{
          "bx--breadcrumb": true,
          "bx--breadcrumb--no-trailing-slash": local.noTrailingSlash,
        }}
      >
        {local.children}
      </ol>
    </nav>
  );
};
