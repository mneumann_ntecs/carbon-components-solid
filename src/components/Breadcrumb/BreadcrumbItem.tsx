import { Component, splitProps } from "solid-js";
import { Link } from "../Link/Link";
import { cx } from "../../utils";

type BreadcrumbItemProps = {
  "aria-current"?: string | boolean;
  href?: string;
  isCurrentPage?: boolean;
  class?: string;
};

export const BreadcrumbItem: Component<BreadcrumbItemProps> = (props) => {
  const [local, restProps] = splitProps(props, [
    "aria-current",
    "href",
    "isCurrentPage",
    "children",
    "class",
  ]);

  const liClasses = () =>
    cx(
      "bx--breadcrumb-item",
      local.isCurrentPage &&
        local["aria-current"] !== "page" &&
        "bx--breadcrumb-item--current",
      local.class
    );

  return (
    <li class={liClasses()} {...restProps}>
      <Link href={local.href} aria-current={local["aria-current"]}>
        {local.children}
      </Link>
    </li>
  );
};
