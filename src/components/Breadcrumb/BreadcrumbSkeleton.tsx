import { Component, splitProps } from "solid-js";
import { cx } from "../../utils";

type BreadcrumbSkeletonProps = {
  noTrailingSlash?: boolean;

  /** Specify the number of breadcrumb items. Default: 3 */
  count?: number;

  class?: string;
};

export const BreadcrumbSkeleton: Component<BreadcrumbSkeletonProps> = (
  props
) => {
  const [local, restProps] = splitProps(props, [
    "noTrailingSlash",
    "count",
    "children",
    "class",
  ]);

  const classes = () =>
    cx(
      "bx--skeleton",
      "bx--breadcrumb",
      local.noTrailingSlash && "bx--breadcrumb--no-trailing-slash",
      local.class
    );

  return (
    <div class={classes()} {...restProps}>
      {Array.from({ length: props.count || 3 }, () => (
        <div class="bx--breadcrumb-item">
          <span class="bx--link">&nbsp;</span>
        </div>
      ))}
    </div>
  );
};
