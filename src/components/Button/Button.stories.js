export default {
  title: "Button",
};

import { Button } from "./Button";

export const ButtonKinds = () => {
  const kinds = [
    "primary",
    "secondary",
    "tertiary",
    "ghost",
    "danger",
    "danger-tertiary",
    "danger-ghost",
  ];

  return (
    <>
      <For each={kinds}>{(kind) => <Button kind={kind}>{kind}</Button>}</For>
    </>
  );
};
