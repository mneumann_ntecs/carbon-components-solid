import { ButtonSkeleton } from "./ButtonSkeleton";
import { UnstyledLink } from "../Link/UnstyledLink";
import { JSX, Component } from "solid-js";
import { Dynamic } from "solid-js/web";
import {
  TooltipPosition,
  tooltipPositionClass,
} from "../../helpers/TooltipPosition";
import {
  TooltipAlignment,
  tooltipAlignmentClass,
} from "../../helpers/TooltipAlignment";
import { CarbonIconComponent } from "carbon-icons-solid/lib/types";
import { cx } from "../../utils";

type ButtonKind =
  | "primary"
  | "secondary"
  | "tertiary"
  | "ghost"
  | "danger"
  | "danger-tertiary"
  | "danger-ghost";

type ButtonSize = "field" | "small";

type ButtonProps = {
  kind?: ButtonKind; // default "primary"
  size?: ButtonSize;
  type?: "button" | "submit"; // default: "button"

  hasIconOnly?: boolean;
  tooltipPosition?: TooltipPosition;
  tooltipAlignment?: TooltipAlignment;

  disabled?: boolean;
  skeleton?: boolean;

  iconDescription?: string;
  icon?: CarbonIconComponent;

  href?: string;
  tabindex?: string; // default "0"

  /** Additional classes for the button */
  class?: string;

  children?: JSX.Element;

  onClick?: JSX.EventHandler<HTMLButtonElement, MouseEvent>;

  style?: string;

  restProps?: any;
};

export const Button: Component<ButtonProps> = (props) => {
  const body = () => (
    <>
      {props.hasIconOnly && (
        <span class="bx--assistive-text">{props.iconDescription}</span>
      )}
      {props.children}
      {props.icon && (
        <Dynamic
          component={props.icon}
          aria-hidden="true"
          class="bx--btn__icon"
          aria-label={props.iconDescription}
        />
      )}
    </>
  );

  if (props.skeleton) {
    return (
      <ButtonSkeleton
        href={props.href}
        size={props.size}
        onClick={props.onClick}
        style={props.hasIconOnly ? `width: 3rem; ${props.style}` : props.style}
        class={props.class}
        {...props.restProps}
      />
    );
  }

  if (props.href && !props.disabled) {
    return (
      <UnstyledLink
        role="button"
        href={props.href}
        tabindex={props.tabindex || "0"}
        class={buttonClass(props)}
        style={props.style}
        children={body()}
      />
    );
  }

  return (
    <button
      type={props.type || "button"}
      tabindex={props.tabindex || "0"}
      disabled={props.disabled}
      class={buttonClass(props)}
      onClick={props.onClick}
      style={props.style}
      {...props.restProps}
    >
      {body()}
    </button>
  );
};

const buttonKindClass = (kind: ButtonKind) => `bx--btn--${kind}`;

const buttonClass = (props: ButtonProps) =>
  cx(
    props.class,
    "bx--btn",
    props.size === "field" && "bx--btn--field",
    props.size === "small" && "bx--btn--sm",
    buttonKindClass(props.kind || "primary"),
    props.disabled && "bx--btn--disabled",
    props.hasIconOnly &&
      "bx--btn--icon-only bx--tooltip__trigger bx--tooltip--a11y",
    tooltipPositionClass(props.hasIconOnly ? props.tooltipPosition : undefined),
    tooltipAlignmentClass(
      props.hasIconOnly ? props.tooltipAlignment : undefined
    )
  );
