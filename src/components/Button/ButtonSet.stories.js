export default {
  title: "ButtonSet",
};

import { createSignal } from "solid-js";
import { Button } from "./Button";
import { ButtonSet } from "./ButtonSet";

export const Default = () => {
  const [stacked, setStacked] = createSignal(false);
  return (
    <ButtonSet stacked={stacked()}>
      <Button kind="primary" onClick={() => setStacked(!stacked())}>
        Toggle Stacked
      </Button>
      <Button kind="secondary">Secondary</Button>
      <Button kind="tertiary">Tertiary</Button>
    </ButtonSet>
  );
};
