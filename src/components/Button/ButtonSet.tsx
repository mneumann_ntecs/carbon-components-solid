import { splitProps } from "solid-js";
import { cx } from "../../utils";

export const ButtonSet = (props) => {
  const [local, restProps] = splitProps(props, ["stacked", "class"]);
  return (
    <div
      class={cx(
        "bx--btn-set",
        local.stacked && "bx--btn-set--stacked",
        local.class
      )}
      {...restProps}
    />
  );
};
