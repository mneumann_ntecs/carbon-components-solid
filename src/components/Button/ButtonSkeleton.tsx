import { splitProps } from "solid-js";
import { cx } from "../../utils";
import { UnstyledLink } from "../Link/UnstyledLink";

export const ButtonSkeleton = (props) => () => {
  const [, restProps] = splitProps(props, ["size", "href", "class"]);
  const classes = () =>
    cx(
      "bx--skeleton",
      "bx--btn",
      props.size === "field" && "bx--btn--field",
      props.size === "small" && "bx--btn--sm",
      props.class
    );

  return props.href === undefined ? (
    <div class={classes()} {...restProps} />
  ) : (
    <UnstyledLink
      role="button"
      href={props.href}
      class={classes()}
      children={""}
      {...restProps}
    />
  );
};
