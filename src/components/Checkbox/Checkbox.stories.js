export default {
  title: "Checkbox",
};

import { createSignal } from "solid-js";
import { Checkbox } from "./Checkbox";

export const Example = () => {
  const [agree, setAgree] = createSignal(undefined);
  return (
    <>
      <Checkbox
        indeterminate={agree() === undefined}
        checked={agree()}
        setChecked={setAgree}
        labelText="Agree Terms of Service"
        invalid={agree() === undefined}
        invalidText="Please check"
      />
      <p>
        {agree()
          ? "Is checked"
          : agree() === undefined
          ? "Indeterminate"
          : "Is not checked"}
      </p>
    </>
  );
};

export const Skeleton = () => {
  return <Checkbox skeleton />;
};
