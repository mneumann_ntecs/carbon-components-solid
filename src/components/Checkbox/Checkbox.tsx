import { Show, splitProps, mergeProps, createMemo } from "solid-js";
import { uniqueId, cx } from "../../utils";
import { CheckboxSkeleton } from "./CheckboxSkeleton";

export const Checkbox = (allProps) => {
  const [ownProps, restProps] = splitProps(allProps, [
    "checked",
    "indeterminate",
    "skeleton",
    "readonly",
    "disabled",
    "labelText",
    "hideLabel",
    "name",
    "title",
    "id",
    "setChecked",
    "invalid",
    "invalidText",
    "onBlur",
    "class",
  ]);
  const props = mergeProps(
    {
      checked: false,
      indeterminate: false,
      skeleton: false,
      readonly: false,
      disabled: false,
      labelText: "",
      hideLabel: false,
      title: undefined,
      name: "",
      invalid: false,
      invalidText: undefined,
    },
    ownProps
  );

  const id = createMemo(() => props.id || uniqueId());
  const errorId = createMemo(() => `error-${id()}`);

  const onChange = () => {
    const setter = props.setChecked;
    if (setter) setter(!props.checked);
  };

  return (
    <>
      {props.skeleton ? (
        <CheckboxSkeleton class={props.class} {...restProps} />
      ) : (
        <div
          class={cx("bx--form-item", "bx--checkbox-wrapper", props.class)}
          {...restProps}
        >
          <input
            data-invalid={props.invalid || undefined}
            aria-describedby={props.invalid ? errorId() : undefined}
            type="checkbox"
            checked={props.checked}
            disabled={props.disabled}
            id={id()}
            indeterminate={props.indeterminate}
            name={props.name}
            readonly={props.readonly}
            class="bx--checkbox"
            onChange={onChange}
            onBlur={props.onBlur}
          />
          <label for={id()} title={props.title} class="bx--checkbox-label">
            <span
              class={cx(
                "bx--checkbox-label-text",
                props.hideLabel && "bx--visually-hidden"
              )}
            >
              {props.labelText}
            </span>
          </label>
          <Show when={props.invalid}>
            <div class="bx--form-requirement" id={errorId()}>
              {props.invalidText}
            </div>
          </Show>
        </div>
      )}
    </>
  );
};
