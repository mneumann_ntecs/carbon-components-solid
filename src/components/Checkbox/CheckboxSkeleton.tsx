import { splitProps } from "solid-js";
import { cx } from "../../utils";

export const CheckboxSkeleton = (props) => {
  const [local, restProps] = splitProps(props, ["class"]);

  return (
    <div
      class={cx(
        "bx--form-item",
        "bx--checkbox-wrapper",
        "bx--checkbox-label",
        local.class
      )}
      {...restProps}
    >
      <span class="bx--checkbox-label-text bx--skeleton"></span>
    </div>
  );
};
