import { splitProps, mergeProps, createMemo } from "solid-js";
import { uniqueId, cx } from "../../utils";

export const InlineCheckbox = (allProps) => {
  const [ownProps, restProps] = splitProps(allProps, [
    "checked",
    "indeterminate",
    "title",
    "id",
    "aria-label",
    "class",
  ]);

  const props = mergeProps(
    {
      checked: false,
      indeterminate: false,
      title: undefined,
      id: undefined,
      "aria-label": undefined,
    },
    ownProps
  );

  const id = createMemo(() => props.id || uniqueId());

  return (
    <>
      <input
        type="checkbox"
        class={cx("bx--checkbox", props.class)}
        checked={props.indeterminate ? false : props.checked}
        indeterminate={props.indeterminate}
        id={id()}
        {...restProps}
        aria-checked={props.indeterminate ? "mixed" : props.checked}
      />
      <label
        for={id()}
        title={props.title}
        aria-label={props["aria-label"]}
        class="bx--checkbox-label"
      ></label>
    </>
  );
};
