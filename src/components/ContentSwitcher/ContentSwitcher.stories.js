export default {
  title: "ContentSwitcher",
};

import { ContentSwitcher } from "./ContentSwitcher";
import { Switch } from "./Switch";
import { Button } from "../Button/Button";
import { createSignal } from "solid-js";
import { Bullhorn16 } from "carbon-icons-solid/lib/Bullhorn16";
import { Analytics16 } from "carbon-icons-solid/lib/Analytics16";

export const Default = () => (
  <ContentSwitcher>
    <Switch text="Latest news" />
    <Switch text="Trending" />
  </ContentSwitcher>
);

export const InitiallySelectedId = () => (
  <ContentSwitcher selectedId="1">
    <Switch text="Latest news" />
    <Switch text="Trending" />
    <Switch text="Recommended" />
  </ContentSwitcher>
);

export const ReactiveExample = () => {
  const [selectedId, selectId] = createSignal("1");

  return (
    <>
      <ContentSwitcher selectedId={() => [selectedId, selectId]}>
        <Switch text="Latest news" />
        <Switch text="Trending" />
        <Switch text="Recommended" />
      </ContentSwitcher>

      <div style="margin-top: var(--cds-spacing-05)">
        <Button
          size="small"
          disabled={selectedId() === "2"}
          onClick={() => selectId("2")}
        >
          Set selected to 2
        </Button>
      </div>

      <div>Selected id: {selectedId()}</div>
    </>
  );
};

export const LightVariant = () => (
  <ContentSwitcher light>
    <Switch text="Latest news" />
    <Switch text="Trending" />
  </ContentSwitcher>
);

export const CustomRendering = () => (
  <ContentSwitcher>
    <Switch>
      <div style="display: flex; align-items: center;">
        <Bullhorn16 style="margin-right: 0.5rem;" />
        Latest news
      </div>
    </Switch>
    <Switch>
      <div style="display: flex; align-items: center;">
        <Analytics16 style="margin-right: 0.5rem;" />
        Trending
      </div>
    </Switch>
  </ContentSwitcher>
);

export const ExtraLargeSize = () => (
  <ContentSwitcher size="xl">
    <Switch text="All" />
    <Switch text="Archived" />
  </ContentSwitcher>
);

export const SmallSize = () => (
  <ContentSwitcher size="sm">
    <Switch text="All" />
    <Switch text="Archived" />
  </ContentSwitcher>
);

export const Disabled = () => (
  <ContentSwitcher>
    <Switch disabled text="All" />
    <Switch disabled text="Archived" />
  </ContentSwitcher>
);
