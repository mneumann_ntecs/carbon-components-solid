import { Component, createContext, createSignal } from "solid-js";
import { Signal, cx } from "../../utils";

type ContentSwitcherProps = {
  light?: boolean;
  size?: "sm" | "xl";
  selectedId?: (() => Signal<string | null>) | string | null;
};

export type ContentSwitcherContextType = {
  createId: () => string;
  selectedId: () => string | null;
  register: (id: string) => void;
  selectId: (id: string) => void;
  selectNext: () => void;
  selectPrev: () => void;
};

// This is just a dummy context in case the <Switch> is used outside of
// a <ContentSwitcher>.
export const ContentSwitcherContext = createContext<ContentSwitcherContextType>(
  {
    createId: () => "",
    selectedId: () => null,
    register: (_id: string) => undefined,
    selectId: (_id: string) => undefined,
    selectNext: () => undefined,
    selectPrev: () => undefined,
  }
);

function signalFromSelectedId(
  selectedId: (() => Signal<string | null>) | string | null | undefined
): Signal<string | null> {
  if (typeof selectedId === "string") {
    return createSignal(selectedId) as Signal<string | null>;
  }
  if (typeof selectedId === "function") {
    return selectedId();
  }
  if (selectedId === null) {
    return createSignal(null) as Signal<string | null>;
  }
  return createSignal("0") as Signal<string | null>;
}

export const ContentSwitcher: Component<ContentSwitcherProps> = (props) => {
  const [selectedId, selectId]: Signal<string | null> = signalFromSelectedId(
    props.selectedId
  );

  const ids: Array<string> = [];

  const createId = () => {
    return `${ids.length}`;
  };

  const register = (id: string) => {
    ids.push(id);
  };

  const selectByIndex = (idx: number) => {
    selectId(ids[idx < 0 ? ids.length - 1 : idx >= ids.length ? 0 : idx]);
  };

  const selectOffset = (offset: number) => {
    const currentId = selectedId();
    selectByIndex(ids.findIndex((id) => id === currentId) + offset);
  };

  const selectNext = () => selectOffset(1);
  const selectPrev = () => selectOffset(-1);

  const store = {
    createId,
    register,
    selectedId,
    selectId,
    selectNext,
    selectPrev,
  };

  return (
    <div
      role="tablist"
      class={cx(
        "bx--content-switcher",
        props.light && "bx--content-switcher--light",
        props.size === "sm" && "bx--content-switcher--sm",
        props.size === "xl" && "bx--content-switcher--xl"
      )}
    >
      <ContentSwitcherContext.Provider value={store}>
        {props.children}
      </ContentSwitcherContext.Provider>
    </div>
  );
};
