import { Component, JSX, useContext, createMemo } from "solid-js";
import {
  ContentSwitcherContext,
  ContentSwitcherContextType,
} from "./ContentSwitcher";
import { cx } from "../../utils";

type SwitchProps = {
  text?: string;
  selected?: boolean;
  disabled?: boolean;
  onClick?: JSX.EventHandler<HTMLButtonElement, MouseEvent>;
  id?: string;
  class?: string;
};

export const Switch: Component<SwitchProps> = (props) => {
  const {
    createId,
    selectedId,
    register,
    selectId,
    selectNext,
    selectPrev,
  }: ContentSwitcherContextType = useContext(ContentSwitcherContext);

  const id = props.id || createId();
  const isSelected = createMemo(() => props.selected || selectedId() === id);

  register(id);

  const onClick = (evt: MouseEvent) => {
    props.onClick && props.onClick(evt);
    evt.preventDefault();
    selectId(id);
  };

  const onKeyDown = ({ key }: KeyboardEvent) => {
    if (key === "ArrowRight") {
      selectNext();
    } else if (key === "ArrowLeft") {
      selectPrev();
    }
  };

  return (
    <button
      role="tab"
      tabindex={isSelected() ? "0" : "-1"}
      aria-selected={isSelected() || undefined}
      disabled={props.disabled || undefined}
      class={cx(
        "bx--content-switcher-btn",
        isSelected() && "bx--content-switcher--selected",
        props.class
      )}
      onClick={onClick}
      onKeyDown={onKeyDown}
    >
      <span class="bx--content-switcher__label">
        {props.text}
        {props.children}
      </span>
    </button>
  );
};
