export default {
  title: "Data Table",
};

import { createMemo, createSignal } from "solid-js";

import {
  TableContainer,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  DataTable,
  OptDataTable,
  OptDataTableModel,
  Toolbar,
  ToolbarContent,
  Link,
} from "../index";
import { Launch16 } from "carbon-icons-solid/lib/Launch16";

export const _Table = () => {
  let array = Array.from({ length: 100 }, (_, i) => [i, i + 1, i + 2]);

  return (
    <TableContainer
      title="A and B and C"
      description="Description"
      stickyHeader
    >
      <Table size="compact" zebra shouldShowBorder stickyHeader sortable>
        <TableHead>
          <TableRow>
            <TableCell>A</TableCell>
            <TableCell>B</TableCell>
            <TableCell>C</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <For each={array}>
            {(row) => (
              <TableRow>
                <For each={row}>
                  {(column) => <TableCell>{column}</TableCell>}
                </For>
              </TableRow>
            )}
          </For>
        </TableBody>
      </Table>
    </TableContainer>
  );
};

const DataTableHeaders = [
  { key: "name", value: "Name" },
  { key: "protocol", value: "Protocol" },
  { key: "port", value: "Port" },
  { key: "rule", value: "Rule" },
];

const DataTableRows = [
  {
    id: "a",
    name: "Load Balancer 3",
    protocol: "HTTP",
    port: 3000,
    rule: "Round robin",
  },
  {
    id: "b",
    name: "Load Balancer 1",
    protocol: "HTTP",
    port: 443,
    rule: "Round robin",
  },
  {
    id: "c",
    name: "Load Balancer 2",
    protocol: "HTTP",
    port: 80,
    rule: "DNS delegation",
  },
  {
    id: "d",
    name: "Load Balancer 6",
    protocol: "HTTP",
    port: 3000,
    rule: "Round robin",
  },
  {
    id: "e",
    name: "Load Balancer 4",
    protocol: "HTTP",
    port: 443,
    rule: "Round robin",
  },
  {
    id: "f",
    name: "Load Balancer 5",
    protocol: "HTTP",
    port: 80,
    rule: "DNS delegation",
  },
];

export const Default = () => (
  <DataTable headers={DataTableHeaders} rows={DataTableRows} />
);

export const WithCustomCellRenderers = () => {
  const renderCellHeader = (header) =>
    header.key === "port" ? `${header.value} (network)` : header.value;

  const renderCell = (row, cell) => {
    if (cell.key === "rule" && cell.value === "Round robin") {
      return (
        <Link
          inline
          target="_blank"
          href="https://en.wikipedia.org/wiki/Round-robin_DNS"
        >
          {cell.value} <Launch16 />
        </Link>
      );
    } else {
      return cell.value;
    }
  };

  return (
    <DataTable
      headers={DataTableHeaders}
      rows={DataTableRows}
      renderCellHeader={renderCellHeader}
      renderCell={renderCell}
    />
  );
};

export const WithTitleAndDescription = () => {
  return (
    <DataTable
      title="Load balancers"
      description="Your organization's active load balancers."
      headers={DataTableHeaders}
      rows={DataTableRows}
    />
  );
};

export const ZebraStripes = () => (
  <DataTable zebra headers={DataTableHeaders} rows={DataTableRows} />
);

export const TallRows = () => (
  <DataTable size="tall" headers={DataTableHeaders} rows={DataTableRows} />
);

export const ShortRows = () => (
  <DataTable size="short" headers={DataTableHeaders} rows={DataTableRows} />
);

export const CompactRows = () => (
  <DataTable size="compact" headers={DataTableHeaders} rows={DataTableRows} />
);

export const Sortable = () => (
  <DataTable sortable headers={DataTableHeaders} rows={DataTableRows} />
);

export const SortableWithCustomDisplayAndSortMethods = () => (
  <DataTable
    sortable
    title="Load balancers"
    description="Your organization's active load balancers."
    headers={[
      { key: "name", value: "Name" },
      { key: "protocol", value: "Protocol" },
      { key: "port", value: "Port" },
      { key: "cost", value: "Cost", display: (cost) => cost + " €" },
      {
        key: "expireDate",
        value: "Expire date",
        display: (date) => new Date(date).toLocaleString(),
        sort: (a, b) => new Date(a) - new Date(b),
      },
    ]}
    rows={[
      {
        id: "a",
        name: "Load Balancer 3",
        protocol: "HTTP",
        port: 3000,
        cost: 100,
        expireDate: "2020-10-21",
      },
      {
        id: "b",
        name: "Load Balancer 1",
        protocol: "HTTP",
        port: 443,
        cost: 200,
        expireDate: "2020-09-10",
      },
      {
        id: "c",
        name: "Load Balancer 2",
        protocol: "HTTP",
        port: 80,
        cost: 150,
        expireDate: "2020-11-24",
      },
      {
        id: "d",
        name: "Load Balancer 6",
        protocol: "HTTP",
        port: 3000,
        cost: 250,
        expireDate: "2020-12-01",
      },
      {
        id: "e",
        name: "Load Balancer 4",
        protocol: "HTTP",
        port: 443,
        cost: 550,
        expireDate: "2021-03-21",
      },
      {
        id: "f",
        name: "Load Balancer 5",
        protocol: "HTTP",
        port: 80,
        cost: 400,
        expireDate: "2020-11-14",
      },
    ]}
  />
);

export const Selectable = () => {
  const headers = [
    { key: "name", value: "Name" },
    { key: "port", value: "Port" },
    { key: "rule", value: "Rule" },
  ];

  const rows = [
    { id: "a", name: "Load Balancer 3", port: 3000, rule: "Round robin" },
    { id: "b", name: "Load Balancer 1", port: 443, rule: "Round robin" },
    { id: "c", name: "Load Balancer 2", port: 80, rule: "DNS delegation" },
    { id: "d", name: "Load Balancer 6", port: 3000, rule: "Round robin" },
    { id: "e", name: "Load Balancer 4", port: 443, rule: "Round robin" },
    { id: "f", name: "Load Balancer 5", port: 80, rule: "DNS delegation" },
  ];

  const [selectedRowIds, setSelectedRowIds] = createSignal([]);

  return (
    <>
      <p>Selected rows: {selectedRowIds().join(", ")}</p>
      <DataTable
        selectable
        selectedRowIds={selectedRowIds()}
        setSelectedRowIds={setSelectedRowIds}
        headers={headers}
        rows={rows}
      />
    </>
  );
};

export const InitialSelectedRows = () => {
  const headers = [
    { key: "name", value: "Name" },
    { key: "port", value: "Port" },
    { key: "rule", value: "Rule" },
  ];

  const rows = [
    { id: "a", name: "Load Balancer 3", port: 3000, rule: "Round robin" },
    { id: "b", name: "Load Balancer 1", port: 443, rule: "Round robin" },
    { id: "c", name: "Load Balancer 2", port: 80, rule: "DNS delegation" },
    { id: "d", name: "Load Balancer 6", port: 3000, rule: "Round robin" },
    { id: "e", name: "Load Balancer 4", port: 443, rule: "Round robin" },
    { id: "f", name: "Load Balancer 5", port: 80, rule: "DNS delegation" },
  ];

  const [selectedRowIds, setSelectedRowIds] = createSignal([
    rows[0].id,
    rows[1].id,
  ]);

  return (
    <DataTable
      batchSelection
      selectedRowIds={selectedRowIds()}
      setSelectedRowIds={setSelectedRowIds}
      headers={headers}
      rows={rows}
    >
      <p>Selected rows: {selectedRowIds().join(", ")}</p>
    </DataTable>
  );
};

export const SelectableWithBatchActions = () => {
  const headers = [
    { key: "name", value: "Name" },
    { key: "port", value: "Port" },
    { key: "rule", value: "Rule" },
  ];

  const rows = [
    { id: "a", name: "Load Balancer 3", port: 3000, rule: "Round robin" },
    { id: "b", name: "Load Balancer 1", port: 443, rule: "Round robin" },
    { id: "c", name: "Load Balancer 2", port: 80, rule: "DNS delegation" },
    { id: "d", name: "Load Balancer 6", port: 3000, rule: "Round robin" },
    { id: "e", name: "Load Balancer 4", port: 443, rule: "Round robin" },
    { id: "f", name: "Load Balancer 5", port: 80, rule: "DNS delegation" },
  ];

  const [selectedRowIds, setSelectedRowIds] = createSignal([]);

  return (
    <DataTable
      batchSelection
      selectedRowIds={selectedRowIds()}
      setSelectedRowIds={setSelectedRowIds}
      headers={headers}
      rows={rows}
    >
      <Toolbar>
        <ToolbarContent>
          <p>Selected rows: {selectedRowIds().join(", ")}</p>
        </ToolbarContent>
      </Toolbar>
    </DataTable>
  );
};

export const Expandable = () => {
  const [expandedRowIds, setExpandedRowIds] = createSignal([]);

  return (
    <DataTable
      expandable
      expandedRowIds={expandedRowIds()}
      setExpandedRowIds={setExpandedRowIds}
      headers={[
        { key: "name", value: "Name" },
        { key: "protocol", value: "Protocol" },
        { key: "port", value: "Port" },
        { key: "rule", value: "Rule" },
      ]}
      rows={[
        {
          id: "a",
          name: "Load Balancer 3",
          protocol: "HTTP",
          port: 3000,
          rule: "Round robin",
        },
        {
          id: "b",
          name: "Load Balancer 1",
          protocol: "HTTP",
          port: 443,
          rule: "Round robin",
        },
        {
          id: "c",
          name: "Load Balancer 2",
          protocol: "HTTP",
          port: 80,
          rule: "DNS delegation",
        },
        {
          id: "d",
          name: "Load Balancer 6",
          protocol: "HTTP",
          port: 3000,
          rule: "Round robin",
        },
        {
          id: "e",
          name: "Load Balancer 4",
          protocol: "HTTP",
          port: 443,
          rule: "Round robin",
        },
        {
          id: "f",
          name: "Load Balancer 5",
          protocol: "HTTP",
          port: 80,
          rule: "DNS delegation",
        },
      ]}
      renderExpandedRow={(row) => <pre>{JSON.stringify(row, null, 2)}</pre>}
    />
  );
};

export const BatchExpansion = () => {
  const [expandedRowIds, setExpandedRowIds] = createSignal([]);

  return (
    <DataTable
      batchExpansion
      expandedRowIds={expandedRowIds()}
      setExpandedRowIds={setExpandedRowIds}
      headers={[
        { key: "name", value: "Name" },
        { key: "protocol", value: "Protocol" },
        { key: "port", value: "Port" },
        { key: "rule", value: "Rule" },
      ]}
      rows={[
        {
          id: "a",
          name: "Load Balancer 3",
          protocol: "HTTP",
          port: 3000,
          rule: "Round robin",
        },
        {
          id: "b",
          name: "Load Balancer 1",
          protocol: "HTTP",
          port: 443,
          rule: "Round robin",
        },
        {
          id: "c",
          name: "Load Balancer 2",
          protocol: "HTTP",
          port: 80,
          rule: "DNS delegation",
        },
        {
          id: "d",
          name: "Load Balancer 6",
          protocol: "HTTP",
          port: 3000,
          rule: "Round robin",
        },
        {
          id: "e",
          name: "Load Balancer 4",
          protocol: "HTTP",
          port: 443,
          rule: "Round robin",
        },
        {
          id: "f",
          name: "Load Balancer 5",
          protocol: "HTTP",
          port: 80,
          rule: "DNS delegation",
        },
      ]}
      renderExpandedRow={(row) => <pre>{JSON.stringify(row, null, 2)}</pre>}
    >
      <Toolbar>
        <ToolbarContent>
          <p>{expandedRowIds().join(", ")}</p>
        </ToolbarContent>
      </Toolbar>
    </DataTable>
  );
};

export const ManyRows = () => {
  const rows = Array.from({ length: 500 }, (_, id) => ({
    id,
    price: Math.random() * 100,
  }));

  const [selectedRowIds, setSelectedRowIds] = createSignal([]);

  return (
    <DataTable
      headers={[
        { key: "id", value: "OrderID" },
        { key: "price", value: "Price" },
      ]}
      sortable
      batchSelection
      selectable
      selectedRowIds={selectedRowIds()}
      setSelectedRowIds={setSelectedRowIds}
      zebra
      rows={rows}
    />
  );
};

export const ManyRowsOptDataTable = () => {
  const rows = Array.from({ length: 500 }, (_, id) => ({
    id,
    price: Math.random() * 100,
  }));

  const headers = [
    { key: "id", value: "OrderID" },
    { key: "price", value: "Price" },
  ];

  const model = OptDataTableModel({ rows, headers });

  const selectedIds = createMemo(() =>
    model[0].rows
      .filter((row) => row.selected)
      .sort((a, b) => a.selected - b.selected)
      .map((row) => row.data.id)
  );

  return (
    <>
      <p>Selected ids: {selectedIds().join(", ")}</p>
      <OptDataTable sortable batchSelection selectable zebra model={model} />
    </>
  );
};
