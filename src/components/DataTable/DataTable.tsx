import { extractProps, cx } from "../../utils";
import { TableContainer } from "./TableContainer";
import { Table } from "./Table";
import { TableHeader } from "./TableHeader";
import { TableHead } from "./TableHead";
import { TableBody } from "./TableBody";
import { TableRow } from "./TableRow";
import { TableCell } from "./TableCell";
import { InlineCheckbox } from "../Checkbox/InlineCheckbox";
import { Show, For, createSignal, createMemo, createState } from "solid-js";
import ChevronRight16 from "carbon-icons-solid/lib/ChevronRight16";

const dataTableDefaultProps = {
  headers: [],
  rows: [],
  size: undefined,
  title: "",
  description: "",
  zebra: false,
  sortable: false,
  expandable: false,
  batchExpansion: false,
  expandedRowIds: [],
  radio: false,
  selectable: false,
  batchSelection: false,
  selectedRowIds: [],
  setSelectedRowIds: undefined,
  setExpandedRowIds: undefined,
  stickyHeader: false,
  children: undefined,

  renderCellHeader: (header) => header.value,
  renderCell: undefined, // Fn(row, cell) -> Element
  renderExpandedRow: undefined, // Fn (row)
};

export const DataTable = (allProps) => {
  const [props, restProps] = extractProps(allProps, dataTableDefaultProps);

  // XXX: Propagate up
  const dispatch = (data) => {
    console.log(data);
  };

  const headerKeys = createMemo(() => props.headers.map(({ key }) => key));

  const [sortHeader, setSortHeader] = createState({
    key: null,
    sort: undefined,
    sortDirection: "none",
  });

  const rows = createMemo(() =>
    props.rows.map((row) => ({
      ...row,
      cells: headerKeys().map((key) => ({ key, value: row[key] })),
    }))
  );

  const sortedRows = createMemo(() => {
    if (
      props.sortable &&
      sortHeader.key !== null &&
      sortHeader.sortDirection !== "none"
    ) {
      const ascending = sortHeader.sortDirection === "ascending";
      const sortKey = sortHeader.key;

      return rows()
        .slice(0)
        .sort((a, b) => {
          const itemA = ascending ? a[sortKey] : b[sortKey];
          const itemB = ascending ? b[sortKey] : a[sortKey];

          if (sortHeader.sort) return sortHeader.sort(itemA, itemB);

          if (typeof itemA === "number" && typeof itemB === "number")
            return itemA - itemB;

          return itemA
            .toString()
            .localeCompare(itemB.toString(), "en", { numeric: true });
        });
    } else {
      return rows();
    }
  });

  const allRowIds = createMemo(() => rows().map((row) => row.id));

  const [expanded, setExpanded] = createSignal(false);

  const expandable = createMemo(() => props.expandable || props.batchExpansion);

  const [parentRowId, setParentRowId] = createSignal(null);

  const setExpandedRowIds = (v) => {
    const setter = props.setExpandedRowIds;
    if (setter) {
      setter(v);
    }
  };

  const expandedRowIds = createMemo(() => props.expandedRowIds);
  const expandedRows = createMemo(() => new Set(expandedRowIds()));

  const selectable = createMemo(
    () => props.selectable || props.radio || props.batchSelection
  );

  const setSelectedRowIds = (newSelectedRowIds) => {
    const setter = props.setSelectedRowIds;
    if (setter) {
      setter(newSelectedRowIds);
    }
  };

  const toggleAllExpansions = (e) => {
    setExpanded(!expanded());
    setExpandedRowIds(expanded() ? allRowIds() : []);
    dispatch({ type: "click:header--expand", expanded: expanded() });
  };

  const toggleRowExpansion = (row) => {
    const rowExpanded = expandedRows().has(row.id);

    setExpandedRowIds(
      rowExpanded
        ? expandedRowIds().filter((id) => id !== row.id)
        : expandedRowIds().concat([row.id])
    );

    dispatch({ type: "click:row--expand", row, expanded: !rowExpanded });
  };

  const renderBatchExpansionButton = () => (
    <button
      type="button"
      class="bx--table-expand__button"
      onClick={[toggleAllExpansions, null]}
    >
      <ChevronRight16 class="bx--table-expand__svg" />
    </button>
  );

  const renderExpandableTableHead = () => (
    <th
      scope="col"
      class="bx--table-expand"
      data-previous-value={expanded() ? "collapsed" : undefined}
    >
      {props.batchExpansion && renderBatchExpansionButton()}
    </th>
  );

  const selectedRowIds = createMemo(() => new Set(props.selectedRowIds));

  const allSelected = createMemo(
    () => selectedRowIds().size == props.rows.length
  );
  const noneSelected = createMemo(() => selectedRowIds().size == 0);
  const indeterminate = createMemo(() => !allSelected() && !noneSelected());

  const onChangeSelectAllRows = () => {
    if (indeterminate() || allSelected()) {
      setSelectedRowIds([]);
    } else {
      setSelectedRowIds(allRowIds());
    }
  };

  // XXX
  var refSelectAll = undefined;

  const renderBatchSelectionHeaderCheckbox = () => (
    <th scope="col" class="bx--table-column-checkbox">
      <InlineCheckbox
        ref={refSelectAll}
        aria-label="Select all rows"
        checked={allSelected() ? true : undefined}
        indeterminate={indeterminate() ? true : undefined}
        onInput={onChangeSelectAllRows}
      />
    </th>
  );

  const nextSortDirection = {
    none: "ascending",
    ascending: "descending",
    descending: "none",
  };

  const onClickHeader = (header) => {
    const active = header.key === sortHeader.key;
    const currentSortDirection = active ? sortHeader.sortDirection : "none";
    const sortDirection = nextSortDirection[currentSortDirection];
    setSortHeader({
      //id: sortDirection === 'none' ? null : props.headers.find((h) => h.key === header.key).id, //thKeys()[header.key],
      key: header.key,
      sort: header.sort,
      sortDirection,
    });
    dispatch({ type: "click:header", header, sortDirection });
  };

  const renderHeader = (header) => {
    if (header.empty) {
      return <th scope="col"></th>;
    } else {
      return (
        <TableHeader
          onClick={() => onClickHeader(header)}
          tableSortable={props.sortable}
          sortHeader={sortHeader}
          key={header.key}
        >
          {props.renderCellHeader(header)}
        </TableHeader>
      );
    }
  };

  // XXX
  const tableHead = (
    <TableHead>
      <TableRow>
        <Show when={expandable()}>{renderExpandableTableHead()}</Show>
        <Show when={selectable() && !props.batchSelection}>
          <th scope="col"></th>
        </Show>
        <Show when={props.batchSelection && !props.radio}>
          {renderBatchSelectionHeaderCheckbox()}
        </Show>
        <For each={props.headers}>{renderHeader}</For>
      </TableRow>
    </TableHead>
  );

  const tableRowClasses = (row) =>
    cx(
      selectedRowIds().has(row.id) && "bx--data-table--selected",
      expandedRows().has(row.id) && "bx--expandable-row",
      expandable() && "bx--parent-row",
      expandable() && parentRowId() === row.id && "bx--expandable-row--hover"
    );

  const renderExpandable = (row) => (
    <TableCell
      class="bx--table-expand"
      headers="expand"
      data-previous-value={expandedRows().has(row.id) ? "collapsed" : undefined}
    >
      <button
        type="button"
        class="bx--table-expand__button"
        aria-label={
          expandedRows().has(row.id)
            ? "Collapse current row"
            : "Expand current row"
        }
        onClick={[toggleRowExpansion, row]}
      >
        <ChevronRight16 class="bx--table-expand__svg" />
      </button>
    </TableCell>
  );

  const onToggleSelect = (rowId) => {
    setSelectedRowIds(
      selectedRowIds().has(rowId)
        ? props.selectedRowIds.filter((id) => id !== rowId)
        : props.selectedRowIds.concat([rowId])
    );
  };

  const renderSelectable = (row) => (
    <td
      classList={{
        "bx--table-column-checkbox": true,
        "bx--table-column-radio": props.radio,
      }}
    >
      {/* if radio => RadioButton */}
      <InlineCheckbox
        name={`select-row-${row.id}`}
        checked={selectedRowIds().has(row.id)}
        onInput={[onToggleSelect, row.id]}
      />
    </td>
  );

  const renderCell = (row, cell, columnIndex) => {
    const header = props.headers[columnIndex()];
    const cellBody = props.renderCell
      ? props.renderCell(row, cell)
      : header.display
      ? header.display(cell.value)
      : cell.value;

    if (header.empty) {
      return (
        <td classList={{ "bx--table-column-menu": header.columnMenu }}>
          {cellBody}
        </td>
      );
    } else {
      return (
        <TableCell onClick={[dispatch, { type: "click:cell", row, cell }]}>
          {cellBody}
        </TableCell>
      );
    }
  };

  const renderRow = (row) => (
    <TableRow
      id={`row-${row.id}`}
      class={tableRowClasses(row)}
      onClick={[dispatch, { type: "click:row", row }]}
      onMouseEnter={[dispatch, { type: "mouseenter:row", row }]}
      onMouseLeave={[dispatch, { type: "mouseleave:row", row }]}
    >
      <Show when={expandable()}>{renderExpandable(row)}</Show>
      <Show when={selectable()}>{renderSelectable(row)}</Show>

      <For each={row.cells}>
        {(cell, columnIndex) => renderCell(row, cell, columnIndex)}
      </For>
    </TableRow>
  );

  const renderExpandableRow = (row) => (
    <tr
      data-child-row
      class="bx--expandable-row"
      onMouseEnter={() => setParentRowId(row.id)}
      onMouseLeave={() => setParentRowId(null)}
    >
      <TableCell
        colspan={
          props.headers.length +
          +(expandable() ? 1 : 0) +
          (selectable() ? 1 : 0)
        }
      >
        <div class="bx--child-row-inner-container">
          {props.renderExpandedRow && props.renderExpandedRow(row)}
        </div>
      </TableCell>
    </tr>
  );

  const tableBody = (
    <TableBody>
      <For each={sortedRows()} fallback={<div>No data</div>}>
        {(row) => (
          <>
            {renderRow(row)}
            <Show when={expandable() && expandedRows().has(row.id)}>
              {renderExpandableRow(row)}
            </Show>
          </>
        )}
      </For>
    </TableBody>
  );

  const table = () => (
    <Table
      zebra={props.zebra}
      size={props.size}
      stickyHeader={props.stickyHeader}
      sortable={props.sortable}
    >
      {tableHead}
      {tableBody}
    </Table>
  );

  const tableContainer = (
    <TableContainer
      title={props.title}
      description={props.description}
      {...restProps}
    >
      {props.children}
      {table()}
    </TableContainer>
  );

  return tableContainer;
};
