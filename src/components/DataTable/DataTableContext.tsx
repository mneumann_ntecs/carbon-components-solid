import { createContext, createState } from "solid-js";

export const DataTableContext = createContext(undefined);

export function DataTableProvider(props) {
  const [state, setState] = createState({
      sortHeader: props.sortHeader,
      tableSortable: props.tableSortable,
    }),
    store = [
      state,
      {
        add() {
          setState("blah", (c) => c + 1);
          //
        },
      },
    ];

  return (
    <DataTableContext.Provider value={store}>
      {props.children}
    </DataTableContext.Provider>
  );
}
