import { extractProps } from "../../utils";
import { TableContainer } from "./TableContainer";
import { Table } from "./Table";
import { TableHeader } from "./TableHeader";
import { TableHead } from "./TableHead";
import { TableBody } from "./TableBody";
import { TableRow } from "./TableRow";
import { TableCell } from "./TableCell";
import { InlineCheckbox } from "../Checkbox/InlineCheckbox";
import { createMemo, createState, Show, For } from "solid-js";
import ChevronRight16 from "carbon-icons-solid/lib/ChevronRight16";
import { cx } from "../../utils";

export function OptDataTableModel({
  headers,
  rows,
  initiallyExpanded,
  initiallySelected,
}) {
  return createState({
    headers,
    rows: rows.map((data, idx) => ({
      data,
      selected: initiallySelected && initiallySelected.includes(idx) ? 1 : 0,
      expanded: initiallyExpanded && initiallyExpanded.includes(idx),
      expandedHover: false,
    })),
    ui: {
      expanded: false,
    },
    sort: {
      key: null,
      sortFn: undefined,
      sortDirection: "none",
    },
  });
}

const dataTableDefaultProps = {
  model: undefined,

  size: undefined,
  title: "",
  description: "",
  zebra: false,
  sortable: false,
  expandable: false,
  batchExpansion: false,
  radio: false,
  selectable: false,
  batchSelection: false,

  stickyHeader: false,
  children: undefined,

  renderCellHeader: (header) => header.value,
  renderCell: undefined, // Fn(row, cell) -> Element
  renderExpandedRow: undefined, // Fn (row)
};

const nextSortDirection = {
  none: "ascending",
  ascending: "descending",
  descending: "none",
};

export const OptDataTable = (allProps) => {
  const [props, restProps] = extractProps(allProps, dataTableDefaultProps);

  const [state, setState] = props.model;

  // XXX: Propagate up
  const dispatch = (data) => {
    console.log(data);
  };

  var selectCounter = 1;

  const sortedRowIndices = createMemo(() => {
    const sort = state.sort;
    const indices = state.rows.map((_, i) => i);

    if (props.sortable && sort.key !== null && sort.sortDirection !== "none") {
      const ascending = sort.sortDirection === "ascending";
      const sortKey = sort.key;

      const rows = state.rows;

      indices.sort((j, k) => {
        const itemA = rows[ascending ? j : k].data[sortKey];
        const itemB = rows[ascending ? k : j].data[sortKey];

        if (sort.sortFn) return sort.sortFn(itemA, itemB);

        if (typeof itemA === "number" && typeof itemB === "number")
          return itemA - itemB;

        return itemA
          .toString()
          .localeCompare(itemB.toString(), "en", { numeric: true });
      });
    }

    return indices;
  });

  const expandable = createMemo(() => props.expandable || props.batchExpansion);
  const selectable = createMemo(
    () => props.selectable || props.radio || props.batchSelection
  );

  //
  // Actions
  //

  const toggleAllExpansions = (e) => {
    setState("ui", "expanded", (old) => !old);
    setState("rows", {}, "expanded", state.ui.expanded);
    dispatch({ type: "click:header--expand", expanded: state.ui.expanded });
  };

  const toggleRowExpansion = (rowIndex) => {
    setState("rows", rowIndex, "expanded", (old) => !old);

    const row = state.rows[rowIndex];

    dispatch({
      type: "click:row--expand",
      row,
      expanded: row.expanded,
    });
  };

  const onClickHeader = (header) => {
    const active = header.key === state.sort.key;
    const currentSortDirection = active ? state.sort.sortDirection : "none";
    const sortDirection = nextSortDirection[currentSortDirection];
    setState("sort", {
      key: header.key,
      sortFn: header.sort,
      sortDirection,
    });
    dispatch({ type: "click:header", header, sortDirection });
  };

  const countSelected = createMemo(() =>
    state.rows.reduce((acc, row) => (row.selected ? acc + 1 : acc), 0)
  );

  const allSelected = createMemo(() => countSelected() == state.rows.length);
  const noneSelected = createMemo(() => countSelected() == 0);
  const indeterminate = createMemo(() => !allSelected() && !noneSelected());

  const toggleBatchSelection = () => {
    const newSelected = indeterminate() || allSelected() ? 0 : ++selectCounter;
    setState("rows", {}, "selected", newSelected);
  };

  const toggleSelection = (rowIndex) => {
    setState("rows", rowIndex, "selected", (old) =>
      old ? 0 : ++selectCounter
    );
  };

  const updateExpandHoverEnter = (rowIndex) =>
    setState("rows", rowIndex, "expandedHover", true);

  const updateExpandHoverLeave = (rowIndex) =>
    setState("rows", rowIndex, "expandedHover", false);

  //
  // Render functions
  //

  const renderExpandableTableHead = () => (
    <th
      scope="col"
      class="bx--table-expand"
      data-previous-value={state.ui.expanded ? "collapsed" : undefined}
    >
      <Show when={props.batchExpansion}>
        <button
          type="button"
          class="bx--table-expand__button"
          onClick={[toggleAllExpansions, null]}
        >
          <ChevronRight16 class="bx--table-expand__svg" />
        </button>
      </Show>
    </th>
  );

  const renderBatchSelectionHeaderCheckbox = () => (
    <th scope="col" class="bx--table-column-checkbox">
      <InlineCheckbox
        aria-label="Select all rows"
        checked={allSelected() ? true : undefined}
        indeterminate={indeterminate() ? true : undefined}
        onInput={toggleBatchSelection}
      />
    </th>
  );

  const renderHeader = (header) => {
    if (header.empty) {
      return <th scope="col"></th>;
    } else {
      return (
        <TableHeader
          onClick={() => onClickHeader(header)}
          tableSortable={props.sortable}
          sortHeader={state.sort}
          key={header.key}
        >
          {props.renderCellHeader(header)}
        </TableHeader>
      );
    }
  };

  // XXX
  const tableHead = (
    <TableHead>
      <TableRow>
        <Show when={expandable()}>{renderExpandableTableHead()}</Show>
        <Show when={selectable() && !props.batchSelection}>
          <th scope="col"></th>
        </Show>
        <Show when={props.batchSelection && !props.radio}>
          {renderBatchSelectionHeaderCheckbox()}
        </Show>
        <For each={state.headers}>{renderHeader}</For>
      </TableRow>
    </TableHead>
  );

  const tableRowClasses = (row) =>
    cx(
      !!row.selected && "bx--data-table--selected",
      row.expanded === true && "bx--expandable-row",
      expandable() && "bx--parent-row",
      expandable() && row.expandedHover === true && "bx--expandable-row--hover"
    );

  const renderExpandable = (row, rowIndex) => (
    <TableCell
      class="bx--table-expand"
      headers="expand"
      data-previous-value={row.expanded ? "collapsed" : undefined}
    >
      <button
        type="button"
        class="bx--table-expand__button"
        aria-label={
          row.expanded ? "Collapse current row" : "Expand current row"
        }
        onClick={[toggleRowExpansion, rowIndex]}
      >
        <ChevronRight16 class="bx--table-expand__svg" />
      </button>
    </TableCell>
  );

  const renderSelectable = (row, rowIndex) => (
    <td
      classList={{
        "bx--table-column-checkbox": true,
        "bx--table-column-radio": props.radio,
      }}
    >
      {/* if radio => RadioButton */}
      <InlineCheckbox
        name={`select-row-${row.data.id}`}
        checked={!!row.selected}
        onInput={[toggleSelection, rowIndex]}
      />
    </td>
  );

  // XXX
  const renderCell = (row, header) => {
    const cell = { key: header.key, value: row.data[header.key] };

    const cellBody = props.renderCell
      ? props.renderCell(row.data, cell)
      : header.display
      ? header.display(cell.value)
      : cell.value;

    if (header.empty) {
      return (
        <td classList={{ "bx--table-column-menu": header.columnMenu }}>
          {cellBody}
        </td>
      );
    } else {
      return (
        <TableCell onClick={[dispatch, { type: "click:cell", row, cell }]}>
          {cellBody}
        </TableCell>
      );
    }
  };

  const renderExpandableRow = (row, rowIndex) => (
    <tr
      data-child-row
      class="bx--expandable-row"
      onMouseEnter={[updateExpandHoverEnter, rowIndex]}
      onMouseLeave={[updateExpandHoverLeave, rowIndex]}
    >
      <TableCell
        colspan={
          state.headers.length + (expandable() ? 1 : 0) + (selectable() ? 1 : 0)
        }
      >
        <div class="bx--child-row-inner-container">
          <Show when={props.renderExpandedRow}>
            {(render) => render(row.data)}
          </Show>
        </div>
      </TableCell>
    </tr>
  );

  const renderRow = (row, rowIndex) => (
    <>
      <TableRow
        id={`row-${row.data.id}`}
        class={tableRowClasses(row)}
        onClick={[dispatch, { type: "click:row", row }]}
        onMouseEnter={[dispatch, { type: "mouseenter:row", row }]}
        onMouseLeave={[dispatch, { type: "mouseleave:row", row }]}
      >
        <Show when={expandable()}>{renderExpandable(row, rowIndex)}</Show>
        <Show when={selectable()}>{renderSelectable(row, rowIndex)}</Show>

        <For each={state.headers}>{(header) => renderCell(row, header)}</For>
      </TableRow>
      <Show when={expandable() && row.expanded}>
        {renderExpandableRow(row, rowIndex)}
      </Show>
    </>
  );

  const tableBody = (
    <TableBody>
      <For each={sortedRowIndices()} fallback={<div>No data</div>}>
        {(rowIndex) => renderRow(state.rows[rowIndex], rowIndex)}
      </For>
    </TableBody>
  );

  const table = (
    <Table
      zebra={props.zebra}
      size={props.size}
      stickyHeader={props.stickyHeader}
      sortable={props.sortable}
    >
      {tableHead}
      {tableBody}
    </Table>
  );

  const tableContainer = (
    <TableContainer
      title={props.title}
      description={props.description}
      {...restProps}
    >
      {props.children}
      {table}
    </TableContainer>
  );

  return tableContainer;
};
