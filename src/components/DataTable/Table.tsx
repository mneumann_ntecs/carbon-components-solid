import { extractProps, cx } from "../../utils";

const tableClasses = (props) =>
  cx(
    "bx--data-table",
    props.size === "compact" && "bx--data-table--compact",
    props.size === "short" && "bx--data-table--short",
    props.size === "tall" && "bx--data-table--tall",
    props.sortable && "bx--data-table--sort",
    props.zebra && "bx--data-table--zebra",
    props.useStaticWidth && "bx--data-table--static",
    !props.shouldShowBorder && "bx--data-table--no-border",
    props.stickyHeader && "bx--data-table--sticky-header",
    props.class
  );

const tableDefaultProps = {
  size: undefined,
  zebra: false,
  useStaticWidth: false,
  shouldShowBorder: false,
  sortable: false,
  stickyHeader: false,
  children: undefined,
  class: undefined,
};

export const Table = (allProps) => {
  const [props, restProps] = extractProps(allProps, tableDefaultProps);
  return () =>
    props.stickyHeader ? (
      <section class="bx--data-table_inner-container" {...restProps}>
        <table class={tableClasses(props)}>{props.children}</table>
      </section>
    ) : (
      <table class={tableClasses(props)} {...restProps}>
        {props.children}
      </table>
    );
};
