import { extractProps, cx } from "../../utils";

const tableContainerDefaultProps = {
  title: "",
  description: "",
  stickyHeader: false,
  children: undefined,
  class: undefined,
};

export const TableContainer = (allProps) => {
  const [props, restProps] = extractProps(allProps, tableContainerDefaultProps);
  return (
    <div
      class={cx(
        "bx--data-table-container",
        props.stickyHeader && "bx--data-table--max-width",
        props.class
      )}
      {...restProps}
    >
      {props.title ? (
        <div class="bx--data-table-header">
          <h4 class="bx--data-table-header__title">{props.title}</h4>
          <p class="bx--data-table-header__description">{props.description}</p>
        </div>
      ) : undefined}
      {props.children}
    </div>
  );
};
