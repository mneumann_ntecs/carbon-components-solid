import ArrowUp20 from "carbon-icons-solid/lib/ArrowUp20";
import ArrowsVertical20 from "carbon-icons-solid/lib/ArrowsVertical20";
import { extractProps, uniqueId } from "../../utils";
import { createMemo } from "solid-js";

const tableHeaderDefaultProps = {
  scope: "col",
  translateWithId: () => "",
  onClick: undefined,
  id: undefined,
  key: undefined,
  children: undefined,
  tableSortable: undefined, // XXX: required
  sortHeader: {}, // XXX: required
};

export const TableHeader = (allProps) => {
  const [props, restProps] = extractProps(allProps, tableHeaderDefaultProps);

  const id = createMemo(() => props.id || uniqueId());
  const active = createMemo(
    () =>
      props.sortHeader.key === props.key &&
      props.sortHeader.sortDirection !== "none"
  );
  const ariaLabel = createMemo(() => props.translateWithId()); // TODO

  const renderTableSortable = () => (
    <th
      aria-sort={active() ? props.sortHeader.sortDirection : "none"}
      scope={props.scope}
      id={id()}
      {...restProps}
    >
      <button
        classList={{
          "bx--table-sort": true,
          "bx--table-sort--active": active(),
          "bx--table-sort--ascending":
            active() && props.sortHeader.sortDirection === "ascending",
        }}
        onClick={props.onClick}
      >
        <div class="bx--table-header-label">{props.children}</div>
        <ArrowUp20 aria-label={ariaLabel()} class="bx--table-sort__icon" />
        <ArrowsVertical20
          aria-label={ariaLabel()}
          class="bx--table-sort__icon-unsorted"
        />
      </button>
    </th>
  );

  const renderTableNotSortable = () => (
    <th scope={props.scope} id={id()} {...restProps}>
      <div class="bx--table-header-label">{props.children}</div>
    </th>
  );

  return () =>
    props.tableSortable ? renderTableSortable() : renderTableNotSortable();
};
