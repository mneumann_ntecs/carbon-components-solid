import { extractProps, cx } from "../../utils";

const toolbarDefaultProps = {
  size: "default",
  class: undefined,
};

export const Toolbar = (allProps) => {
  const [props, restProps] = extractProps(allProps, toolbarDefaultProps);
  // XXX: Setup context

  return () => (
    <section
      aria-label="data table toolbar"
      class={cx(
        "bx--table-toolbar",
        props.size === "sm" && "bx--table-toolbar--small",
        props.size === "default" && "bx--table-toolbar--normal",
        props.class
      )}
      {...restProps}
    />
  );
};
