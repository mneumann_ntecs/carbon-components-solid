export const ToolbarContent = (props) => (
  <div class="bx--toolbar-content">{props.children}</div>
);
