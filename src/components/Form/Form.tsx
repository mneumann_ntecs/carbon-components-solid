import { splitProps } from "solid-js";
import { preventDefault } from "../../events";
import { cx } from "../../utils";

export const Form = (props) => {
  const [local, restProps] = splitProps(props, ["onSubmit", "class"]);
  return (
    <form
      onSubmit={[preventDefault, local.onSubmit]}
      class={cx("bx--form", local.class)}
      {...restProps}
    />
  );
};
