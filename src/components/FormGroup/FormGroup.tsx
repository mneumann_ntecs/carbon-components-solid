import { splitProps } from "solid-js";
import { cx } from "../../utils";

export const FormGroup = (allProps) => {
  const [props, restProps] = splitProps(allProps, [
    "invalid",
    "children",
    "message",
    "legendText",
    "messageText",
    "class",
  ]);

  return (
    <fieldset
      data-invalid={props.invalid || undefined}
      class={cx("bx--fieldset", props.class)}
      {...restProps}
    >
      <legend class="bx--label">{props.legendText}</legend>
      {props.children}
      {props.message && (
        <div class="bx--form-requirement">{props.messageText}</div>
      )}
    </fieldset>
  );
};
