import { splitProps } from "solid-js";
import { cx } from "../../utils";

export const FormItem = (props) => {
  const [local, restProps] = splitProps(props, ["class"]);
  return <div class={cx("bx--form-item", local.class)} {...restProps} />;
};
