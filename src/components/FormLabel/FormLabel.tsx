import { splitProps } from "solid-js";
import { uniqueId, cx } from "../../utils";

export const FormLabel = (props) => {
  const [local, restProps] = splitProps(props, ["id", "class"]);

  return (
    <label
      class={cx("bx--label", local.class)}
      for={local.id || uniqueId()}
      {...restProps}
    />
  );
};
