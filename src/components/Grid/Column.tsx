import { Component, createMemo, splitProps } from "solid-js";
import { Dynamic } from "solid-js/web";
import { cx } from "../../utils";

type AspectRatio = "2x1" | "16x9" | "9x16" | "1x2" | "4x3" | "3x4" | "1x1";
type ColumnBreakpoint =
  | null
  | boolean
  | number
  | { span: number; offset: number };
type BreakpointName = "sm" | "md" | "lg" | "xlg" | "max";

type ColumnProps = {
  /**
   * Provide a custom element to render instead of the default <div>.
   *
   * Default: 'div'
   */
  as?: string;

  /**
   * Specify a custom CSS class to be applied to the `Column`.
   *
   * Default: undefined
   */
  class?: string;

  /**
   * Specify column span for the `sm` breakpoint (Default breakpoint up to 672px)
   * This breakpoint supports 4 columns by default.
   *
   * @see https://www.carbondesignsystem.com/guidelines/layout#breakpoints
   */
  sm?: ColumnBreakpoint;

  /**
   * Specify column span for the `md` breakpoint (Default breakpoint up to 1056px)
   * This breakpoint supports 8 columns by default.
   *
   * @see https://www.carbondesignsystem.com/guidelines/layout#breakpoints
   */
  md?: ColumnBreakpoint;

  /**
   * Specify column span for the `lg` breakpoint (Default breakpoint up to 1312px)
   * This breakpoint supports 16 columns by default.
   *
   * @see https://www.carbondesignsystem.com/guidelines/layout#breakpoints
   */
  lg?: ColumnBreakpoint;

  /**
   * Specify column span for the `xlg` breakpoint (Default breakpoint up to
   * 1584px) This breakpoint supports 16 columns by default.
   *
   * @see https://www.carbondesignsystem.com/guidelines/layout#breakpoints
   */
  xlg?: ColumnBreakpoint;

  /**
   * Specify column span for the `max` breakpoint. This breakpoint supports 16
   * columns by default.
   *
   * @see https://www.carbondesignsystem.com/guidelines/layout#breakpoints
   */
  max?: ColumnBreakpoint;

  /**
   * Specify the aspect ratio of the column
   *
   * Default: undefined
   */
  aspectRatio?: AspectRatio;

  /**
   * Set to `true` to remove the gutter.
   *
   * Default: false
   */
  noGutter?: boolean;

  /**
   * Set to `true` to remove the left gutter.
   *
   * Default: false
   */
  noGutterLeft?: boolean;

  /**
   * Set to `true` to remove the right gutter.
   *
   * Default: false
   */
  noGutterRight?: boolean;

  /**
   * Set to `true` to add top and bottom padding to all columns.
   *
   * Default: false
   */
  padding?: boolean;
};

export const Column: Component<ColumnProps> = (props) => {
  const [local, restProps] = splitProps(props, [
    "as",
    "class",
    "sm",
    "md",
    "lg",
    "xlg",
    "max",
    "aspectRatio",
    "noGutter",
    "noGutterLeft",
    "noGutterRight",
    "padding",
    "children",
  ]);

  const cssClasses = createMemo(() => {
    const columnClass = cx(
      columnSpanToClass(local.sm, "sm"),
      columnSpanToClass(local.md, "md"),
      columnSpanToClass(local.lg, "lg"),
      columnSpanToClass(local.xlg, "xlg"),
      columnSpanToClass(props.max, "max")
    );

    return cx(
      local.class,
      columnClass,
      columnClass === "" ? "bx--col" : null,
      local.noGutter && "bx--no-gutter",
      local.noGutterLeft && "bx--no-gutter--left",
      local.noGutterRight && "bx--no-gutter--right",
      local.aspectRatio &&
        `bx--aspect-ratio bx--aspect-ratio--${local.aspectRatio}`,
      local.padding && "bx--col-padding"
    );
  });

  return (
    <Dynamic component={local.as || "div"} class={cssClasses()} {...restProps}>
      {local.children}
    </Dynamic>
  );
};

function columnSpanToClass(
  breakpoint: ColumnBreakpoint | undefined,
  name: BreakpointName
): string | null {
  if (breakpoint === true) {
    // If our breakpoint is a boolean, the user has specified that the column
    // should be "auto" at this size
    return `bx--col-${name}`;
  } else if (typeof breakpoint === "number") {
    // If our breakpoint is a number, the user has specified the number of
    // columns they'd like this column to span
    return `bx--col-${name}-${breakpoint}`;
  } else if (breakpoint !== null && typeof breakpoint === "object") {
    const { span, offset } = breakpoint;
    return cx(
      typeof span === "number"
        ? `bx--col-${name}-${span}`
        : span === true
        ? `bx--col-${name}`
        : null,
      typeof offset === "number" ? `bx--offset-${name}-${offset}` : null
    );
  } else {
    return null;
  }
}
