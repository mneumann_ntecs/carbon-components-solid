export default {
  title: "Grid",
};

import { Grid } from "./Grid";
import { Row } from "./Row";
import { Column } from "./Column";

export const Default = () => (
  <Grid>
    <Row>
      <Column style="outline: 1px solid var(--cds-interactive-04)">
        Column
      </Column>
      <Column style="outline: 1px solid var(--cds-interactive-04)">
        Column
      </Column>
      <Column style="outline: 1px solid var(--cds-interactive-04)">
        Column
      </Column>
      <Column style="outline: 1px solid var(--cds-interactive-04)">
        Column
      </Column>
    </Row>
  </Grid>
);

export const FullWidth = () => (
  <Grid fullWidth>
    <Row>
      <Column style="outline: 1px solid var(--cds-interactive-04)">
        Column
      </Column>
      <Column style="outline: 1px solid var(--cds-interactive-04)">
        Column
      </Column>
      <Column style="outline: 1px solid var(--cds-interactive-04)">
        Column
      </Column>
      <Column style="outline: 1px solid var(--cds-interactive-04)">
        Column
      </Column>
    </Row>
  </Grid>
);

export const Narrow = () => (
  <Grid narrow>
    <Row>
      <Column style="outline: 1px solid var(--cds-interactive-04)">
        Column
      </Column>
      <Column style="outline: 1px solid var(--cds-interactive-04)">
        Column
      </Column>
      <Column style="outline: 1px solid var(--cds-interactive-04)">
        Column
      </Column>
      <Column style="outline: 1px solid var(--cds-interactive-04)">
        Column
      </Column>
    </Row>
  </Grid>
);

export const Condensed = () => (
  <Grid condensed>
    <Row>
      <Column style="outline: 1px solid var(--cds-interactive-04)">
        Column
      </Column>
      <Column style="outline: 1px solid var(--cds-interactive-04)">
        Column
      </Column>
      <Column style="outline: 1px solid var(--cds-interactive-04)">
        Column
      </Column>
      <Column style="outline: 1px solid var(--cds-interactive-04)">
        Column
      </Column>
    </Row>
  </Grid>
);

export const Responsive = () => (
  <Grid>
    <Row>
      <Column
        sm={1}
        md={4}
        lg={8}
        style="outline: 1px solid var(--cds-interactive-04)"
      >
        sm: 1, md: 4, lg: 8
      </Column>
      <Column
        sm={1}
        md={2}
        lg={2}
        style="outline: 1px solid var(--cds-interactive-04)"
      >
        sm: 1, md: 2, lg: 2
      </Column>
      <Column
        sm={1}
        md={1}
        lg={1}
        style="outline: 1px solid var(--cds-interactive-04)"
      >
        sm: 1, md: 1, lg: 1
      </Column>
      <Column
        sm={1}
        md={1}
        lg={1}
        style="outline: 1px solid var(--cds-interactive-04)"
      >
        sm: 1, md: 1, lg: 1
      </Column>
    </Row>
  </Grid>
);

export const OffsetColumns = () => (
  <Grid>
    <Row>
      <Column
        sm={{ span: 1, offset: 3 }}
        style="outline: 1px solid var(--cds-interactive-04)"
      >
        Offset 3
      </Column>
      <Column
        sm={{ span: 2, offset: 2 }}
        style="outline: 1px solid var(--cds-interactive-04)"
      >
        Offset 2
      </Column>
      <Column
        sm={{ span: 3, offset: 1 }}
        style="outline: 1px solid var(--cds-interactive-04)"
      >
        Offset 1
      </Column>
      <Column
        sm={{ span: 4, offset: 0 }}
        style="outline: 1px solid var(--cds-interactive-04)"
      >
        Offset 0
      </Column>
    </Row>
  </Grid>
);

export const AspectRatioColumns = () => (
  <Grid>
    <Row>
      <Column
        aspectRatio="2x1"
        style="outline: 1px solid var(--cds-interactive-04)"
      >
        2x1
      </Column>
      <Column
        aspectRatio="2x1"
        style="outline: 1px solid var(--cds-interactive-04)"
      >
        2x1
      </Column>
    </Row>
  </Grid>
);

export const PaddedColumns = () => (
  <>
    <Grid narrow padding>
      <Row>
        <Column style="outline: 1px solid var(--cds-interactive-04)">
          Column
        </Column>
        <Column style="outline: 1px solid var(--cds-interactive-04)">
          Column
        </Column>
        <Column style="outline: 1px solid var(--cds-interactive-04)">
          Column
        </Column>
        <Column style="outline: 1px solid var(--cds-interactive-04)">
          Column
        </Column>
      </Row>
    </Grid>
    <Grid narrow>
      <Row padding>
        <Column style="outline: 1px solid var(--cds-interactive-04)">
          Column
        </Column>
        <Column style="outline: 1px solid var(--cds-interactive-04)">
          Column
        </Column>
        <Column style="outline: 1px solid var(--cds-interactive-04)">
          Column
        </Column>
        <Column style="outline: 1px solid var(--cds-interactive-04)">
          Column
        </Column>
      </Row>
      <Row>
        <Column padding style="outline: 1px solid var(--cds-interactive-04)">
          Column
        </Column>
        <Column style="outline: 1px solid var(--cds-interactive-04)">
          Column
        </Column>
        <Column style="outline: 1px solid var(--cds-interactive-04)">
          Column
        </Column>
        <Column style="outline: 1px solid var(--cds-interactive-04)">
          Column
        </Column>
      </Row>
    </Grid>
  </>
);
