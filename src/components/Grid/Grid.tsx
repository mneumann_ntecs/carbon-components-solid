import { Component, splitProps, createMemo } from "solid-js";
import { Dynamic } from "solid-js/web";
import { cx } from "../../utils";

type GridProps = {
  /**
   * Provide a custom element to render instead of the default <div>.
   *
   * Default: 'div'
   */
  as?: string;

  /**
   * Specify a custom CSS class to be applied to the `Grid`.
   *
   * Default: undefined
   */
  class?: string;

  /**
   * Collapse the gutter to 1px. Useful for fluid layouts.
   * Rows have 1px of margin between them to match gutter.
   *
   * Default: false
   */
  condensed?: boolean;

  /**
   * Remove the default max width that the grid has set.
   *
   * Default: false
   */
  fullWidth?: boolean;

  /**
   * Container hangs 16px into the gutter. Useful for
   * typographic alignment with and without containers.
   *
   * Default: false
   */
  narrow?: boolean;

  /**
   * Set to `true` to remove the gutter.
   *
   * Default: false
   */
  noGutter?: boolean;

  /**
   * Set to `true` to remove the left gutter.
   *
   * Default: false
   */
  noGutterLeft?: boolean;

  /**
   * Set to `true` to remove the right gutter.
   *
   * Default: false
   */
  noGutterRight?: boolean;

  /**
   * Set to `true` to add top and bottom padding to all columns.
   *
   * Default: false
   */
  padding?: boolean;
};

export const Grid: Component<GridProps> = (props) => {
  const [local, restProps] = splitProps(props, [
    "as",
    "class",
    "condensed",
    "fullWidth",
    "narrow",
    "noGutter",
    "noGutterLeft",
    "noGutterRight",
    "padding",
    "children",
  ]);

  const cssClasses = createMemo(() =>
    cx(
      local.class,
      "bx--grid",
      local.condensed && "bx--grid--condensed",
      local.narrow && "bx--grid--narrow",
      local.fullWidth && "bx--grid--full-width",
      local.noGutter && "bx--no-gutter",
      local.noGutterLeft && "bx--no-gutter--left",
      local.noGutterRight && "bx--no-gutter--right",
      local.padding && "bx--row-padding"
    )
  );

  return (
    <Dynamic component={local.as || "div"} class={cssClasses()} {...restProps}>
      {local.children}
    </Dynamic>
  );
};
