import { Component, splitProps, createMemo } from "solid-js";
import { Dynamic } from "solid-js/web";
import { cx } from "../../utils";

type RowProps = {
  /**
   * Provide a custom element to render instead of the default <div>.
   *
   * Default: 'div'
   */
  as?: string;

  /**
   * Specify a custom CSS class to be applied to the `Row`.
   *
   * Default: undefined
   */
  class?: string;

  /**
   * Specify a single row as condensed.Rows that are adjacent
   * and are condensed will have 2px of margin between them to match gutter.
   *
   * Default: false
   */
  condensed?: boolean;

  /**
   * Specify a single row as narrow. The container will hang
   * 16px into the gutter.
   *
   * Default: false
   */
  narrow?: boolean;

  /**
   * Set to `true` to remove the gutter.
   *
   * Default: false
   */
  noGutter?: boolean;

  /**
   * Set to `true` to remove the left gutter.
   *
   * Default: false
   */
  noGutterLeft?: boolean;

  /**
   * Set to `true` to remove the right gutter.
   *
   * Default: false
   */
  noGutterRight?: boolean;

  /**
   * Set to `true` to add top and bottom padding to all columns.
   *
   * Default: false
   */
  padding?: boolean;
};

export const Row: Component<RowProps> = (props) => {
  const [local, restProps] = splitProps(props, [
    "as",
    "class",
    "condensed",
    "narrow",
    "noGutter",
    "noGutterLeft",
    "noGutterRight",
    "padding",
    "children",
  ]);

  const cssClasses = createMemo(() =>
    cx(
      local.class,
      "bx--row",
      local.condensed && "bx--row--condensed",
      local.narrow && "bx--row--narrow",
      local.noGutter && "bx--no-gutter",
      local.noGutterLeft && "bx--no-gutter--left",
      local.noGutterRight && "bx--no-gutter--right",
      local.padding && "bx--row-padding"
    )
  );

  return (
    <Dynamic component={local.as || "div"} class={cssClasses()} {...restProps}>
      {local.children}
    </Dynamic>
  );
};
