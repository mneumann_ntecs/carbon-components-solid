export default {
  title: "Icon",
};

import { Icon } from "./Icon";
import { Add16 } from "carbon-icons-solid/lib/Add16";
import { Add20 } from "carbon-icons-solid/lib/Add20";
import { Add24 } from "carbon-icons-solid/lib/Add24";
import { Add32 } from "carbon-icons-solid/lib/Add32";

export const Default = () => (
  <>
    <Icon render={Add16} />
    <Icon render={Add20} />
    <Icon render={Add24} />
    <Icon render={Add32} />
  </>
);

export const Skeleton = () => (
  <>
    <Icon skeleton render={Add16} />
    <Icon skeleton size={20} render={Add20} />
    <Icon skeleton size={24} render={Add24} />
    <Icon skeleton size={32} render={Add32} />
  </>
);
