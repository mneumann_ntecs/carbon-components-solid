import { JSX, splitProps } from "solid-js";
import { Dynamic } from "solid-js/web";
import {
  CarbonIconProps,
  CarbonIconComponent,
} from "carbon-icons-solid/lib/types";
import { IconSkeleton, IconSkeletonProps } from "./IconSkeleton";

type IconProps =
  | ({
      skeleton: true;
    } & IconSkeletonProps)
  | ({
      skeleton?: false | undefined;
      render?: CarbonIconComponent;
    } & CarbonIconProps);

type IconComponent = (props: IconProps) => JSX.Element;

export const Icon: IconComponent = (props) => {
  return () => {
    if (props.skeleton === true) {
      const [local, restProps] = splitProps(props, ["size", "skeleton"]);
      return <IconSkeleton size={local.size} {...restProps} />;
    } else {
      const [local, restProps] = splitProps(props, ["skeleton", "render"]);
      return <Dynamic component={local.render} {...restProps} />;
    }
  };
};
