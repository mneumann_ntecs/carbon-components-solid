import { JSX, splitProps } from "solid-js";
import { cx } from "../../utils";

export type IconSkeletonProps = {
  /** The size of the icon. Default: 16 */
  size?: number;
  class?: string;
} & JSX.HTMLAttributes<HTMLDivElement>;

type IconSkeletonComponent = (props: IconSkeletonProps) => JSX.Element;

export const IconSkeleton: IconSkeletonComponent = (props) => {
  const [local, restProps] = splitProps(props, ["size", "style", "class"]);
  return (
    <div
      class={cx("bx--icon--skeleton", local.class)}
      style={`${local.style}; width: ${local.size || 16}px; height: ${
        local.size || 16
      }px;`}
      {...restProps}
    ></div>
  );
};
