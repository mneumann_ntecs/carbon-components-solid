export default {
  title: "Inline Loading",
};

import { InlineLoading } from "./InlineLoading";
import { createSignal } from "solid-js";

export const Default = () => <InlineLoading />;

export const WithDescription = () => (
  <InlineLoading description="Loading metrics..." />
);

export const StatusStates = () => (
  <>
    <InlineLoading status="active" description="Submitting..." />
    <InlineLoading status="inactive" description="Cancelling..." />
    <InlineLoading status="finished" description="Success" />
    <InlineLoading status="error" description="An error occurred" />
  </>
);

export const OnSuccess = () => {
  const [fired, fire] = createSignal(false);
  return (
    <>
      <div>Fires after 3s: {fired() ? "fired" : "waiting..."}</div>
      <InlineLoading
        status="finished"
        description="Success"
        successDelay={3000}
        onSuccess={() => fire(true)}
      />
    </>
  );
};
