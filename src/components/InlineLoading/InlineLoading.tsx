import {
  mergeProps,
  createEffect,
  onCleanup,
  Switch,
  Match,
  JSX,
} from "solid-js";
import { ErrorFilled16 } from "carbon-icons-solid/lib/ErrorFilled16";
import { CheckmarkFilled16 } from "carbon-icons-solid/lib/CheckmarkFilled16";
import { Loading } from "../Loading/Loading";

type InlineLoadingProps = {
  /** Loading status. Default: "active" */
  status?: "inactive" | "active" | "finished" | "error";

  /** Loading description */
  description?: string;

  /** ARIA label for loading icon */
  iconDescription?: string;

  /**
   * Timeout delay (ms) after `status` is set to `"success"`.
   * Default: 1500.
   */
  successDelay?: number;

  style?: string;

  onSuccess?: () => void;
  onClick?: JSX.EventHandler<HTMLDivElement, FocusEvent>;
  onMouseOver?: JSX.EventHandler<HTMLDivElement, FocusEvent>;
  onMouseEnter?: JSX.EventHandler<HTMLDivElement, FocusEvent>;
  onMouseLeave?: JSX.EventHandler<HTMLDivElement, FocusEvent>;
};

type InlineLoadingComponent = (props: InlineLoadingProps) => JSX.Element;

const DefaultSuccessDelay = 1500;

export const InlineLoading: InlineLoadingComponent = (allProps) => {
  const props = mergeProps(
    {
      status: "active",
      successDelay: DefaultSuccessDelay,
      onSuccess: () => {},
    },
    allProps
  );

  let timerId: ReturnType<typeof setTimeout> | null = null;

  const clearTimer = () => {
    if (timerId) {
      clearTimeout(timerId);
    }
    timerId = null;
  };

  const setTimer = () => {
    timerId = setTimeout(props.onSuccess, props.successDelay);
  };

  const reloadTimer = () => {
    clearTimer();
    setTimer();
  };

  createEffect(() => {
    if (props.status === "finished") {
      reloadTimer();
    }
  });

  onCleanup(clearTimer);

  return (
    <div
      class="bx--inline-loading"
      style={props.style}
      aria-live="assertive"
      onClick={props.onClick}
      onMouseOver={props.onMouseOver}
      onMouseEnter={props.onMouseEnter}
      onMouseLeave={props.onMouseLeave}
    >
      {loadingAnimation(props)}
      {props.description && (
        <div class="bx--inline-loading__text">{props.description}</div>
      )}
    </div>
  );
};

const loadingAnimation = (props: InlineLoadingProps) => (
  <div class="bx--inline-loading__animation">
    <Switch>
      <Match when={props.status === "error"}>
        <ErrorFilled16 class="bx--inline-loading--error" />
      </Match>
      <Match when={props.status === "finished"}>
        <CheckmarkFilled16 class="bx--inline-loading__checkmark-container" />
      </Match>
      <Match when={props.status === "inactive" || props.status === "active"}>
        <Loading
          small
          description={props.iconDescription}
          withOverlay={false}
          active={props.status === "active"}
        />
      </Match>
    </Switch>
  </div>
);
