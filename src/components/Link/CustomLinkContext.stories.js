export default {
  title: "Custom Link Context",
};

import { Link } from "./Link";
import { CustomLinkContext } from "./CustomLinkContext";

const CustomLink = (props) => (
  <a
    {...props}
    onClick={(ev) => {
      alert("clicked");
      ev.preventDefault();
    }}
  >
    {props.children} (opens alert)
  </a>
);

export const Default = () => (
  <CustomLinkContext.Provider value={{ component: CustomLink }}>
    <p>
      <Link href="/">With CustomLinkContext</Link>
    </p>
    <CustomLinkContext.Provider value={{}}>
      <p>
        <Link href="/">Without CustomLinkContext</Link>
      </p>
    </CustomLinkContext.Provider>
  </CustomLinkContext.Provider>
);
