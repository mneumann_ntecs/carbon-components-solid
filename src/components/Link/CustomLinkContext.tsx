import { createContext, Component, JSX } from "solid-js";

type CustomLinkContextType = {
  component: Component<JSX.HTMLAttributes<HTMLAnchorElement>> | string | null;
};

export const CustomLinkContext = createContext<CustomLinkContextType>({
  component: null,
});
