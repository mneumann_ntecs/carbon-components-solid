import { Component, JSX, splitProps, createMemo } from "solid-js";
import { cx } from "../../utils";
import { UnstyledLink } from "./UnstyledLink";

type LinkProps = {
  disabled?: boolean;
  href?: string;
  inline?: boolean;
  size?: "sm" | "lg";
  visited?: boolean;
  target?: string;
  class?: string;
} & JSX.HTMLAttributes<HTMLAnchorElement> &
  JSX.HTMLAttributes<HTMLParagraphElement>;

export const Link: Component<LinkProps> = (props) => {
  const [, restProps] = splitProps(props, [
    "disabled",
    "href",
    "inline",
    "size",
    "visited",
    "children",
    "class",
  ]);

  const classes = createMemo(() =>
    cx(
      "bx--link",
      props.disabled && "bx--link--disabled",
      props.inline && "bx--link--inline",
      props.visited && "bx--link--visited",
      props.size === "sm" && "bx--link--sm",
      props.size === "lg" && "bx--link--lg",
      props.class
    )
  );

  return () =>
    props.disabled ? (
      <p class={classes()} {...restProps}>
        {props.children}
      </p>
    ) : (
      <UnstyledLink href={props.href} class={classes()} {...restProps}>
        {props.children}
      </UnstyledLink>
    );
};
