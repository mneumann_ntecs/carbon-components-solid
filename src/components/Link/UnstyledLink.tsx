import { Component, JSX, useContext, createMemo } from "solid-js";
import { Dynamic } from "solid-js/web";
import { CustomLinkContext } from "./CustomLinkContext";

type UnstyledLinkProps = {
  href?: string;
  target?: string;
  class?: string;
} & JSX.HTMLAttributes<HTMLAnchorElement>;

export const UnstyledLink: Component<UnstyledLinkProps> = (props) => {
  const ctx = useContext(CustomLinkContext);

  const rel = createMemo(() =>
    props.target === "_blank" ? "noopener noreferrer" : undefined
  );

  return ctx && ctx.component ? (
    <Dynamic component={ctx.component} rel={rel()} {...props} />
  ) : (
    <a rel={rel()} {...props} />
  );
};
