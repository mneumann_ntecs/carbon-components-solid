export default {
  title: "Loading",
};

import { Loading } from "./Loading";

export const DefaultWithOverlay = () => <Loading />;
export const NoOverlay = () => <Loading withOverlay={false} />;
export const SmallSize = () => <Loading withOverlay={false} small />;
export const Inactive = () => <Loading active={false} />;
export const WithDescription = () => <Loading description="Loading..." />;
