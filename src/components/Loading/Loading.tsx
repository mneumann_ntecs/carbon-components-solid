import { mergeProps, JSX } from "solid-js";
import { LoadingSpinner } from "./LoadingSpinner";

type LoadingProps = {
  /** Set to `true` to use the small variant. Default: false. */
  small?: boolean;

  /** Set to `false` to disable the active state. Default: true */
  active?: boolean;

  /** Set to `false` to disable the overlay. Default: true */
  withOverlay?: boolean;

  /** Specify the label description. Default: "Active loading indicator" */
  description?: string;

  /** Set an id for the label element. Default: unique */
  id?: string;
};

const LoadingPropsDefaults: LoadingProps = {
  small: false,
  active: true,
  withOverlay: true,
  description: "Active loading indicator",
  id: undefined,
};

type LoadingComponent = (props: LoadingProps) => JSX.Element;

export const Loading: LoadingComponent = (allProps) => {
  const props = mergeProps(LoadingPropsDefaults, allProps);

  const loading = () => <LoadingSpinner {...props} />;

  return props.withOverlay ? (
    <div
      classList={{
        "bx--loading-overlay": true,
        "bx--loading-overlay--stop": !props.active,
      }}
    >
      {loading()}
    </div>
  ) : (
    loading()
  );
};
