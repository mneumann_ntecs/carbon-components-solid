import { createMemo } from "solid-js";
import { uniqueId, cx } from "../../utils";

type LoadingSpinnerProps = {
  small?: boolean;
  active?: boolean;
  description?: string;
  id?: string;
  class?: string;
};

const SpinnerRadius = {
  small: "26.8125",
  large: "37.5",
};

export const LoadingSpinner = (props: LoadingSpinnerProps) => {
  const id = createMemo(() => props.id || uniqueId());
  const spinnerRadius = createMemo(() =>
    props.small ? SpinnerRadius.small : SpinnerRadius.large
  );

  return (
    <div
      aria-atomic="true"
      aria-labelledby={id()}
      aria-live={props.active ? "assertive" : "off"}
      class={cx(
        "bx--loading",
        props.small && "bx--loading--small",
        !props.active && "bx--loading--stop",
        props.class
      )}
    >
      <label class="bx--visually-hidden" id={id()}>
        {props.description}
      </label>
      <svg class="bx--loading__svg" viewBox="0 0 150 150">
        <title>{props.description}</title>
        {props.small ? (
          <circle
            class="bx--loading__background"
            cx="50%"
            cy="50%"
            r={spinnerRadius()}
          ></circle>
        ) : null}
        <circle
          class="bx--loading__stroke"
          cx="50%"
          cy="50%"
          r={spinnerRadius()}
        ></circle>
      </svg>
    </div>
  );
};
