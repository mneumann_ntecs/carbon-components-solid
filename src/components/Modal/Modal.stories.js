export default {
  title: "Modal",
};

import { Modal } from "./Modal";
import {
  Button,
  Form,
  Link,
  InlineLoading,
  InlineNotification,
  Loading,
  TextInput,
  Checkbox,
} from "../index";
import { Example as TextInputExample } from "../TextInput/TextInput.stories";
import { createSignal, createState, createMemo } from "solid-js";

export const Example = () => {
  const [isOpen, setOpen] = createSignal(true);
  const open = () => setOpen(true);
  const close = () => setOpen(false);

  return (
    <>
      <Button onClick={open}>Open Modal</Button>

      <Modal
        size="lg"
        open={isOpen()}
        modalHeading="Create Account"
        hasForm
        primaryButtonText="Create Account"
        secondaryButtonText="Cancel"
        onOpen={open}
        onClose={close}
        onSubmit={() => {
          close();
          alert("Submit");
        }}
        onClickSecondaryButton={close}
      >
        <Form>
          <TextInputExample />
        </Form>
      </Modal>
    </>
  );
};

export const SignupExample = () => {
  const [isOpen, setOpen] = createSignal(true);
  const open = () => setOpen(true);
  const close = () => setOpen(false);

  const [state, setState] = createState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    agreeTos: false,
  });

  const [visited, setVisited] = createState({
    firstName: false,
    lastName: false,
    email: false,
    password: false,
    agreeTos: false,
  });

  const errors = {
    firstName: createMemo(() =>
      state.firstName.length === 0 ? ["First name required"] : []
    ),
    lastName: createMemo(() =>
      state.lastName.length === 0 ? ["Last name required"] : []
    ),
    email: createMemo(() =>
      state.email.length === 0 ? ["Email address required"] : []
    ),
    agreeTos: createMemo(() => (!state.agreeTos ? ["Check to continue"] : [])),
  };

  const submitEnabled = createMemo(
    () =>
      errors.firstName().length === 0 &&
      errors.lastName().length === 0 &&
      errors.email().length === 0 &&
      errors.agreeTos().length === 0
  );

  const [isSubmitting, setSubmitting] = createSignal(false);
  const [hasRemoteErrors, setRemoteErrors] = createSignal(false);

  const onSubmit = () => {
    setRemoteErrors(false);
    setSubmitting(true);
    // Simulate API call
    setTimeout(() => {
      setSubmitting(false);
      setRemoteErrors(true);
    }, 1500);
  };

  return (
    <>
      <Button onClick={open}>Open Signup</Button>

      <Modal
        size="sm"
        open={isOpen()}
        modalHeading="Signup for an Account"
        hasForm
        primaryButtonText="Create Account"
        secondaryButtonText="Cancel"
        onOpen={open}
        onClose={close}
        onSubmit={onSubmit}
        onClickSecondaryButton={close}
        primaryButtonDisabled={!submitEnabled()}
      >
        <p style="margin-bottom: 2rem">
          If you already have an account, <Link href="/login">Login</Link>.
        </p>

        {isSubmitting() ? (
          <Loading description="Submitting Registration" />
        ) : undefined}

        <Form>
          <div style="min-height: 5.25rem">
            <TextInput
              labelText="First name"
              placeholder="Enter first name"
              value={() => [
                () => state["firstName"],
                (v) => setState("firstName", v),
              ]}
              invalid={visited.firstName && errors.firstName().length > 0}
              invalidText={errors.firstName().join(". ")}
              onBlur={() => setVisited("firstName", true)}
              required
            />
          </div>

          <div style="margin-top: 0.25rem; min-height: 5.25rem">
            <TextInput
              labelText="Last name"
              placeholder="Enter last name"
              value={() => [
                () => state["lastName"],
                (v) => setState("lastName", v),
              ]}
              invalid={visited.lastName && errors.lastName().length > 0}
              invalidText={errors.lastName().join(". ")}
              onBlur={() => setVisited("lastName", true)}
              required
            />
          </div>

          <div style="margin-top: 0.25rem; height: 5.25rem">
            <TextInput
              type="email"
              labelText="Email"
              placeholder="Enter your email address"
              value={() => [() => state["email"], (v) => setState("email", v)]}
              invalid={visited.email && errors.email().length > 0}
              invalidText={errors.email().join(". ")}
              onBlur={() => setVisited("email", true)}
              required
            />
          </div>

          <div style="margin-top: 0.25rem; min-height: 3.125rem;">
            <Checkbox
              labelText={
                <span>
                  I hereby agree to the{" "}
                  <Link href="/terms">Terms of Service</Link>
                </span>
              }
              checked={state.agreeTos}
              setChecked={(v) => setState("agreeTos", v)}
              invalid={visited.agreeTos && errors.agreeTos().length > 0}
              invalidText={errors.agreeTos().join(". ")}
              onBlur={() => setVisited("agreeTos", true)}
            />
          </div>
        </Form>

        <div style="min-height: 4.5rem; overflow: hidden;">
          <Show when={hasRemoteErrors()}>
            <InlineNotification
              lowContrast
              kind="error"
              title="Registration failed!"
              subtitle="Please retry"
            />
          </Show>
        </div>
      </Modal>
    </>
  );
};

export const WithInlineLoadingOnSubmit = () => {
  const [isOpen, setOpen] = createSignal(true);
  const open = () => setOpen(true);
  const close = () => setOpen(false);
  const [primaryButtonText, setPrimaryButtonText] = createSignal(
    "Create Account"
  );
  const [primaryButtonDisabled, setPrimaryButtonDisabled] = createSignal(false);

  return (
    <>
      <Button onClick={open}>Open Modal</Button>

      <Modal
        size="lg"
        open={isOpen()}
        modalHeading="Create Account"
        hasForm
        primaryButtonText={primaryButtonText()}
        primaryButtonDisabled={primaryButtonDisabled()}
        secondaryButtonText="Cancel"
        onOpen={open}
        onClose={close}
        onSubmit={() => {
          setPrimaryButtonText(
            <InlineLoading
              style="margin-left: 1rem"
              status="active"
              description="Submitting"
            />
          );
          setPrimaryButtonDisabled(true);
          setTimeout(() => {
            setPrimaryButtonText("Create Account");
            setPrimaryButtonDisabled(false);
          }, 3000);
        }}
        onClickSecondaryButton={close}
      >
        <Form>
          <TextInputExample />
        </Form>
      </Modal>
    </>
  );
};
