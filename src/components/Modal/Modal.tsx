import {
  mergeProps,
  createEffect,
  createMemo,
  createSignal,
  onMount,
} from "solid-js";
import { uniqueId, cx } from "../../utils";
import { Button } from "../Button/Button";
import Close20 from "carbon-icons-solid/lib/Close20";

export const Modal = (allProps) => {
  const props = mergeProps(
    {
      size: undefined,
      open: false,
      danger: false,
      alert: false,
      passiveModal: false,
      modalHeading: undefined,
      modalLabel: undefined,
      modalAriaLabel: undefined,
      iconDescription: "Close the modal",
      hasForm: false,
      hasScrollingContent: false,
      primaryButtonText: "",
      primaryButtonDisabled: false,
      shouldSubmitOnEnter: true,
      secondaryButtonText: "",
      selectorPrimaryFocus: "[data-modal-primary-focus]",
      preventCloseOnClickOutside: false,
      id: undefined,
      "aria-label": undefined,
      children: undefined,
      onOpen: undefined,
      onClose: undefined,
      onSubmit: undefined,
      onClickSecondaryButton: undefined,
      hideCloseButton: false,
      class: undefined,
    },
    allProps
  );

  const id = createMemo(() => props.id || uniqueId());
  const modalLabelId = createMemo(
    () => `bx--modal-header__label--modal-${id()}`
  );
  const modalHeadingId = createMemo(
    () => `bx--modal-header__heading--modal-${id()}`
  );
  const modalBodyId = createMemo(() => `bx--modal-body--${id()}`);
  const ariaLabel = createMemo(
    () =>
      props.modalLabel ||
      props["aria-label"] ||
      props.modalAriaLabel ||
      props.modalHeading
  );

  const alertDialogProps = {
    role: createMemo(() =>
      props.alert ? (props.passiveModal ? "alert" : "alertdialog") : undefined
    ),
    ariaDescribedBy: createMemo(() =>
      props.alert && !props.passiveModal ? modalBodyId() : undefined
    ),
  };

  onMount(() => {
    document.body.classList.remove("bx--body--with-modal-open");
  });

  const [opened, setOpened] = createSignal(false);
  const [didClickInnerModal, setDidClickInnerModal] = createSignal(false);

  // Custom event dispatchers
  const dispatchOpen = () => props.onOpen && props.onOpen();
  const dispatchClose = () => props.onClose && props.onClose();
  const dispatchClickSecondaryButton = () =>
    props.onClickSecondaryButton && props.onClickSecondaryButton();
  const dispatchSubmit = () => props.onSubmit && props.onSubmit();

  const closeDialog = () => {
    setOpened(false);
    dispatchClose();
    document.body.classList.remove("bx--body--with-modal-open");
  };

  const onKeyDown = ({ key }: KeyboardEvent) => {
    if (props.open) {
      if (key === "Escape") {
        closeDialog();
      } else if (props.shouldSubmitOnEnter && key === "Enter") {
        dispatchSubmit();
      }
    }
  };

  const onClick = () => {
    if (!didClickInnerModal() && !props.preventCloseOnClickOutside) {
      closeDialog();
    }
    setDidClickInnerModal(false);
  };

  var innerModal: any = null;
  var buttonRef: any = null;

  const focus = (element: any) => {
    const node = element.querySelector(props.selectorPrimaryFocus) || buttonRef;
    if (node) node.focus();
  };

  createEffect(() => {
    if (opened()) {
      if (!props.open) {
        closeDialog();
      }
    } else if (props.open) {
      setOpened(true);
      focus(innerModal);
      dispatchOpen();
      document.body.classList.add("bx--body--with-modal-open");
    }
  });

  const closeButton = () =>
    props.hideCloseButton ? undefined : (
      <button
        ref={buttonRef}
        type="button"
        aria-label={props.iconDescription}
        title={props.iconDescription}
        class="bx--modal-close"
        onClick={closeDialog}
      >
        <Close20
          aria-label={props.iconDescription}
          class="bx--modal-close__icon"
        />
      </button>
    );

  const modalHeaderDiv = () => (
    <div class="bx--modal-header">
      {props.passiveModal && closeButton()}
      {props.modalLabel && (
        <h2 id={modalLabelId()} class="bx--modal-header__label">
          {props.modalLabel}
        </h2>
      )}
      <h3 id={modalHeadingId()} class="bx--modal-header__heading">
        {props.modalHeading}
      </h3>
      {!props.passiveModal && closeButton()}
    </div>
  );

  const modalBody = () => (
    <div
      id={modalBodyId()}
      class={cx(
        "bx--modal-content",
        props.hasForm && "bx--modal-content--with-form",
        props.hasScrollingContent && "bx--modal-scroll-content"
      )}
      tabindex={props.hasScrollingContent ? "0" : undefined}
      role={props.hasScrollingContent ? "region" : undefined}
      aria-label={props.hasScrollingContent ? ariaLabel() : undefined}
      aria-labelledby={props.modalLabel ? modalLabelId() : modalHeadingId()}
    >
      {props.children}
    </div>
  );
  const modalFooter = () => (
    <div class="bx--modal-footer">
      <Button kind="secondary" onClick={() => dispatchClickSecondaryButton()}>
        {props.secondaryButtonText}
      </Button>
      <Button
        kind={props.danger ? "danger" : "primary"}
        disabled={props.primaryButtonDisabled}
        onClick={() => dispatchSubmit()}
      >
        {props.primaryButtonText}
      </Button>
    </div>
  );

  return (
    <div
      role="presentation"
      tabindex="-1"
      id={id()}
      class={cx(
        "bx--modal",
        !props.passiveModal && "bx--modal-tall",
        props.open && "is-visible",
        props.danger && "bx--modal--danger",
        props.class
      )}
      onKeyDown={onKeyDown}
      onClick={onClick}
    >
      <div
        ref={innerModal}
        role={alertDialogProps.role() || "dialog"}
        aria-describedby={alertDialogProps.ariaDescribedBy()}
        aria-modal="true"
        aria-label={ariaLabel()}
        class={cx(
          "bx--modal-container",
          props.size === "xs" && "bx--modal-container--xs",
          props.size === "sm" && "bx--modal-container--sm",
          props.size === "lg" && "bx--modal-container--lg"
        )}
        onClick={() => setDidClickInnerModal(true)}
      >
        {modalHeaderDiv()}
        {modalBody()}

        {props.hasScrollingContent && (
          <div class="bx--modal-content--overflow-indicator"></div>
        )}

        {!props.passiveModal && modalFooter()}
      </div>
    </div>
  );
};
