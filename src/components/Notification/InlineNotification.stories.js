export default {
  title: "Inline Notification",
};

import { InlineNotification } from "./Notification";
import { NotificationActionButton } from "./NotificationActionButton";

export const Default_error = () => <InlineNotification />;

export const Hidden_close_button = () => (
  <InlineNotification
    hideCloseButton
    kind="warning"
    title="Upcoming scheduled maintenance"
  />
);

export const With_actions = () => (
  <InlineNotification
    kind="warning"
    title="Upcoming scheduled maintenance"
    renderActions={
      <NotificationActionButton>Learn more</NotificationActionButton>
    }
  />
);

export const Notification_variants = () => (
  <>
    <InlineNotification kind="error" />
    <InlineNotification kind="info" />
    <InlineNotification kind="info-square" />
    <InlineNotification kind="success" />
    <InlineNotification kind="warning" />
    <InlineNotification kind="warning-alt" />
  </>
);

export const Low_contrast = () => (
  <>
    <InlineNotification lowContrast kind="error" />
    <InlineNotification lowContrast kind="info" />
    <InlineNotification lowContrast kind="info-square" />
    <InlineNotification lowContrast kind="success" />
    <InlineNotification lowContrast kind="warning" />
    <InlineNotification lowContrast kind="warning-alt" />
  </>
);
