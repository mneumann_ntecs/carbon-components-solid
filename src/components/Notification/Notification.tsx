import {
  JSX,
  Component,
  Show,
  onMount,
  onCleanup,
  createSignal,
} from "solid-js";
import { cx } from "../../utils";
import { NotificationKind } from "./NotificationKind";
import { NotificationButton } from "./NotificationButton";
import { NotificationIcon } from "./NotificationIcon";
import { NotificationTextDetails } from "./NotificationTextDetails";

export type NotificationProps = {
  /** Specify the kind of notification icon. Default: "error" */
  kind?: NotificationKind;

  /** Set the type of notification. Default: "toast" */
  notificationType?: "toast" | "inline";

  /** Set to `true` to use the low contrast variant. Default: false */
  lowContrast?: boolean;

  /** Set the timeout duration (ms) to hide the notification after opening it. Default: no timeout */
  timeout?: number;

  /** Set the `role` attribute. Default: "alert" */
  role?: string;

  /** Specify the title text. Default: "Title" */
  title?: string;

  /** Specify the subtitle text. Default: "" */
  subtitle?: string;

  /** Specify the caption text. Default: "Caption" */
  caption?: string;

  /** Specify the ARIA label for the icon. Default: "Closes notification" */
  iconDescription?: string;

  /** Set to `true` to hide the close button. Default: false */
  hideCloseButton?: boolean;

  /** Called when the notification is closed. */
  onClose?: () => void;

  renderActions?: JSX.Element;

  style?: string;

  onClick?: JSX.EventHandler<HTMLDivElement, FocusEvent>;
  onMouseOver?: JSX.EventHandler<HTMLDivElement, FocusEvent>;
  onMouseEnter?: JSX.EventHandler<HTMLDivElement, FocusEvent>;
  onMouseLeave?: JSX.EventHandler<HTMLDivElement, FocusEvent>;
};

export const Notification: Component<NotificationProps> = (props) => {
  const kind = props.kind || "error";
  const notificationType = props.notificationType || "toast";
  const timeout = props.timeout;
  const role = props.role || "alert";
  const title = props.title || "Title";
  const subtitle = props.subtitle || "";
  const caption = props.caption || "Caption";
  const iconDescription = props.iconDescription || "Closes notification";

  const [open, setOpen] = createSignal(true);

  let timeoutId: any = undefined;

  const close = () => {
    setOpen(false);
    props.onClose && props.onClose();
  };

  onMount(() => {
    if (timeout) {
      timeoutId = setTimeout(close, timeout);
    }
  });

  onCleanup(() => {
    if (timeoutId) {
      clearTimeout(timeoutId);
      timeoutId = undefined;
    }
  });

  const renderInner = () => (
    <>
      <NotificationIcon
        notificationType={notificationType}
        kind={kind}
        iconDescription={iconDescription}
      />
      <NotificationTextDetails
        title={title}
        subtitle={subtitle}
        caption={caption}
        notificationType={notificationType}
      >
        {props.children}
      </NotificationTextDetails>
    </>
  );

  const className = cx(
    `bx--${notificationType}-notification`,
    props.lowContrast && `bx--${notificationType}-notification--low-contrast`,
    props.hideCloseButton &&
      `bx--${notificationType}-notification--hide-close-button`,
    `bx--${notificationType}-notification--${kind}`
  );

  return (
    <Show when={open()}>
      <div
        role={role}
        kind={kind}
        class={className}
        onClick={props.onClick}
        onMouseOver={props.onMouseOver}
        onMouseEnter={props.onMouseEnter}
        onMouseLeave={props.onMouseLeave}
        style={props.style}
      >
        {notificationType === "inline" && (
          <div class="bx--inline-notification__details">{renderInner()}</div>
        )}
        {notificationType === "toast" && renderInner()}

        {props.renderActions}

        <Show when={!props.hideCloseButton}>
          <NotificationButton
            iconDescription={iconDescription}
            notificationType={notificationType}
            onClick={close}
          />
        </Show>
      </div>
    </Show>
  );
};

type InlineNotificationProps = Omit<
  NotificationProps,
  "notificationType" | "caption"
>;
type ToastNotificationProps = Omit<
  NotificationProps,
  "notificationType" | "renderActions"
>;

export const InlineNotification: Component<InlineNotificationProps> = (
  props
) => <Notification notificationType="inline" {...props} />;

export const ToastNotification: Component<ToastNotificationProps> = (props) => (
  <Notification notificationType="toast" {...props} />
);
