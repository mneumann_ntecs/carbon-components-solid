import { Button } from "../Button/Button";

export const NotificationActionButton = (props) => (
  <Button
    kind="ghost"
    size="small"
    class="bx--inline-notification__action-button"
    {...props}
  />
);
