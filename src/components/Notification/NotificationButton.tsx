import { JSX } from "solid-js";
import { Dynamic } from "solid-js/web";
import { CarbonIconComponent } from "carbon-icons-solid/lib/types";
import { Close20 } from "carbon-icons-solid/lib/Close20";

type NotificationButtonProps = {
  /** Set the type of notification. Default: "toast" */
  notificationType?: "toast" | "inline";

  /** Specify the icon to render. Default: Close20 */
  renderIcon?: CarbonIconComponent;

  /** Specify the title of the icon. */
  title?: string;

  /** Specify the ARIA label for the icon. Default: "Closes icon" */
  iconDescription?: string;

  onClick?: JSX.EventHandler<HTMLDivElement, FocusEvent>;
  onMouseOver?: JSX.EventHandler<HTMLDivElement, FocusEvent>;
  onMouseEnter?: JSX.EventHandler<HTMLDivElement, FocusEvent>;
  onMouseLeave?: JSX.EventHandler<HTMLDivElement, FocusEvent>;
};

type NotificationButtonComponent = (
  props: NotificationButtonProps
) => JSX.Element;

export const NotificationButton: NotificationButtonComponent = (props) => {
  const notificationType = props.notificationType || "toast";
  const renderIcon = props.renderIcon || Close20;
  const iconDescription = props.iconDescription || "Close icon";

  return (
    <button
      type="button"
      aria-label={iconDescription}
      title={iconDescription}
      class={`bx--${notificationType}-notification__close-button`}
      onClick={props.onClick}
      onMouseOver={props.onMouseOver}
      onMouseEnter={props.onMouseEnter}
      onMouseLeave={props.onMouseLeave}
    >
      <Dynamic
        component={renderIcon}
        title={props.title}
        class={`bx--${notificationType}-notification__close-icon`}
      />
    </button>
  );
};
