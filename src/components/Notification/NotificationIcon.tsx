import { JSX } from "solid-js";
import { Dynamic } from "solid-js/web";
import { CheckmarkFilled20 } from "carbon-icons-solid/lib/CheckmarkFilled20";
import { ErrorFilled20 } from "carbon-icons-solid/lib/ErrorFilled20";
import { InformationFilled20 } from "carbon-icons-solid/lib/InformationFilled20";
import { InformationSquareFilled20 } from "carbon-icons-solid/lib/InformationSquareFilled20";
import { WarningFilled20 } from "carbon-icons-solid/lib/WarningFilled20";
import { WarningAltFilled20 } from "carbon-icons-solid/lib/WarningAltFilled20";
import { CarbonIconComponent } from "carbon-icons-solid/lib/types";
import { NotificationKind } from "./NotificationKind";

type NotificationIconProps = {
  /** Specify the kind of notification icon. Default: "error" */
  kind?: NotificationKind;

  /** Set the type of notification. Default: "toast" */
  notificationType?: "toast" | "inline";

  /** Specify the ARIA label for the icon. Default: "Closes notification" */
  iconDescription?: string;
};

const icons: Record<NotificationKind, CarbonIconComponent> = {
  error: ErrorFilled20,
  info: InformationFilled20,
  "info-square": InformationSquareFilled20,
  success: CheckmarkFilled20,
  warning: WarningFilled20,
  "warning-alt": WarningAltFilled20,
};

type NotificationIconComponent = (props: NotificationIconProps) => JSX.Element;

export const NotificationIcon: NotificationIconComponent = (props) => {
  const kind = props.kind || "error";
  const notificationType = props.notificationType || "toast";
  const iconDescription = props.iconDescription || "Closes notification";

  return (
    <Dynamic
      component={icons[kind]}
      title={iconDescription}
      class={`bx--${notificationType}-notification__icon`}
    />
  );
};
