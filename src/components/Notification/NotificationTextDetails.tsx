import { Component } from "solid-js";

type NotificationTextDetailsProps = {
  /** Set the type of notification. Default: "toast" */
  notificationType?: "toast" | "inline";

  /** Specify the title text. Default: "Title" */
  title?: string;

  /** Specify the subtitle text. Default: "" */
  subtitle?: string;

  /** Specify the caption text. Default: "Caption" */
  caption?: string;
};

export const NotificationTextDetails: Component<NotificationTextDetailsProps> = (
  props
) => {
  const notificationType = props.notificationType || "toast";
  const title = props.title || "Title";
  const subtitle = props.subtitle || "";
  const caption = props.caption || "Caption";

  if (notificationType === "toast") {
    return (
      <div class="bx--toast-notification__details">
        <h3 class="bx--toast-notification__title">{title}</h3>
        <div class="bx--toast-notification__subtitle">{subtitle}</div>
        <div class="bx--toast-notification__caption">{caption}</div>
        {props.children}
      </div>
    );
  }

  if (notificationType === "inline") {
    return (
      <div class="bx--inline-notification__text-wrapper">
        <p class="bx--inline-notification__title">{title}</p>
        <div class="bx--inline-notification__subtitle">{subtitle}</div>
        {props.children}
      </div>
    );
  }
};
