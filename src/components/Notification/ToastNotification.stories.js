export default {
  title: "Toast Notification",
};

import { ToastNotification } from "./Notification";
import { NotificationActionButton } from "./NotificationActionButton";

export const Default_error = () => <ToastNotification />;

export const Hidden_close_button = () => (
  <ToastNotification
    hideCloseButton
    kind="warning"
    title="Upcoming scheduled maintenance"
  />
);

export const Notification_variants = () => (
  <>
    <ToastNotification kind="error" />
    <ToastNotification kind="info" />
    <ToastNotification kind="info-square" />
    <ToastNotification kind="success" />
    <ToastNotification kind="warning" />
    <ToastNotification kind="warning-alt" />
  </>
);

export const Low_contrast = () => (
  <>
    <ToastNotification lowContrast kind="error" />
    <ToastNotification lowContrast kind="info" />
    <ToastNotification lowContrast kind="info-square" />
    <ToastNotification lowContrast kind="success" />
    <ToastNotification lowContrast kind="warning" />
    <ToastNotification lowContrast kind="warning-alt" />
  </>
);
