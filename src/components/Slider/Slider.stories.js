export default {
  title: "Slider",
};

import { Slider } from "./Slider";
import { createSignal } from "solid-js";

export const Default = () => <Slider />;

export const Custom_minimum_maximum_values = () => (
  <Slider
    labelText="Runtime memory (MB)"
    min={10}
    max={990}
    maxLabel="990 MB"
    value={() => createSignal(100)}
  />
);

export const Custom_step_value = () => (
  <Slider
    labelText="Runtime memory (MB)"
    min={10}
    max={990}
    maxLabel="990 MB"
    value={() => createSignal(100)}
    step={10}
  />
);

export const Light_variant = () => (
  <Slider
    light
    labelText="Runtime memory (MB)"
    min={10}
    max={990}
    maxLabel="990 MB"
    value={() => createSignal(100)}
    step={10}
  />
);

export const Read_value = () => {
  const [value, setValue] = createSignal(100);
  return (
    <>
      <p>Current value: {value()}</p>
      <Slider value={() => [value, setValue]} />
    </>
  );
};
