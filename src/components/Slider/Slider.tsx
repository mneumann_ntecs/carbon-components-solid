import { mergeProps, createSignal, createMemo, onCleanup, JSX } from "solid-js";
import { uniqueId, cx, clamp, Signal } from "../../utils";

type SliderProps = {
  /** The value of the slider */
  value?: () => Signal<number>;

  /** Minimum slider value. Default: 0 */
  min?: number;

  /** Maximum slider value. Default: 100 */
  max?: number;

  /** Label for min value. Default: undefined */
  minLabel?: string;

  /** Label for max value. Default: undefined */
  maxLabel?: string;

  /** The step value. Default 1 */
  step?: number;

  /** A value determining how much the value should increase/decrease by Shift+arrow keys,
   * which will be `(max - min) / stepMultiplier`.
   *
   * Default: 4.
   */
  stepMultiplier?: number;

  /** Set to `true` to require a value. Default: false */
  required?: boolean;

  /** The input type. Default: 'number' */
  inputType?: "number"; // XXX

  /** Set to `true` to disable the slider. Default: false */
  disabled?: boolean;

  /** Set to `true` to enable the light variant. Default: false */
  light?: boolean;

  /** Set to `true` to hide the text input. Default: false */
  hideTextInput?: boolean;

  /** The id for the slider div element. Default: unique */
  id?: string;

  /** The id of the label. Default: unique based on `id` */
  labelId?: string;

  /** Set to `true` to indicate an invalid state. Default: false */
  invalid?: boolean;

  /** Specify the label text. Default: "" */
  labelText?: string;

  /** Set a name for the slider element. Default: "" */
  name?: string;

  /** ARIA label for the input element. Default: undefined */
  "aria-label"?: string;
};

const KEYS: { [k: string]: number } = {
  ArrowDown: -1,
  ArrowLeft: -1,
  ArrowRight: 1,
  ArrowUp: 1,
};

type SliderComponent = (props: SliderProps) => JSX.Element;

export const Slider: SliderComponent = (allProps) => {
  const defaultId = createMemo(() => uniqueId());

  const props = mergeProps(
    {
      value: () => createSignal(0),
      min: 0,
      max: 100,
      //minLabel: "",
      //maxLabel: "",
      step: 1,
      stepMultiplier: 4,
      required: false,
      inputType: "number",
      disabled: false,
      light: false,
      hideTextInput: false,
      get id(): string {
        return defaultId();
      },
      get labelId(): string {
        return `label-${props.id}`;
      },
      invalid: false,
      labelText: "",
      name: "",
      "aria-label": "Slider number input",
      class: undefined,
    },
    allProps
  );

  const onDrag = (evt: MouseEvent | TouchEvent) => {
    if (props.disabled) return;
    if (evt instanceof MouseEvent) {
      setValue(calcValue(evt.clientX));
    } else if (evt instanceof TouchEvent && evt.touches.length > 0) {
      setValue(calcValue(evt.touches[0].clientX));
    }
  };

  const onDragStop = (evt: MouseEvent | TouchEvent) => {
    if (props.disabled) return;

    const doc = trackRef!.ownerDocument;
    removeDragStopHandlers(doc);
    removeDragHandlers(doc);

    onDrag(evt);
  };

  const onDragStart = (evt: MouseEvent | TouchEvent) => {
    if (props.disabled) return;

    const doc = trackRef!.ownerDocument;

    registerDragStopHandlers(doc);
    registerDragHandlers(doc);

    onDrag(evt);
  };

  const registerDragHandlers = (doc: HTMLDocument) => {
    doc.addEventListener("mousemove", onDrag);
    doc.addEventListener("touchmove", onDrag);
  };

  const removeDragHandlers = (doc: HTMLDocument) => {
    doc.removeEventListener("mousemove", onDrag);
    doc.removeEventListener("touchmove", onDrag);
  };

  const registerDragStopHandlers = (doc: HTMLDocument) => {
    doc.addEventListener("mouseup", onDragStop);
    doc.addEventListener("touchend", onDragStop);
    doc.addEventListener("touchcancel", onDragStop);
  };

  const removeDragStopHandlers = (doc: HTMLDocument) => {
    doc.removeEventListener("mouseup", onDragStop);
    doc.removeEventListener("touchend", onDragStop);
    doc.removeEventListener("touchcancel", onDragStop);
  };

  onCleanup(() => {
    const doc = trackRef!.ownerDocument;

    removeDragStopHandlers(doc);
    removeDragHandlers(doc);
  });

  let trackRef: HTMLDivElement | undefined = undefined;

  const [_value, _setValue] = props.value();

  const clampedValue = createMemo(() => clamp(_value(), props.min, props.max));
  const setValue = (nextValue: number) =>
    _setValue(clamp(nextValue, props.min, props.max));

  const range = createMemo(() => props.max - props.min);
  const left = createMemo(() => ((clampedValue() - props.min) / range()) * 100);

  // clamp it initially
  setValue(_value());

  const formatLabel = (value: number, valueLabel: string | undefined) =>
    valueLabel !== undefined ? valueLabel : value;

  const calcValue = (offsetX: number) => {
    const { left, width } = trackRef!.getBoundingClientRect();
    return (
      props.min +
      Math.round((range() * ((offsetX - left) / width)) / props.step) *
        props.step
    );
  };

  const onKeyDown = ({ shiftKey, key }: KeyboardEvent) => {
    if (props.disabled) return;

    const direction = KEYS[key];
    if (direction) {
      const delta =
        props.step *
        (shiftKey ? range() / props.step / props.stepMultiplier : 1) *
        direction;
      if (delta) {
        setValue(clampedValue() + delta);
      }
    }
  };

  const onChange = (evt: Event) => {
    if (props.disabled) return;

    const target = evt.target! as HTMLInputElement;
    const targetValue = Number.parseFloat(target.value);

    if (isNaN(targetValue)) {
      // XXX
      setValue(props.min);
    } else {
      setValue(targetValue);
    }

    // Update the <input> value, as clampedValue is a memo.
    target.value = clampedValue().toString();
  };

  return (
    <div class={cx("bx--form-item", props.class)}>
      <label
        for={props.id}
        class={cx("bx--label", props.disabled && "bx--label--disabled")}
        id={props.labelId}
      >
        {props.labelText}
      </label>
      <div class="bx--slider-container">
        <span class="bx--slider__range-label">
          {formatLabel(props.min, props.minLabel)}
        </span>

        <div
          role="presentation"
          tabindex="-1"
          class={cx("bx--slider", props.disabled && "bx--slider--disabled")}
          onMouseDown={onDragStart}
          onTouchStart={onDragStart}
          onKeyDown={onKeyDown}
          data-invalid={props.invalid || undefined}
        >
          <div
            role="slider"
            tabindex="0"
            class="bx--slider__thumb"
            style={`left: ${left()}%`}
            aria-labelledby={props.labelId}
            aria-valuemax={props.max}
            aria-valuemin={props.min}
            aria-valuenow={clampedValue()}
            id={props.id}
          />

          <div ref={trackRef} class="bx--slider__track" />

          <div
            class="bx--slider__filled-track"
            style={`transform: translate(0, -50%) scaleX(${left() / 100})`}
          />
        </div>
        <span class="bx--slider__range-label">
          {formatLabel(props.max, props.maxLabel)}
        </span>

        <input
          type={props.hideTextInput ? "hidden" : props.inputType}
          style={props.hideTextInput ? "display: none" : undefined}
          id={`${props.id}-input-for-slider`}
          name={props.name}
          class={cx(
            "bx--text-input",
            "bx--slider-text-input",
            props.light && "bx--text-input--light",
            props.invalid && "bx--text-input--invalid"
          )}
          value={clampedValue()}
          aria-label={props["aria-label"]}
          disabled={props.disabled}
          required={props.required}
          min={props.min}
          max={props.max}
          step={props.step}
          onChange={onChange}
          data-invalid={props.invalid || undefined}
          aria-invalid={props.invalid || undefined}
        />
      </div>
    </div>
  );
};
