import { JSX } from "solid-js";
import { Signal, cx } from "../../utils";
import { WarningFilled16 } from "carbon-icons-solid/lib/WarningFilled16";
import { WarningAltFilled16 } from "carbon-icons-solid/lib/WarningAltFilled16";

export type InputSize = "sm" | "xl";

export type FormRequirement = {
  /** Default: `error-${id}` */
  errorId?: string;
  invalid?: boolean;
  invalidText?: string;

  /** Default: `warn-${id}` */
  warnId?: string;
  warn?: boolean;
  warnText?: string;
};

export type CommonInputProps = {
  id?: string; // default: unique
  name?: string;
  size?: InputSize;
  placeholder?: string;
  light?: boolean;
  disabled?: boolean;
  required?: boolean;
  tabindex?: string | number;
  /** `value` itself is not reactive. But the signal obtained by calling
   * value() is of course reactive.
   */
  value?: () => Signal<string>;

  /** If set to `true` display the label in the same line as the text input field */
  inline?: boolean;

  helperText?: string;
  labelText?: string;
  hideLabel?: boolean;

  /** Style applied to the outer <div> element. */
  style?: string;

  onBlur?: JSX.EventHandler<HTMLInputElement, FocusEvent>;
} & FormRequirement;

const inputClasses = (
  props: {
    size?: InputSize;
    light?: boolean;
    invalid?: boolean;
    warn?: boolean;
  },
  controlType: "PasswordInput" | "TextInput"
) =>
  cx(
    "bx--text-input",
    controlType === "PasswordInput" && "bx--password-input",
    props.light && "bx--text-input--light",
    props.invalid && "bx--text-input--invalid",
    props.warn && "bx--text-input--warn",
    props.size === "sm" && "bx--text-input--sm",
    props.size === "xl" && "bx--text-input--xl"
  );

const inputWrapperClasses = (
  props: { light?: boolean; inline?: boolean },
  controlType: "PasswordInput" | "TextInput"
) =>
  cx(
    "bx--form-item",
    "bx--text-input-wrapper",
    props.light && "bx--text-input-wrapper--light",
    props.inline && "bx--text-input-wrapper--inline",
    controlType === "PasswordInput" && "bx--password-input-wrapper"
  );

const fieldWrapperClasses = (props: { invalid?: boolean; warn?: boolean }) =>
  cx(
    "bx--text-input__field-wrapper",
    !props.invalid && props.warn && "bx--text-input__field-wrapper--warning"
  );

const labelClasses = (props: {
  hideLabel?: boolean;
  disabled?: boolean;
  inline?: boolean;
  size?: InputSize;
}) =>
  cx(
    "bx--label",
    props.hideLabel && "bx--visually-hidden",
    props.disabled && "bx--label--disabled",
    props.inline && "bx--label--inline",
    props.inline && props.size === "sm" && "bx--label--inline--sm",
    props.inline && props.size === "xl" && "bx--label--inline--xl"
  );

const renderFieldWrapper = (
  props: CommonInputProps & { fluid?: boolean },
  input: JSX.Element
) => {
  return (
    <div
      data-invalid={props.invalid || undefined}
      class={fieldWrapperClasses(props)}
    >
      {props.invalid && (
        <WarningFilled16 class="bx--text-input__invalid-icon" />
      )}
      {!props.invalid && props.warn && (
        <WarningAltFilled16 class="bx--text-input__invalid-icon bx--text-input__invalid-icon--warning" />
      )}
      {input}
      {props.fluid && <hr class="bx--text-input__divider" />}
      {props.fluid && !props.inline && renderFormRequirement(props)}
    </div>
  );
};

const renderFormRequirement = (props: FormRequirement) =>
  props.invalid || props.warn ? (
    <div
      class="bx--form-requirement"
      id={props.invalid ? props.errorId : props.warn ? props.warnId : undefined}
    >
      {props.invalid
        ? props.invalidText
        : props.warn
        ? props.warnText
        : undefined}
    </div>
  ) : null;

const renderHelperText = (props: {
  inline?: boolean;
  helperText?: string;
  disabled?: boolean;
}) =>
  props.helperText ? (
    <div class={helperTextClasses(props)}>{props.helperText}</div>
  ) : null;

const helperTextClasses = (props: { disabled?: boolean; inline?: boolean }) =>
  cx(
    "bx--form__helper-text",
    props.disabled && "bx--form__helper-text--disabled",
    props.inline && "bx--form__helper-text--inline"
  );

const fieldOuterWrapperClasses = (props: { inline?: boolean }) =>
  cx(
    "bx--text-input__field-outer-wrapper",
    props.inline && "bx--text-input__field-outer-wrapper--inline"
  );

export const renderInput = (
  props: CommonInputProps,
  typeFn: () => string,
  controlType: "TextInput" | "PasswordInput"
) => {
  const [value, setValue] = props.value!();

  return (
    <input
      data-invalid={props.invalid || undefined}
      aria-invalid={props.invalid || undefined}
      aria-describedby={
        props.invalid ? props.errorId : props.warn ? props.warnId : undefined
      }
      title={props.placeholder}
      disabled={props.disabled}
      id={props.id}
      name={props.name}
      placeholder={props.placeholder}
      type={typeFn()}
      value={value()}
      required={props.required}
      class={inputClasses(props, controlType)}
      tabindex={props.tabindex}
      onInput={({ target }) => {
        setValue(target.value);
      }}
      onBlur={props.onBlur}
      data-toggle-password-visibility={
        controlType === "PasswordInput" ? "true" : undefined
      }
    />
  );
};

const renderLabel = (props: CommonInputProps) =>
  props.inline ? renderInlineLabel(props) : renderNormalLabel(props);

const renderInlineLabel = (props: CommonInputProps & { fluid?: boolean }) => (
  <div class="bx--text-input__label-helper-wrapper">
    {renderNormalLabel(props)}
    {!props.fluid && renderHelperText(props)}
  </div>
);

const renderNormalLabel = (props: CommonInputProps) =>
  props.labelText ? (
    <label for={props.id} class={labelClasses(props)}>
      {props.labelText}
    </label>
  ) : null;

export const renderControl = (
  props: CommonInputProps & { fluid?: boolean },
  controlType: "TextInput" | "PasswordInput",
  typeFn: () => string,
  afterInput: JSX.Element | null
) => (
  <div style={props.style} class={inputWrapperClasses(props, controlType)}>
    {renderLabel(props)}

    <div class={fieldOuterWrapperClasses(props)}>
      {renderFieldWrapper(
        props,
        <>
          {renderInput(props, typeFn, controlType)}
          {afterInput}
        </>
      )}
      {!props.fluid && renderFormRequirement(props)}
      {!props.fluid &&
        !props.invalid &&
        !props.warn &&
        !props.inline &&
        renderHelperText(props)}
    </div>
  </div>
);
