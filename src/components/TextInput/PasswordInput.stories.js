export default {
  title: "Password Input",
};

import { createSignal } from "solid-js";
import { PasswordInput } from "./PasswordInput";

export const Sizes = () => (
  <>
    <PasswordInput labelText="Small (sm)" size="sm" />
    <PasswordInput labelText="Default" />
    <PasswordInput labelText="Extra large (xl)" size="xl" />
  </>
);

export const WithLabel = () => (
  <PasswordInput labelText="This is the label" placeholder="Enter..." />
);

export const HiddenLabel = () => (
  <PasswordInput
    labelText="This is the label"
    hideLabel
    placeholder="Enter..."
  />
);

export const WithHelperText = () => (
  <PasswordInput helperText="This is the helper text" placeholder="Enter..." />
);

export const LabelAndHelperText = () => (
  <PasswordInput
    labelText="This is the label"
    helperText="This is the helper text"
    placeholder="Enter..."
  />
);

export const Light = () => (
  <PasswordInput light labelText="This is the label" placeholder="Enter..." />
);

export const Invalid = () => (
  <PasswordInput
    labelText="This is the label"
    invalid
    invalidText="Invalid text"
    placeholder="Enter..."
  />
);

export const Warn = () => (
  <PasswordInput
    labelText="This is the label"
    warn
    warnText="Warn text"
    placeholder="Enter..."
  />
);

export const Inline = () => (
  <PasswordInput inline labelText="This is the label" />
);

export const InlineInvalid = () => (
  <PasswordInput
    inline
    labelText="This is the label"
    invalid
    invalidText="Invalid text"
  />
);

export const Example = () => {
  const [password, setPassword] = createSignal("");
  return (
    <PasswordInput
      size="xl"
      labelText="Password"
      placeholder="Password"
      invalid={password().length < 4}
      invalidText="Minimum 4 characters"
      value={() => [password, setPassword]}
    />
  );
};
