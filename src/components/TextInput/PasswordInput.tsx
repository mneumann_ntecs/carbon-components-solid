import { createMemo, createSignal, mergeProps, JSX } from "solid-js";
import { uniqueId } from "../../utils";
import {
  TooltipPosition,
  tooltipPositionClass,
} from "../../helpers/TooltipPosition";
import {
  TooltipAlignment,
  tooltipAlignmentClass,
} from "../../helpers/TooltipAlignment";
import View16 from "carbon-icons-solid/lib/View16";
import ViewOff16 from "carbon-icons-solid/lib/ViewOff16";
import { renderControl, CommonInputProps } from "./Common";
import { cx } from "../../utils";

type PasswordInputProps = {
  /** Defaults to "password" */
  type?: "text" | "password";

  /** Defaults to "Hide password" */
  hidePasswordLabel?: string;
  /** Defaults to "Show password" */
  showPasswordLabel?: string;

  tooltipPosition?: TooltipPosition;
  tooltipAlignment?: TooltipAlignment;
} & CommonInputProps;

type PasswordInputComponent = (props: PasswordInputProps) => JSX.Element;

export const PasswordInput: PasswordInputComponent = (allProps) => {
  const defaultId = createMemo(() => uniqueId());

  const props = mergeProps(
    {
      type: "password",
      hidePasswordLabel: "Hide password",
      showPasswordLabel: "Show password",

      get id(): string {
        return defaultId();
      },
      get warnId(): string {
        return `warn-${props.id}`;
      },
      get errorId(): string {
        return `error-${props.id}`;
      },
      value: () => createSignal(""),
    },
    allProps,
    // XXX: PasswordInput cannot be fluid (yet). Don't put it inside a <FluidForm>
    { fluid: false }
  );

  const [type, setType] = createSignal(props.type);
  const togglePasswordVisibility = () => {
    setType(type() === "password" ? "text" : "password");
  };
  const passwordIsVisible = createMemo(() => type() === "text");

  const renderButton = () => (
    <button
      type="button"
      class={passwordVisibilityToggleClasses(props)}
      onClick={togglePasswordVisibility}
    >
      <span class="bx--assistive-text">
        {passwordIsVisible()
          ? props.hidePasswordLabel
          : props.showPasswordLabel}
      </span>
      {passwordIsVisible() ? (
        <ViewOff16 class="bx--icon-visibility-off" />
      ) : (
        <View16 class="bx--icon-visibility-on" />
      )}
    </button>
  );

  return renderControl(props, "PasswordInput", type, renderButton());
};

const passwordVisibilityToggleClasses = (props: {
  tooltipPosition?: TooltipPosition;
  tooltipAlignment?: TooltipAlignment;
}) =>
  cx(
    "bx--text-input--password__visibility__toggle",
    "bx--btn--icon-only",
    "bx--tooltip__trigger",
    "bx--tooltip--a11y",
    tooltipPositionClass(props.tooltipPosition),
    tooltipAlignmentClass(props.tooltipAlignment)
  );
