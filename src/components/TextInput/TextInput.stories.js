export default {
  title: "Text Input",
};

import { TextInput } from "./TextInput";
import { createSignal } from "solid-js";

export const Sizes = () => (
  <>
    <TextInput labelText="Small (sm)" size="sm" />
    <TextInput labelText="Default" />
    <TextInput labelText="Extra large (xl)" size="xl" />
  </>
);

export const WithLabel = () => (
  <TextInput labelText="This is the label" placeholder="Enter..." />
);

export const HiddenLabel = () => (
  <TextInput labelText="This is the label" hideLabel placeholder="Enter..." />
);

export const WithHelperText = () => (
  <TextInput helperText="This is the helper text" placeholder="Enter..." />
);

export const LabelAndHelperText = () => (
  <TextInput
    labelText="This is the label"
    helperText="This is the helper text"
    placeholder="Enter..."
  />
);

export const Light = () => (
  <TextInput light labelText="This is the label" placeholder="Enter..." />
);

export const Invalid = () => (
  <TextInput
    labelText="This is the label"
    invalid
    invalidText="Invalid text"
    placeholder="Enter..."
  />
);

export const Warn = () => (
  <TextInput
    labelText="This is the label"
    warn
    warnText="Warn text"
    placeholder="Enter..."
  />
);

export const Fluid = () => (
  <form class="bx--form bx--form--fluid">
    <TextInput fluid placeholder="..." labelText="This is the label" />
  </form>
);

export const FluidInvalid = () => (
  <form class="bx--form bx--form--fluid">
    <TextInput
      fluid
      invalid
      invalidText="INVALID"
      placeholder="..."
      labelText="This is the label"
    />
  </form>
);

export const Inline = () => <TextInput inline labelText="This is the label" />;

export const InlineInvalid = () => (
  <TextInput
    inline
    labelText="This is the label"
    invalid
    invalidText="Invalid text"
  />
);

export const Example = () => {
  const [firstName, setFirstName] = createSignal("");
  const [lastName, setLastName] = createSignal("");

  const [lastNameBlur, setLastNameBlur] = createSignal("");
  const [lastNameVisited, setLastNameVisited] = createSignal(false);

  return (
    <>
      <TextInput
        type="text"
        required
        labelText="First name"
        placeholder="Enter first name..."
        invalid={firstName().length < 4}
        invalidText="Minimum 4 characters"
        warn={firstName().length > 10}
        warnText="More than 10 characters"
        value={() => [firstName, setFirstName]}
      />
      <TextInput
        type="text"
        required
        labelText="Last name"
        placeholder="Enter last name..."
        invalid={lastName().length < 1}
        invalidText="Required"
        value={() => [lastName, setLastName]}
      />

      <TextInput
        type="text"
        required
        labelText="Last name (on Blur)"
        placeholder="Enter last name..."
        invalid={lastNameVisited() && lastNameBlur().length < 1}
        invalidText="Required"
        onBlur={() => setLastNameVisited(true)}
        value={() => [lastNameBlur, setLastNameBlur]}
      />
    </>
  );
};
