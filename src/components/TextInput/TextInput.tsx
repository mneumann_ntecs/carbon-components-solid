import { createSignal, JSX, mergeProps, createMemo } from "solid-js";
import { uniqueId } from "../../utils";
import { renderControl, CommonInputProps } from "./Common";

type TextInputProps = {
  /** Defaults to "text" */
  type?: "text" | "password" | "email";

  /** Default: Use context supplied value. */
  fluid?: boolean;
} & CommonInputProps;

type TextInputComponent = (props: TextInputProps) => JSX.Element;

export const TextInput: TextInputComponent = (allProps) => {
  const defaultId = createMemo(() => uniqueId());

  const fluidContext = { fluid: false }; // XXX

  const props = mergeProps(
    {
      type: "text",
      get id(): string {
        return defaultId();
      },
      get warnId(): string {
        return `warn-${props.id}`;
      },
      get errorId(): string {
        return `error-${props.id}`;
      },
      fluid: fluidContext.fluid,
      value: () => createSignal(""),
    },
    allProps
  );

  return renderControl(props, "TextInput", () => props.type, null);
};
