export default {
  title: "Text Input Skeleton",
};

import { TextInputSkeleton } from "./TextInputSkeleton";

export const Default = () => <TextInputSkeleton />;

export const HideLabel = () => <TextInputSkeleton hideLabel />;
