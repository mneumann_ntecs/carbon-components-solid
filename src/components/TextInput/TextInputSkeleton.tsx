import { splitProps } from "solid-js";
import { cx } from "../../utils";

export const TextInputSkeleton = (allProps) => {
  const [props, restProps] = splitProps(allProps, ["hideLabel", "class"]);

  return (
    <div class={cx("bx--form-item", props.class)} {...restProps}>
      {!props.hideLabel && <span class="bx--label bx--skeleton"></span>}
      <div class="bx--skeleton bx--text-area"></div>
    </div>
  );
};
