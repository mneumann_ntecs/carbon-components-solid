export default {
  title: "Theme",
};

import { Theme } from "./Theme";
import { Example } from "../Modal/Modal.stories";

export const White = () => (
  <>
    <Theme theme="white" />
    <Example />
  </>
);
export const G10 = () => (
  <>
    <Theme theme="g10" />
    <Example />
  </>
);
export const G90 = () => (
  <>
    <Theme theme="g90" />
    <Example />
  </>
);
export const G100 = () => (
  <>
    <Theme theme="g100" />
    <Example />
  </>
);
