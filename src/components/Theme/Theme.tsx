import { createEffect, Component } from "solid-js";

export const AllThemes = ["white", "g10", "g90", "g100"];

export const isValidTheme = (theme: string) => AllThemes.includes(theme);
export const isDarkTheme = (theme: string) =>
  theme === "g90" || theme === "g100";
export const isLightTheme = (theme: string) => !isDarkTheme(theme);

type ThemeProps = {
  theme: "white" | "g10" | "g90" | "g100";
};

export const Theme: Component<ThemeProps> = (props) => {
  createEffect(() => {
    const theme = props.theme;
    if (isValidTheme(theme)) {
      document.documentElement.setAttribute("theme", theme);
    }
  });

  return props.children;
};
