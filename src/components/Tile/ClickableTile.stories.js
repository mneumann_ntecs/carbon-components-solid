export default {
  title: "ClickableTile",
};

import { ClickableTile } from "./ClickableTile";

export const Default = () => (
  <ClickableTile href="https://www.carbondesignsystem.com/">
    Carbon Design System
  </ClickableTile>
);

export const LightVariant = () => (
  <ClickableTile light href="https://www.carbondesignsystem.com/">
    Carbon Design System
  </ClickableTile>
);

export const Multiple = () => (
  <>
    <For each={[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}>
      {(i) => <ClickableTile>{i}</ClickableTile>}
    </For>
  </>
);
