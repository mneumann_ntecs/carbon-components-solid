import { Component, JSX } from "solid-js";
import { UnstyledLink } from "../Link/UnstyledLink";
import { cx } from "../../utils";

type ClickableTileProps = {
  /** When `true` shows the light variant. Default: false. */
  light?: boolean;

  /** Called when the tile is clicked, either via mouse or keyboard */
  onClicked?: () => void;

  target?: string;
  href?: string;
  class?: string;

  onClick?: JSX.EventHandler<HTMLAnchorElement, FocusEvent>;
  onMouseOver?: JSX.EventHandler<HTMLAnchorElement, FocusEvent>;
  onMouseEnter?: JSX.EventHandler<HTMLAnchorElement, FocusEvent>;
  onMouseLeave?: JSX.EventHandler<HTMLAnchorElement, FocusEvent>;
  onKeyDown?: JSX.EventHandler<HTMLAnchorElement, KeyboardEvent>;
};

export const ClickableTile: Component<ClickableTileProps> = (props) => (
  <UnstyledLink
    href={props.href}
    target={props.target}
    class={cx(
      "bx--tile",
      "bx--tile--clickable",
      props.light && "bx--tile--light"
    )}
    onClick={(ev) => {
      props.onClick && props.onClick(ev);
      props.onClicked && props.onClicked();
    }}
    onKeyDown={(ev) => {
      props.onKeyDown && props.onKeyDown(ev);
      if (ev.key === " " || ev.key === "Enter") {
        props.onClicked && props.onClicked();
      }
    }}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
  >
    {props.children}
  </UnstyledLink>
);
