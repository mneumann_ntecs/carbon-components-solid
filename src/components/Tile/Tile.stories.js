export default {
  title: "Tile",
};

import { Tile } from "./Tile";

export const Default = () => <Tile>Content</Tile>;

export const LightVariant = () => <Tile light>Content</Tile>;
