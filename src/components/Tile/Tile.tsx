import { Component, JSX } from "solid-js";
import { cx } from "../../utils";

type TileProps = {
  /** Set to `true` to enable the light variant. Default: false. */
  light?: boolean;

  class?: string;

  onClick?: JSX.EventHandler<HTMLDivElement, FocusEvent>;
  onMouseOver?: JSX.EventHandler<HTMLDivElement, FocusEvent>;
  onMouseEnter?: JSX.EventHandler<HTMLDivElement, FocusEvent>;
  onMouseLeave?: JSX.EventHandler<HTMLDivElement, FocusEvent>;
};

export const Tile: Component<TileProps> = (props) => (
  <div
    class={cx("bx--tile", props.light && "bx--tile--light", props.class)}
    onClick={props.onClick}
    onMouseOver={props.onMouseOver}
    onMouseEnter={props.onMouseEnter}
    onMouseLeave={props.onMouseLeave}
  >
    {props.children}
  </div>
);
