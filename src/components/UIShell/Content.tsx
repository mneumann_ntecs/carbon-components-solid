import { Component, JSX, mergeProps, splitProps } from "solid-js";

type ContentProps = JSX.HTMLAttributes<HTMLElement>;

export const Content: Component<ContentProps> = (props) => {
  props = mergeProps({ id: "main-content" }, props);
  const [local, restProps] = splitProps(props, ["class"]);

  return <main class={`bx--content ${local.class || ""}`} {...restProps} />;
};
