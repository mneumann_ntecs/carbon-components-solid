import {
  createSignal,
  createEffect,
  onMount,
  onCleanup,
  mergeProps,
  Show,
} from "solid-js";
import { HamburgerMenu } from "../SideNav/HamburgerMenu";
import { UnstyledLink } from "../../Link/UnstyledLink";
import { shouldRenderHamburgerMenu } from "../navStore";

export const Header = (allProps) => {
  const props = mergeProps(
    {
      expandedByDefault: true,
      persistentHamburgerMenu: false,
      "aria-label": undefined,
    },
    allProps
  );

  const [winWidth, setWinWidth] = createSignal(window.innerWidth);
  const [, setSideNavOpen] = props.sideNavOpenSignal;

  createEffect(() => {
    setSideNavOpen(
      props.expandedByDefault &&
        winWidth() >= 1056 &&
        !props.persistentHamburgerMenu
    );
  });

  const onResize = () => setWinWidth(window.innerWidth);

  onMount(() => window.addEventListener("resize", onResize));
  onCleanup(() => window.removeEventListener("resize", onResize));

  return (
    <header role="banner" aria-label={props["aria-label"]} class="bx--header">
      {props.skipToContent}
      <Show
        when={
          (shouldRenderHamburgerMenu() && winWidth() < 1056) ||
          props.persistentHamburgerMenu
        }
      >
        <HamburgerMenu openSignal={props.sideNavOpenSignal} />
      </Show>
      <UnstyledLink href={props.href} class="bx--header__name">
        {props.company && (
          <span class="bx--header__name--prefix">{props.company}&nbsp;</span>
        )}
        {props.platform}
      </UnstyledLink>
      {props.children}
    </header>
  );
};
