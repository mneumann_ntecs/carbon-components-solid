import { Component, JSX, splitProps } from "solid-js";
import { cx } from "../../../utils";

type HeaderNavProps = {
  /** Specify the ARIA label for the nav. */
  "aria-label"?: string;

  class?: string;
} & JSX.HTMLAttributes<HTMLElement>;

export const HeaderNav: Component<HeaderNavProps> = (props) => {
  const [local, restProps] = splitProps(props, [
    "children",
    "aria-label",
    "class",
  ]);

  return (
    <nav
      aria-label={local["aria-label"]}
      class={cx("bx--header__nav", local.class)}
      {...restProps}
    >
      <ul
        role="menubar"
        aria-label={local["aria-label"]}
        class="bx--header__menu-bar"
      >
        {local.children}
      </ul>
    </nav>
  );
};
