import { JSX, splitProps } from "solid-js";
import { UnstyledLink } from "../../Link/UnstyledLink";
import { cx } from "../../../utils";

type HeaderNavItemProps = {
  href?: string;
  text?: string;
  class?: string;
} & JSX.AnchorHTMLAttributes<HTMLAnchorElement>;

type HeaderNavItemComponent = (props: HeaderNavItemProps) => JSX.Element;

export const HeaderNavItem: HeaderNavItemComponent = (props) => {
  const [, restProps] = splitProps(props, [
    "href",
    "text",
    "children",
    "class",
  ]);

  return (
    <li>
      <UnstyledLink
        role="menuitem"
        tabindex="0"
        href={props.href}
        class={cx("bx--header__menu-item", props.class)}
        {...restProps}
      >
        <span class="bx--text-truncate--end">{props.text}</span>
      </UnstyledLink>
    </li>
  );
};
