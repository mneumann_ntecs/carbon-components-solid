import { Component } from "solid-js";

export const HeaderUtilities: Component<{}> = (props) => (
  <div class="bx--header__global">{props.children}</div>
);
