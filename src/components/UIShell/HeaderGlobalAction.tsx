import { JSX, Component } from "solid-js";
import { cx } from "../../utils";

type HeaderGlobalActionProps = {
  isActive?: boolean;
  icon?: JSX.Element;
  onClick?: JSX.EventHandler<HTMLButtonElement, MouseEvent>;
  "aria-label"?: string;
  "aria-labelledby"?: string;
  class?: string;
};

export const HeaderGlobalAction: Component<HeaderGlobalActionProps> = (
  props
) => {
  return (
    <button
      type="button"
      class={cx(
        "bx--header__action",
        props.isActive && "bx--header__action--active",
        props.class
      )}
      aria-label={props["aria-label"]}
      aria-labelledby={props["aria-labelledby"]}
      onClick={props.onClick}
    >
      {props.icon}
      {props.children}
    </button>
  );
};
