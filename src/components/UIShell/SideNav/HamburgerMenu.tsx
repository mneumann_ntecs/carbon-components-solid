import { splitProps, JSX } from "solid-js";
import Close20 from "carbon-icons-solid/lib/Close20";
import Menu20 from "carbon-icons-solid/lib/Menu20";
import { cx } from "../../../utils";

type HamburgerMenuProps = {
  openSignal: [() => boolean, (s: boolean) => void];
  class?: string;
} & JSX.ButtonHTMLAttributes<HTMLButtonElement>;
type HamburgerMenuComponent = (props: HamburgerMenuProps) => JSX.Element;

export const HamburgerMenu: HamburgerMenuComponent = (props) => {
  const [local, restProps] = splitProps(props, ["openSignal", "class"]);

  let [isOpen, setOpen] = local.openSignal;

  return (
    <button
      type="button"
      title="Open menu"
      class={cx(
        "bx--header__action",
        "bx--header__menu-trigger",
        "bx--header__menu-toggle",
        local.class
      )}
      onClick={() => setOpen(!isOpen())}
      {...restProps}
    >
      {isOpen() ? <Close20 title="Close" /> : <Menu20 title="Open Menu" />}
    </button>
  );
};
