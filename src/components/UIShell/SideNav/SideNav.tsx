import { onMount, onCleanup, Component, JSX, splitProps } from "solid-js";
import { setShouldRenderHamburgerMenu } from "../navStore";
import { cx } from "../../../utils";

type SideNavProps = {
  /** Set to `true` to use the fixed variant. Default: false */
  fixed?: boolean;

  sideNavOpenSignal: [() => boolean, (s: boolean) => void];

  /**
   * Specify the ARIA label for the nav
   */
  "aria-label"?: string;

  class?: string;
} & JSX.HTMLAttributes<HTMLElement>;

export const SideNav: Component<SideNavProps> = (props) => {
  const [local, restProps] = splitProps(props, [
    "children",
    "fixed",
    "sideNavOpenSignal",
    "class",
  ]);

  const [isOpen, setOpen] = local.sideNavOpenSignal;

  const close = () => {
    setOpen(false);
  };

  onMount(() => {
    setShouldRenderHamburgerMenu(true);
  });

  onCleanup(() => {
    setShouldRenderHamburgerMenu(false);
  });

  return () => (
    <>
      {local.fixed ? null : (
        <div
          onClick={close}
          class={cx(
            "bx--side-nav__overlay",
            isOpen() && "bx--side-nav__overlay-active",
            local.class
          )}
        ></div>
      )}
      <nav
        class={cx(
          "bx--side-nav",
          isOpen() && "bx--side-nav--expanded",
          !isOpen() && local.fixed && "bx--side-nav--collapsed",
          "bx--side-nav__navigation",
          "bx--side-nav--ux",
          local.class
        )}
        {...restProps}
      >
        {local.children}
      </nav>
    </>
  );
};
