import { Component } from "solid-js";
import { cx } from "../../../utils";

type SideNavIconProps = {
  small?: boolean;
  class?: string;
};

export const SideNavIcon: Component<SideNavIconProps> = (props) => (
  <div
    class={cx(
      "bx--side-nav__icon",
      props.small && "bx--side-nav__item--small",
      props.class
    )}
  >
    {props.children}
  </div>
);
