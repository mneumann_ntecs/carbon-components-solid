import { Component } from "solid-js";
import { cx } from "../../../utils";

type SideNavItemProps = {
  large?: boolean;
  class?: string;
};

export const SideNavItem: Component<SideNavItemProps> = (props) => (
  <li
    class={cx(
      "bx--side-nav__item",
      props.large && "bx--side-nav__item--large",
      props.class
    )}
  >
    {props.children}
  </li>
);
