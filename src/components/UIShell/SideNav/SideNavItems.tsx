import { Component } from "solid-js";

export const SideNavItems: Component<{}> = (props) => (
  <ul class="bx--side-nav__items">{props.children}</ul>
);
