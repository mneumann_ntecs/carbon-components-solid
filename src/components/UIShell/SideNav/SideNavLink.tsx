import { JSX, splitProps } from "solid-js";
import { SideNavItem } from "./SideNavItem";
import { SideNavIcon } from "./SideNavIcon";
import { SideNavLinkText } from "./SideNavLinkText";
import { UnstyledLink } from "../../Link/UnstyledLink";
import { cx } from "../../../utils";

type SideNavLinkProps = {
  isSelected?: boolean;
  href?: string;
  text?: string;
  icon?: () => JSX.Element;
  target?: string;
  large?: boolean;
  class?: string;
} & JSX.HTMLAttributes<HTMLAnchorElement>;

type SideNavLinkComponent = (props: SideNavLinkProps) => JSX.Element;

export const SideNavLink: SideNavLinkComponent = (props) => {
  const [, restProps] = splitProps(props, [
    "isSelected",
    "href",
    "text",
    "icon",
    "large",
    "class",
  ]);

  return (
    <SideNavItem large={props.large}>
      <UnstyledLink
        aria-current={props.isSelected ? "page" : undefined}
        href={props.href}
        class={cx(
          "bx--side-nav__link",
          props.isSelected && "bx--side-nav__link--current",
          props.class
        )}
        {...restProps}
      >
        {props.icon && <SideNavIcon small>{props.icon()}</SideNavIcon>}
        <SideNavLinkText>{props.text}</SideNavLinkText>
      </UnstyledLink>
    </SideNavItem>
  );
};
