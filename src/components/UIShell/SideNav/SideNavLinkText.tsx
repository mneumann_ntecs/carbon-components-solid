import { Component } from "solid-js";

export const SideNavLinkText: Component<{}> = (props) => (
  <span class="bx--side-nav__link-text">{props.children}</span>
);
