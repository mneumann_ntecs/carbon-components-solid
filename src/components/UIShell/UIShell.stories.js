import { createSignal } from "solid-js";

import {
  Header,
  HeaderNav,
  HeaderNavItem,
  HeaderUtilities,
  HeaderGlobalAction,
  Content,
  SideNav,
  SideNavItems,
  SideNavLink,
  Icon,
} from "../../components";
import { Notification20 } from "carbon-icons-solid/lib/Notification20";
import { UserAvatar20 } from "carbon-icons-solid/lib/UserAvatar20";
import { AppSwitcher20 } from "carbon-icons-solid/lib/AppSwitcher20";
import { Add16 } from "carbon-icons-solid/lib/Add16";

export default {
  title: "UIShell",
};

export const _Header = () => {
  const sideNavOpenSignal = createSignal(true);

  return (
    <>
      <Header
        company="Carbon Components"
        href="/"
        platform="for Solid"
        sideNavOpenSignal={sideNavOpenSignal}
        persistentHamburgerMenu
      >
        <HeaderNav>
          <HeaderNavItem href="/" text="Link 1" />
          <HeaderNavItem href="/" text="Link 2" />
          <HeaderNavItem href="/" text="Link 3" />
        </HeaderNav>

        <HeaderUtilities>
          <HeaderGlobalAction
            aria-label="Notifications"
            icon={<Notification20 />}
          />
          <HeaderGlobalAction
            aria-label="User Avatar"
            icon={<UserAvatar20 />}
          />
          <HeaderGlobalAction
            aria-label="App Switcher"
            icon={<AppSwitcher20 />}
          />
        </HeaderUtilities>

        <SideNav sideNavOpenSignal={sideNavOpenSignal}>
          <SideNavItems>
            <SideNavLink text="Link 1" />
            <SideNavLink large text="Link 2" href="/" />
            <SideNavLink icon={<Icon render={Add16} />} text="Link 3" />
          </SideNavItems>
        </SideNav>
      </Header>

      <Content>
        <h1>Welcome</h1>
        <p>This is the content</p>
      </Content>
    </>
  );
};
