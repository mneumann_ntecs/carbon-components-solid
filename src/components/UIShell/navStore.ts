import { createSignal } from "solid-js";
const [shouldRenderHamburgerMenu, setShouldRenderHamburgerMenu] = createSignal(
  true
);
export { shouldRenderHamburgerMenu, setShouldRenderHamburgerMenu };
