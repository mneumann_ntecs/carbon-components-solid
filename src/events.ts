export function preventDefault(origHandler, ev) {
  ev.preventDefault();
  callEventHandler(origHandler, ev);
}

export function callEventHandler(origHandler, ev) {
  if (typeof origHandler === "function") {
    origHandler(ev);
  } else {
    const [handler, value] = origHandler;
    if (handler) {
      handler(value, ev);
    }
  }
}
