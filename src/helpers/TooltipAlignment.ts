export type TooltipAlignment = "start" | "center" | "end";

export const tooltipAlignmentClass = (
  tooltipAlignment?: TooltipAlignment
): string | undefined =>
  tooltipAlignment && `bx--tooltip--align-${tooltipAlignment}`;
