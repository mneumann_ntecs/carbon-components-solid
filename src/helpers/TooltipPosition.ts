export type TooltipPosition = "top" | "right" | "bottom" | "left";

export const tooltipPositionClass = (
  tooltipPosition?: TooltipPosition
): string | undefined => tooltipPosition && `bx--tooltip--${tooltipPosition}`;
