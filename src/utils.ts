import { splitProps, mergeProps } from "solid-js";

var idCounter = 0;

export function uniqueId(prefix: string = "ccs-"): string {
  var id = ++idCounter;
  return `${prefix}${id}`;
}

export type Signal<T> = [() => T, (v: T) => T];

export function clamp(val: number, min: number, max: number): number {
  return Math.max(min, Math.min(val, max));
}

export function extractProps(allProps, ownPropsDefaults) {
  const ownKeys = Object.keys(
    Object.getOwnPropertyDescriptors(ownPropsDefaults)
  );

  const [ownProps, restProps] = splitProps(allProps, ownKeys);
  const props = mergeProps(ownPropsDefaults, ownProps);

  return [props, restProps];
}

export function cx(
  ...classNames: Array<string | undefined | null | boolean>
): string {
  return classNames.filter(Boolean).join(" ");
}
